﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GDLibrary
{
    public class MenuUtility
    {
        public static MenuItem GetClickedMenuItem(List<MenuItem> list, MouseManager mouseManager)
        {
            for (int i = 0; i < list.Count; i++)
            {
                MenuItem item = list[i];

                //is the mouse over the item?
                if (mouseManager.Bounds.Intersects(item.Bounds))
                {
                    item.SetActive(true);

                    //is the left mouse button clicked
                    if (mouseManager.IsLeftButtonClickedOnce())
                        return item;
                }
                else
                {
                    item.SetActive(false);
                }
            }

            return null;
        }
    }
}
