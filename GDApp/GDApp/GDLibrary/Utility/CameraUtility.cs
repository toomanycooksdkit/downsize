﻿using Microsoft.Xna.Framework;

namespace GDLibrary
{
    public class CameraUtility
    {
        //camera to target vector, also provides access to distance from camera to target
        public static Vector3 GetCameraToTarget(GameObject parentObject, Camera parentCamera, out float distance)
        {
            //camera to target object vector
            Vector3 cameraToTarget = parentObject.Transform.Translation - parentCamera.Transform.Translation;

            //distance from camera to target
            distance = cameraToTarget.Length();

            //camera to target object vector
            cameraToTarget.Normalize();

            return cameraToTarget;
        }

        //camera to target vector
        public static Vector3 GetCameraToTarget(GameObject parentObject, Camera parentCamera)
        {
            //camera to target object vector
            return Vector3.Normalize(parentObject.Transform.Translation - parentCamera.Transform.Translation);
        }
    }
}
