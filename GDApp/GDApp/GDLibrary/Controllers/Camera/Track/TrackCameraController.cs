﻿using GDApp;
using Microsoft.Xna.Framework;

namespace GDLibrary
{
    public class TrackCameraController : CameraController
    {
        #region Variables
        private Camera parentCamera;
        private Transform3DCurve track;
        #endregion

        #region Properties
        public Camera ParentCamera
        {
            get
            {
                return parentCamera;
            }
            set
            {
                parentCamera = value;
            }
        }
        private Transform3DCurve Track
        {
            get
            {
                return track;
            }
            set
            {
                track = value;
            }
        }
        #endregion

        public TrackCameraController(Main game, Camera parentCamera, Transform3DCurve track)
            : base(game)
        {
            this.parentCamera = parentCamera;
            this.track = track;
        }

        public override void Update(GameTime gameTime, Camera camera)
        {
            if(this.track != null)
                UpdateController(gameTime);
        }

        private void UpdateController(GameTime gameTime)
        {
            Vector3 translation, look, up;
            this.track.Evalulate((float)gameTime.TotalGameTime.TotalMilliseconds, 2, out translation, out look, out up);

            this.parentCamera.Transform.Translation = translation;
            this.parentCamera.Transform.Look = look;
            this.parentCamera.Transform.Up = up;
        }

        //to do - clone, dispose

    }
}
