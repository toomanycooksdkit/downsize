﻿using GDApp;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using System.Collections.Generic;

namespace GDLibrary
{
    public class FirstPersonCameraController : CameraController
    {
        #region Variables
        private Main game;
        private Keys[] keys;
        private float moveSpeed, strafeSpeed, rotationSpeed;
        private bool bLockVertical;

        //variable set from main, so no property
        private Vector2 screenCentre;

        //local vars
        private Vector2 mouseDelta;
        private Matrix cameraRotation;
        #endregion

        #region Properties
        public Keys[] Keys
        {
            get
            {
                return keys;
            }
            set
            {
                keys = value;
            }
        }
        public float MoveSpeed
        {
            get
            {
                return moveSpeed;
            }
            set
            {
                moveSpeed = value;
            }
        }
        public float StrafeSpeed
        {
            get
            {
                return strafeSpeed;
            }
            set
            {
                strafeSpeed = value;
            }
        }
        public float RotationSpeed
        {
            get
            {
                return rotationSpeed;
            }
            set
            {
                rotationSpeed = value;
            }
        }
              
        public bool LockVertical
        {
            get
            {
                return bLockVertical;
            }
            set
            {
                bLockVertical = value;
            }
        }

        #endregion

        public FirstPersonCameraController(Main game, Keys[] keys,
            float moveSpeed, float strafeSpeed, float rotationSpeed, bool bLockVertical)
            : base(game)
        {
            this.game = game;
            this.keys = keys;
            this.moveSpeed = moveSpeed;
            this.strafeSpeed = strafeSpeed;
            this.rotationSpeed = rotationSpeed;
            this.bLockVertical = bLockVertical;

            //centre the mouse
            this.screenCentre = game.ScreenCentre;
            game.MouseManager.SetPosition(this.screenCentre);

        }

        public override void Update(GameTime gameTime, Camera camera)
        {
            HandleKeyboardInput(gameTime, camera);
            HandleMouseInput(gameTime, camera);
            this.game.MouseManager.SetPosition(camera.ViewPortCentre);
        }

        private void HandleMouseInput(GameTime gameTime, Camera camera)
        {
            //update rotation if the mouse position changes
            if (this.MouseManager.HasMoved())
            {
                //get mouse delta from view port centre and multiply by the time elapsed in ms since last update
                this.mouseDelta -= this.MouseManager.GetDeltaFromPosition(camera.ViewPortCentre) * gameTime.ElapsedGameTime.Milliseconds * this.rotationSpeed;

                //calculate matrix rotation
                this.cameraRotation = Matrix.CreateFromYawPitchRoll(mouseDelta.X, mouseDelta.Y, 0);

                //all rotations to look and up are made relative to the original look and up vectors - this is why we use ResetLook and ResetUp
                camera.Transform.Look = Vector3.Normalize(Vector3.Transform(camera.Transform.OriginalLook, cameraRotation));
                camera.Transform.Up = Vector3.Normalize(Vector3.Transform(camera.Transform.OriginalUp, cameraRotation));

            }
        }

        private void HandleKeyboardInput(GameTime gameTime, Camera camera)
        {
            Vector3 translate = Vector3.Zero;

            //W = forward
            if (this.KeyboardManager.IsKeyDown(this.keys[KeyInfo.Key_Array_Index_Forward]))
            {
                translate += camera.Transform.Look * this.moveSpeed;
            }
            //S = back
            else if (this.KeyboardManager.IsKeyDown(this.keys[KeyInfo.Key_Array_Index_Backward]))
            {
                translate += -camera.Transform.Look * this.moveSpeed;
            }

            //A = strafe left
            if (this.KeyboardManager.IsKeyDown(this.keys[KeyInfo.Key_Array_Index_Left]))
            {
                translate += -camera.Transform.Right * this.strafeSpeed;
            }

            //D = strafe right
            else if (this.KeyboardManager.IsKeyDown(this.keys[KeyInfo.Key_Array_Index_Right]))
            {
                translate += camera.Transform.Right * this.strafeSpeed;
            }

            if (!translate.Equals(Vector3.Zero))
            {
                translate *= gameTime.ElapsedGameTime.Milliseconds;

                //if we are working in first person we should not be able to move vertically
                if (bLockVertical)
                    translate.Y = 0;

                camera.Transform.MoveBy(translate);
            }
        }

        //to do - clone, dispose

    }
}
