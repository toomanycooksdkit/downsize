﻿using GDApp;
using Microsoft.Xna.Framework;

namespace GDLibrary
{
    public class RailCameraController : TargetCameraController
    {
        #region Variables
        private RailParameters railParameters;
        #endregion

        #region Properties
        public RailParameters RailParameters
        {
            get
            {
                return railParameters;
            }
            set
            {
                railParameters = value;
            }
        }
        #endregion

        public RailCameraController(Main game, Camera parentCamera, GameObject parentObject, 
            RailParameters railParameters)
            : base(game, parentCamera, parentObject)
        {
            this.railParameters = railParameters;

            //set the camera at the midpoint of the rail - remove and the camera will never be on the rail and so wont update correctly
            this.ParentCamera.Transform.Translation = railParameters.MidPoint;
        }

        protected override void UpdateController(GameTime gameTime, Camera camera)
        {
            Vector3 cameraToTarget = CameraUtility.GetCameraToTarget(this.ParentObject, this.ParentCamera);

            //new position for camera if it is positioned between start and the end points of the rail
            Vector3 projectedCameraPosition = this.ParentCamera.Transform.Translation
                + Vector3.Dot(cameraToTarget, railParameters.Look) * railParameters.Look * gameTime.ElapsedGameTime.Milliseconds;

            //do not allow the camera to move outside the rail
            if (railParameters.InsideRail(projectedCameraPosition))
                this.ParentCamera.Transform.Translation = projectedCameraPosition;

            //set the camera to look at the object
            this.ParentCamera.Transform.Look = cameraToTarget;
        }

        //to do - clone, dispose

    }
}
