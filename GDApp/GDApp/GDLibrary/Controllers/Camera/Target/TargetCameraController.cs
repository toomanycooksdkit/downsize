﻿using GDApp;
using Microsoft.Xna.Framework;

namespace GDLibrary
{
    //This class is abstract since we never create an instance of it because the UpdateController() method performs no action.
    //See ThirdPersonCameraController, RailCameraControlller, and OrbitCameraController
    public abstract class TargetCameraController : CameraController
    {
        #region Variables
        private Camera parentCamera;
        private GameObject parentObject;
        #endregion

        #region Properties
        public Camera ParentCamera
        {
            get
            {
                return parentCamera;
            }
            set
            {
                parentCamera = value;
            }
        }
        public GameObject ParentObject
        {
            get
            {
                return parentObject;
            }
            set
            {
                parentObject = value;
            }
        }
        #endregion

        public TargetCameraController(Main game, Camera parentCamera, GameObject parentObject)
            : base(game)
        {
            this.parentCamera = parentCamera;
            this.parentObject = parentObject;
        }

        public override void Update(GameTime gameTime, Camera camera)
        {
            //if (this.parentObject != null)
                UpdateController(gameTime, camera);
        }

        protected virtual void UpdateController(GameTime gameTime, Camera camera)
        {

        }
        //to do - clone, dispose
    }
}
