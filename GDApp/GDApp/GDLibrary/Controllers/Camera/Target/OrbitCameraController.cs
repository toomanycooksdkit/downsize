﻿using GDApp;
using Microsoft.Xna.Framework;

namespace GDLibrary
{
    public class OrbitCameraController : ThirdPersonCameraController
    {
        #region Variables 
        #endregion

        #region Properties 
        #endregion

        public OrbitCameraController(Main game, Camera parentCamera,
            GameObject parentObject, float distance, float elevationAngle, float lerpSpeed)
            : base(game, parentCamera, parentObject, distance, elevationAngle, lerpSpeed)
        {
            //to do...
        }

        protected override void UpdateController(GameTime gameTime, Camera camera)
        {
           //to do...
        }

        //to do - clone, dispose
    }
}
