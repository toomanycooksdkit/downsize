﻿using GDApp;
using GDApp.GDLibrary.Objects.ADownsizeObjects;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;

namespace GDLibrary
{
    public class ThirdPersonCameraController : TargetCameraController
    {
        #region Variables
        private float elevationAngle;
        private float distance;

        //used to dampen camera movement
        private Vector3 oldTranslation;
        private Vector3 oldLook;
        private Vector3 oldUp;
        private float lerpSpeed=0.2f;
        private Main game;
        private float leftRightAngle;
        private Vector2 mouseDelta;



        private float upDownSpeed;

        #endregion

        #region Properties
        public float LerpSpeed
        {
            get
            {
                return lerpSpeed;
            }
            set
            {

                //lerp speed should be in the range >0 and <=1
                lerpSpeed = (value > 0) && (lerpSpeed <= 1) ? value : 0.1f;
            }
        }

        public float Distance
        {
            get
            {
                return distance;
            }
            set
            {
                //distance should not be lower than 0
                distance = (value > 0) ? value : 1;
            }
        }
        public float ElevationAngle 
        { 
            get
            {
                return elevationAngle;
            }
            set
            {
                elevationAngle = value%360;

                elevationAngle = MathHelper.ToRadians(elevationAngle);
            }
        }

        public float LeftRightAngle
        {
            get
            {
                return leftRightAngle;
            }
            set
            {
                leftRightAngle = value % 360;

                leftRightAngle = MathHelper.ToRadians(leftRightAngle);
            }
        }
        #endregion

        public ThirdPersonCameraController(Main game, Camera parentCamera, 
            GameObject parentObject, float distance, float elevationAngle, float lerpSpeed)
            : base(game, parentCamera, parentObject)
        {
            this.game = game;

            this.upDownSpeed = 1f;
            //call properties to set validation on distance and radian conversion
            this.Distance = distance;
            this.ElevationAngle = elevationAngle;

            this.LeftRightAngle = 170;

            //dampen camera movement
            this.lerpSpeed = lerpSpeed; //slow = 0.05f, med = 0.1f, fast = 0.2f
            this.oldTranslation = this.ParentCamera.Transform.Translation;
            this.oldLook = this.ParentCamera.Transform.Look;
            this.oldUp = this.ParentCamera.Transform.Up;
        }


        public override void Update(GameTime gameTime, Camera camera)
        {
            ChangeElAngle(gameTime);
            HandleMouseInput(gameTime);
            UpdateController2Angles(gameTime, camera);
            //UpdateController1Angle(gameTime, camera);
            //this.game.MouseManager.SetPosition(camera.ViewPortCentre);
        }

        private void HandleMouseInput(GameTime gameTime)
        {
            //update rotation if the mouse position changes
            if (!game.MouseManager.IsRightButtonClicked())
            {
                this.game.MouseManager.SetPosition(game.ScreenCentre);

                if (game.MouseManager.HasMoved())
                {
                    this.mouseDelta = game.MouseManager.GetDeltaFromPosition(game.ScreenCentre) * gameTime.ElapsedGameTime.Milliseconds * 0.001f;
                    elevationAngle -= mouseDelta.Y;
                    this.mouseDelta = Vector2.Zero;

                }
            }

            //update rotation if the mouse position changes
            if (game.MouseManager.IsMiddleButtonPressed())
            {
                this.game.MouseManager.SetPosition(game.ScreenCentre);

                if (game.MouseManager.HasMoved())
                {
                    this.mouseDelta = game.MouseManager.GetDeltaFromPosition(game.ScreenCentre) * gameTime.ElapsedGameTime.Milliseconds * 0.001f;
                    leftRightAngle -= mouseDelta.X;
                    this.mouseDelta = Vector2.Zero;

                }
            }

        }

        protected void UpdateController1Angle(GameTime gameTime, Camera camera)
        {


            //rotate the target look around the target right to get a vector pointing away from the target at a specified elevation
            Vector3 cameraToTarget = Vector3.Transform(this.ParentObject.Transform.Look, Matrix.CreateFromAxisAngle(this.ParentObject.Transform.Right, elevationAngle));
            //normalize to give unit length, otherwise distance from camera to target will vary over time
            cameraToTarget.Normalize();

            //set the position of the camera to be a set distance from target and at certain elevation angle
            this.ParentCamera.Transform.Translation = Vector3.Lerp(this.oldTranslation, cameraToTarget * distance + this.ParentObject.Transform.Translation, lerpSpeed);

            Vector3 cameraToTargetLook = this.ParentObject.Transform.Translation - this.ParentCamera.Transform.Translation;
            cameraToTargetLook.Normalize();
            this.ParentCamera.Transform.Look = cameraToTargetLook;

            this.ParentCamera.Transform.Up = Vector3.Lerp(this.oldUp, this.ParentObject.Transform.Up, lerpSpeed);

            this.oldTranslation = this.ParentCamera.Transform.Translation;
            this.oldLook = this.ParentCamera.Transform.Look;
            this.oldUp = this.ParentCamera.Transform.Up;
        }

        protected void UpdateController2Angles(GameTime gameTime, Camera camera)
        {
            PlayerCollidableObject po = (PlayerCollidableObject)this.ParentObject;
            
            //rotate the target look around the target right to get a vector pointing away from the target at a specified elevation
            Vector3 cameraToTarget = Vector3.Transform(this.ParentObject.Transform.Look, Matrix.CreateFromAxisAngle(this.ParentObject.Transform.Right, elevationAngle));

            Vector3 cameraToTargetSides = Vector3.Transform(this.ParentObject.Transform.Look, Matrix.CreateFromAxisAngle(this.ParentObject.Transform.Up, leftRightAngle));

            Vector3 totalTarget = cameraToTarget + cameraToTargetSides;
            totalTarget.Normalize();

            //normalize to give unit length, otherwise distance from camera to target will vary over time
            cameraToTarget.Normalize();
            this.ParentCamera.Transform.Translation = totalTarget * distance + po.CharacterBody.Position;

            Vector3 cameraToTargetLook = po.CharacterBody.Position - this.ParentCamera.Transform.Translation;
            cameraToTargetLook.Normalize();

            //set the camera to have the same orientation as the target object
            this.ParentCamera.Transform.Look = cameraToTargetLook;

            this.ParentCamera.Transform.Up = Vector3.Lerp(this.oldUp, this.ParentObject.Transform.Up, lerpSpeed);

            this.oldTranslation = this.ParentCamera.Transform.Translation;
            this.oldLook = this.ParentCamera.Transform.Look;
            this.oldUp = this.ParentCamera.Transform.Up;
        }
  
        float speed = 1;

        private void ChangeElAngle(GameTime gameTime)
        {
            


            if (game.KeyboardManager.IsKeyDown(Keys.G))
            {
                speed += 0.1f;
            }
            else if (game.KeyboardManager.IsKeyDown(Keys.V))
            {
                speed -= 0.1f;
            }
            

            if (game.KeyboardManager.IsKeyDown(Keys.K))
            {
                elevationAngle += 0.01f * speed;
            }
            else if (game.KeyboardManager.IsKeyDown(Keys.M))
            {
                elevationAngle -= 0.01f * speed;
            }

            if (game.KeyboardManager.IsKeyDown(Keys.B))
            {
                distance += 0.1f * speed;
            }
            else if (game.KeyboardManager.IsKeyDown(Keys.H))
            {
                distance -= 0.1f * speed;
            }

            
            distance -= game.MouseManager.GetScrollWheelDelta() * 0.01f;

            if (game.KeyboardManager.IsKeyDown(Keys.J))
            {
                leftRightAngle += 0.01f * speed;

            }
            else if (game.KeyboardManager.IsKeyDown(Keys.N))
            {
                leftRightAngle -= 0.01f * speed;
            }

        }



        //to do - clone, dispose
    }
}
