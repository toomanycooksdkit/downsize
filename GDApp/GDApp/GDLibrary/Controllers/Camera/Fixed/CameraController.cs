﻿using GDApp;
using Microsoft.Xna.Framework;

namespace GDLibrary
{
    public class CameraController
    {
        #region Variables
        private KeyboardManager keyboardManager;
        private MouseManager mouseManager;
        #endregion

        #region Properties
        protected KeyboardManager KeyboardManager
        {
            get
            {
                return keyboardManager;
            }
        }

        protected MouseManager MouseManager
        {
            get
            {
                return mouseManager;
            }
        }
        #endregion

        public CameraController(Main game)
        {
            this.keyboardManager = game.KeyboardManager;
            this.mouseManager = game.MouseManager;
        }

        public virtual void Update(GameTime gameTime, Camera camera)
        {

        }



        //to do - clone, dispose
        
    }
}
