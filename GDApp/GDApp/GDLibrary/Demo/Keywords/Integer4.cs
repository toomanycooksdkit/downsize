﻿
namespace GDLibrary
{
    public class Integer4
    {
        private int x, y, z, w;

        public int X
        {
            get
            {
                return x;
            }
            set
            {
                x = value;
            }
        }
        public int Y
        {
            get
            {
                return y;
            }
            set
            {
                y = value;
            }
        }
        public int Z
        {
            get
            {
                return z;
            }
            set
            {
                z = value;
            }
        }
        public int W
        {
            get
            {
                return w;
            }
            set
            {
                w = value;
            }
        }


        public Integer4(int x, int y, int z, int w)
        {
            this.x = x; this.y = y; this.z = z; this.w = w;
        }

        public static void Add(Integer4 a, Integer4 b, 
            out Integer4 result, out float distance)
        {
            result = new Integer4(
                a.X + b.X, a.Y + b.Y, a.Z + b.Z, a.W + b.W);

            distance = 10;
        }

        public static Integer4 operator *(Integer4 value, int multiplier)
        {
            return new Integer4(value.X * multiplier,
                value.Y * multiplier,
                value.Z * multiplier,
                value.W * multiplier);
        }

        public static Integer4 operator *(int multiplier, Integer4 value)
        {
            return new Integer4(value.X * multiplier,
                value.Y * multiplier,
                value.Z * multiplier,
                value.W * multiplier);
        }

        public override string ToString()
        {
            return "(x: " + x + ", " + "y: " + y + ", " + "z: " + z + "w: " + w + ")";
        }

        //to do - add at least two operators e.g. + and *

    }
}
