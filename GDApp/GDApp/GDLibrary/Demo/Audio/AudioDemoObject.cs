﻿using GDApp;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;

namespace GDLibrary
{
    public class AudioDemoObject : ModelObject
    {
        #region Variables
        private AudioEmitter audioEmitter;
        #endregion

        #region Properties
        #endregion

        public AudioDemoObject(Main game, Transform3D transform, BasicEffect effect,
            ModelData modelData, Texture2D texture, Color color)
            : base(game, transform, effect, modelData, texture, color)
        {
            this.audioEmitter = new AudioEmitter();
        }

        public override void Update(GameTime gameTime)
        {
            this.audioEmitter.Position = this.Transform.Translation;
            
            if (this.Game.KeyboardManager.IsKeyDown(Microsoft.Xna.Framework.Input.Keys.P))
                this.Game.SoundManager.Play3DCue("boing", this.audioEmitter);

              base.Update(gameTime);
        }
    }
}
