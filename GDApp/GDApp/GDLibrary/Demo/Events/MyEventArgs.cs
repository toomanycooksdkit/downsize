﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GDLibrary
{
    public class MyEventArgs : EventArgs
    {
        public string s;
        public int y;

        public MyEventArgs(string s, int y)
        {
            this.s = s; this.y = y;
        }

        public override string ToString()
        {
            return "s: " + s + ", " + "y: " + y;
        }
    }
}
