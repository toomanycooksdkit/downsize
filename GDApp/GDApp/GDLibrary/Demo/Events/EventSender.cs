﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

//See https://msdn.microsoft.com/en-us/library/aa645739%28v=vs.71%29.aspx
//See https://msdn.microsoft.com/en-us/library/aa288459(v=vs.71).aspx
namespace GDLibrary
{
    //step 1 - a delegate encapsulates (or represents) a method (i.e. Main::sender_Updated()) so that we can call the method anonymously
    public delegate void UpdatedEventHandler(object sender, MyEventArgs m);

    public class EventSender
    {
        //step 2 - an event can be thought of has having multiple listeners attached. these listeners are effectively the methods (i.e. Main::sender_Updated()) that register for notification
        public event UpdatedEventHandler Updated;

        //step 3 - the method which is called to trigger the event
        public void OnUpdated(MyEventArgs m)
        {
            //step 4 - if this event is not null then some object (i.e. Main) has registered a method to be called when the event fires
            if (Updated != null)
                //fire the event i.e. call all the registered methods for the event
                Updated(this, m);
        }
        public void GenerateEvent()
        {
            //step 5 - so now make up the event arguments object and fire the event
            MyEventArgs m = new MyEventArgs("a message", 10);
            OnUpdated(m);
        }
    }
}
