﻿using Microsoft.Xna.Framework;

namespace GDLibrary
{
    public class Transform3DCurve
    {
        private Curve3D translationCurve3D, lookCurve3D, upCurve3D;

        public Transform3DCurve(CurveLoopType curveLoopType)
        {
            this.translationCurve3D = new Curve3D(curveLoopType);
            this.lookCurve3D = new Curve3D(curveLoopType);
            this.upCurve3D = new Curve3D(curveLoopType);
        }

        public void Add(Vector3 translation, Vector3 look, Vector3 up, float time)
        {
            this.translationCurve3D.Add(translation, time);
            this.lookCurve3D.Add(look, time);
            this.upCurve3D.Add(up, time);
        }

        public void Clear()
        {
            this.translationCurve3D.Clear();
            this.lookCurve3D.Clear();
            this.upCurve3D.Clear();
        }

        //See https://msdn.microsoft.com/en-us/library/t3c3bfhx.aspx for information on using the out keyword
        public void Evalulate(float time, int precision, out Vector3 translation, out Vector3 look, out Vector3 up)
        {
            translation = this.translationCurve3D.Evaluate(time, precision);
            look = this.lookCurve3D.Evaluate(time, precision);
            up = this.upCurve3D.Evaluate(time, precision);
        }
    }
}
