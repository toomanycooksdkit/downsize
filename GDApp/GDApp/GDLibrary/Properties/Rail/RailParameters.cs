﻿using System;
using Microsoft.Xna.Framework;

namespace GDLibrary
{
    public class RailParameters
    {
        private Vector3 start, end, look, midPoint;
        private float length;

        bool bDirty = true;
        private string name;

        #region PROPERTIES
        public string Name
        {
            get
            {
                return name;
            }
        }

        public Vector3 Look
        {
            get
            {
                Update();
                return look;
            }
        }


        public float Length
        {
            get
            {
                Update();
                return length;
            }
        }

        private void Update()
        {
            if (bDirty)
            {
                this.length = Math.Abs(Vector3.Distance(start, end));
                this.look = Vector3.Normalize(end - start);
                bDirty = false;
            }
        }

        public Vector3 Start
        {
            get
            {
                return start;
            }
            set
            {
                start = value;
                bDirty = true;
            }
        }

        public Vector3 MidPoint
        {
            get
            {
                return midPoint;
            }
        }
        public Vector3 End
        {
            get
            {
                return end;
            }
            set
            {
                end = value;
                bDirty = true;
            }
        }
        #endregion

        public RailParameters(string name, Vector3 start, Vector3 end)
        {
            this.name = name;

            this.start = start;
            this.end = end;
            this.midPoint = (start + end)/2;

            this.look = Vector3.Normalize(end - start);
            this.length = Math.Abs(Vector3.Distance(end, start));
        }

        /// <summary>
        /// Returns true if the position is between start and end, otherwise false
        /// </summary>
        /// <param name="position"></param>
        /// <returns></returns>
        public bool InsideRail(Vector3 position)
        {
            float distanceToStart = Vector3.Distance(position, start);
            float distanceToEnd = Vector3.Distance(position, end);
            return ((distanceToStart <= length) && (distanceToEnd <= length));
        }


    }
}
