﻿using Microsoft.Xna.Framework;
using System;

namespace GDLibrary
{
    public class Transform3D : ICloneable
    {
        #region Variables
        //pre-sets
        public static Transform3D ZeroUnitX = new Transform3D(Vector3.Zero, Vector3.UnitX, Vector3.UnitY);
  
        private Vector3 translation, rotation, scale;
        private Vector3 look, up, right;
        private Vector3 originalTranslation, originalRotation, originalScale;
        private Vector3 originalUp, originalLook;

        private bool bDirty;
        private Matrix world;
        private Vector3 rotAxisVec;
        private float angle;
        private bool withAxisAngle;
        #endregion

        #region Properties
        public bool WithAxisAngle
        {
            set
            {
                withAxisAngle = value;
                bDirty = true;
            }
            get
            {

                return withAxisAngle;
                
            }
        }
        public Matrix World
        {
            set
            {
                world = value;
            }
            get
            {
                if (bDirty)
                    UpdateWorld();

                return world;
            }
        }
        public Vector3 Translation
        {
            get
            {
                return translation;
            }
            set
            {
                translation = value;
                bDirty = true;
            }
        }
        public Vector3 Rotation
        {
            get
            {
                return rotation;
            }
            set
            {
                rotation = value;
                bDirty = true;
            }
        }
        public Vector3 Scale
        {
            get
            {
                return scale;
            }
            set
            {
                scale = value;
            }
        }

        public Vector3 Target
        {
            get
            {
                return translation + look;
            }
        }
        public Vector3 Up
        {
            get
            {
                return up;
            }
            set
            {
                up = value;
            }
        }
        public Vector3 Look
        {
            get
            {
                return look;
            }
            set
            {
                look = value;
            }
        }
        public Vector3 Right
        {
            get
            {
                return Vector3.Normalize(Vector3.Cross(this.look, this.up));
            }
        }
        public Vector3 OriginalLook
        {
            get
            {
                return originalLook;
            }
        }
        public Vector3 OriginalRotation
        {
            get
            {
                return originalRotation;
            }
        }
        public Vector3 OriginalScale
        {
            get
            {
                return originalScale;
            }
        }
        public Vector3 OriginalUp
        {
            get
            {
                return originalUp;
            }
        }
        public Matrix Orientation
        {
            get
            {
                return Matrix.CreateRotationX(MathHelper.ToRadians(rotation.X))
                        * Matrix.CreateRotationY(MathHelper.ToRadians(rotation.Y))
                            * Matrix.CreateRotationZ(MathHelper.ToRadians(rotation.Z));
            }
        }

        public Matrix AxisAngleRotation
        {
            get
            {
                return Matrix.CreateFromAxisAngle(this.rotAxisVec, this.angle);
            }
        }
        #endregion

        

        //used only by an object that doesn't scale or rotate initially i.e. a camera
        public Transform3D(Vector3 translation, Vector3 look, Vector3 up)
            : this(translation, Vector3.Zero, Vector3.One, look, up)
        {
        }    

        //used by any drawn object in the world that will move
        public Transform3D(Vector3 translation, Vector3 rotation, Vector3 scale, Vector3 look, Vector3 up)
        {
            //set translation, rotation and scale. store originals
            this.translation = this.originalTranslation = translation;
            this.rotation = this.originalRotation = rotation;
            this.scale = this.originalScale = scale;

            //set look, up, right and store originals
            this.look = this.originalLook = Vector3.Normalize(look);
            this.up = this.originalUp = Vector3.Normalize(up);
            this.right = Vector3.Normalize(Vector3.Cross(this.look, this.up));

            this.withAxisAngle = false;
            //flag as dirty so that the World matrix will be generated inside the property the first time it is invoked
            bDirty = true;
        }

        public void reset()
        {
            this.translation = originalTranslation;
            this.rotation = originalRotation;
            this.scale = originalScale;

            this.look = originalLook;
            this.up = originalUp;
            this.right = Vector3.Normalize(Vector3.Cross(this.look, this.up));
        }
      
        public void RotateBy(float magnitude)
        {
            this.rotation.Y += magnitude;
            this.look = Vector3.Normalize(Vector3.Transform(originalLook,
                Matrix.CreateRotationY(MathHelper.ToRadians(rotation.Y))));
            this.right = Vector3.Normalize(Vector3.Cross(look, up));

            bDirty = true;
        }

        public void RotateTo(float magnitude)
        {
            this.rotation.Y = magnitude;
            this.look = Vector3.Normalize(Vector3.Transform(originalLook,
                Matrix.CreateRotationY(MathHelper.ToRadians(rotation.Y))));
            this.right = Vector3.Normalize(Vector3.Cross(look, up));

            bDirty = true;
        }

      

        public void MoveBy(Vector3 translation)
        {
            this.translation += translation;
            bDirty = true;
        }

        public void ScaleTo(Vector3 scale)
        {
            this.scale = scale;
            bDirty = true;
        }

        public void rotateByAxisAngle(Vector3 rotAxisVec, float angle)
        {
            this.rotAxisVec = rotAxisVec;
            this.angle = angle;
            this.withAxisAngle = true;
            this.bDirty = true;

        }


        private void UpdateWorld()
        {

            if (withAxisAngle == true)
            {
                this.world = Matrix.Identity * Matrix.CreateScale(scale)
                        * Matrix.CreateTranslation(translation)
                        * Matrix.CreateRotationX(MathHelper.ToRadians(rotation.X))
                        * Matrix.CreateRotationY(MathHelper.ToRadians(rotation.Y))
                        * Matrix.CreateRotationZ(MathHelper.ToRadians(rotation.Z));
            }
            else
            {
                this.world = Matrix.Identity * Matrix.CreateScale(scale)
                    * Matrix.CreateRotationX(MathHelper.ToRadians(rotation.X))
                    * Matrix.CreateRotationY(MathHelper.ToRadians(rotation.Y))
                    * Matrix.CreateRotationZ(MathHelper.ToRadians(rotation.Z))
                    * Matrix.CreateTranslation(translation);
            }

            bDirty = false;
        }



        //clone
        public object Clone()
        {
            return new Transform3D(this.translation, this.rotation, this.scale, this.look, this.up);
        }

        /*  
         * Q. Should we add a dispose() method? If not, why? 
         * Clue: Look at the types of the variables in this class. Value or reference types?
         * A. You cannot nullify a reference type
         * e.g. 
         *      int x = 5; 
         *      x = null;
         */
    }
}
