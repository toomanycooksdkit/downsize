﻿using Microsoft.Xna.Framework;
using System;

namespace GDLibrary
{
    public class Transform2D : ICloneable
    {
        #region Variables
        //pre-sets
        public static Transform2D Zero = new Transform2D(Vector2.Zero, 0, Vector2.One);

        private Vector2 translation, scale;
        private float rotation;

        private Vector2 originalTranslation, originalScale;
        private float originalRotation;
        #endregion

        #region Properties
        public Vector2 Translation
        {
            get
            {
                return translation;
            }
            set
            {
                translation = value;
            }
        }
        public float Rotation
        {
            get
            {
                return rotation;
            }
            set
            {
                rotation = value;
            }
        }
        public Vector2 Scale
        {
            get
            {
                return scale;
            }
            set
            {
                scale = value;
            }
        }
        public Vector2 OriginalTranslation
        {
            get
            {
                return originalTranslation;
            }
        }
        public float OriginalRotation
        {
            get
            {
                return originalRotation;
            }
        }
        public Vector2 OriginalScale
        {
            get
            {
                return originalScale;
            }
        }
        #endregion


        //used by any drawn UI object 
        public Transform2D(Vector2 translation, float rotation, Vector2 scale)
        {
            //set translation, rotation and scale. store originals
            this.translation = this.originalTranslation = translation;
            this.rotation = this.originalRotation = rotation;
            this.scale = this.originalScale = scale;
        }

        public void reset()
        {
            this.translation = originalTranslation;
            this.rotation = originalRotation;
            this.scale = originalScale;
        }

        public void RotateBy(float rotation)
        {
            this.rotation += rotation;
        }

        public void MoveBy(Vector2 translation)
        {
            this.translation += translation;
        }

        public void ScaleTo(Vector2 scale)
        {
            this.scale = scale;
        }

        //clone
        public object Clone()
        {
            return new Transform2D(this.translation, this.rotation, this.scale);
        }
    }
}
