﻿using Microsoft.Xna.Framework;
using System;

namespace GDLibrary
{
    public class ProjectionParameters : ICloneable
    {
        #region Variables
        //pre-sets
        public static ProjectionParameters StandardFourThreeFar = new ProjectionParameters(MathHelper.ToRadians(45), 4 / 3.0f, 1, 1200);
        public static ProjectionParameters StandardFourThreeMedium = new ProjectionParameters(MathHelper.ToRadians(45), 4 / 3.0f, 1, 800);
        public static ProjectionParameters StandardFourThreeNear = new ProjectionParameters(MathHelper.ToRadians(45), 4 / 3.0f, 1, 400);

        //properties of the virtual camera
        private float fieldOfView, aspectRatio, nearClipPlane, farClipPlane;
         
        //perspective projection matrix formed from the parameters above
        private Matrix projection;

        //clean dirty flag to indicate an update has occured
        private bool bDirty;
        #endregion

        #region Properties
        public float FieldOfView
        {
            get
            {
                return fieldOfView;
            }
            set
            {
                fieldOfView = value;
                bDirty = true;
            }
        }
        public float AspectRatio
        {
            get
            {
                return aspectRatio;
            }
            set
            {
                aspectRatio = value;
                bDirty = true;
            }
        }
        public float NearClipPlane
        {
            get
            {
                return nearClipPlane;
            }
            set
            {
                nearClipPlane = value;
                bDirty = true;
            }
        }
        public float FarClipPlane
        {
            get
            {
                return farClipPlane;
            }
            set
            {
                farClipPlane = value;
                bDirty = true;
            }
        }

        public Matrix Projection
        {
            get
            {
                if (bDirty)
                    UpdateProjection();
                    
                return projection;
            }
        }
        #endregion

        public ProjectionParameters(float fieldOfView, float aspectRatio, float nearClipPlane, float farClipPlane)
        {
            this.fieldOfView = fieldOfView;
            this.aspectRatio = aspectRatio;
            this.nearClipPlane = nearClipPlane;
            this.farClipPlane = farClipPlane;

            this.bDirty = true; //flag for first update
        }

        private void UpdateProjection()
        {
            this.projection = Matrix.CreatePerspectiveFieldOfView(this.fieldOfView, this.aspectRatio, this.nearClipPlane, this.farClipPlane);
            this.bDirty = false; 
        }

        //to do - clone

        public object Clone()
        {
            return new ProjectionParameters(this.fieldOfView, this.aspectRatio, this.nearClipPlane, this.farClipPlane);
        }
    }
}
