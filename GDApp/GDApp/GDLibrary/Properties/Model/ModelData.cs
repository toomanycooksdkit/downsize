﻿using JigLibX.Geometry;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GDLibrary 
{
    public class ModelData 
    {
        #region Variables
        private Model model;
        private Vector3 scale, translation;
        private Matrix orientation;
        private Matrix[] boneTransforms;
        #endregion

        #region Properties
        public Vector3 Scale
        {
            get
            {
                return scale;
            }
        }
        public Vector3 Translation
        {
            get
            {
                return translation;
            }
        }
        public Matrix Orientation
        {
            get
            {
                return orientation;
            }
        }

        public Model Model
        {
            get
            {
                return model;
            }
            set
            {
                model = value;

                //copy the array of parent bone transforms for each mesh (1 parent bone per mesh)
                boneTransforms = new Matrix[this.model.Bones.Count];
                model.CopyAbsoluteBoneTransformsTo(boneTransforms);

                //decompose the parent bone matrix to determine what the translation, scale, and rotation are for the model
                Quaternion modelRotationAsQuaternion = new Quaternion();
                boneTransforms[model.Meshes[0].ParentBone.Index].Decompose(out scale, out modelRotationAsQuaternion, out translation);
                Matrix.CreateFromQuaternion(ref modelRotationAsQuaternion, out orientation);
            }
        }
        public Matrix[] BoneTransforms
        {
            get
            {
                return boneTransforms;
            }
        }
        #endregion

        public ModelData(Model model)
        {
            //call property above which will set bone transforms
            this.Model = model;
        }

        
    
        public virtual void Draw(Effect effect, Matrix world)
        {
            BasicEffect basicEffect = effect as BasicEffect;

            foreach (ModelMesh mesh in this.model.Meshes)
            {
                foreach (ModelMeshPart part in mesh.MeshParts)
                {
                    part.Effect = effect;
                }
                basicEffect.World = this.boneTransforms[mesh.ParentBone.Index] * world;
                mesh.Draw();
            }
        }
    }
}
