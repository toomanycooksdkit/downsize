﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Xml.Serialization;

namespace GDLibrary
{
    /// <summary>
    /// Base class for all camera types.
    /// </summary>
    public class Camera : ICloneable
    {
        #region Variables
        //every camera has a unique index
        private ushort index;
        private static ushort indexCount = 0;

        //every camera has a unique name
        private string name;

        //first person, free look, arc?
        private CameraType cameraType;

        //on, active, or active + in focus
        private ActivationType activationType;

        //projection and transform (i.e. position, rotation) information 
        private Transform3D transform;
        private ProjectionParameters projectionProperties;

        //a camera is static unless we set a controller e.g. FirstPersonCameraController
        private CameraController cameraController;

        private Viewport viewPort;
        private Vector2 viewPortCentre;
        #endregion

        #region Properties
        public ActivationType Activation
        {
            get
            {
                return activationType;
            }
            set
            {
                activationType = value; 
            }
        }

        public Vector2 ViewPortCentre
        {
            get
            {
                return viewPortCentre;
            }
        }
        public Viewport Viewport
        {
            get
            {
                return viewPort;
            }
            set
            {
                viewPort = value;
                viewPortCentre = new Vector2(this.viewPort.Width / 2.0f, this.viewPort.Height / 2.0f);
            }
        }
        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value.Trim(); //remove any whitespaces with a trim()
            }
        }
        public CameraType CameraType
        {
            get
            {
                return cameraType;
            }
            set
            {
                cameraType = value; 
            }
        }
        public int Index
        {
            get
            {
                return (int)index;
            }
        }
        public Matrix View
        {
            get
            {
                return Matrix.CreateLookAt(transform.Translation, transform.Target, transform.Up);
            }
        }
        public Matrix Projection
        {
            get
            {
                return projectionProperties.Projection;
            }
        }
        public Transform3D Transform
        {
            get
            {
                return transform;
            }
            set
            {
                transform = value;
            }
        }
        public ProjectionParameters ProjectionProperties
        {
            get
            {
                return projectionProperties;
            }
        }
        
        public CameraController CameraController
        {
            get
            {
                return cameraController;
            }
            set
            {
                cameraController = value;
            }
        }
        public BoundingFrustum BoundingFrustum
        {
            get
            {
                return new BoundingFrustum(this.View * this.Projection);
            }
        }
        public BoundingSphere BoundingSphere
        {
            get
            {
                return new BoundingSphere(this.Transform.Translation, GameInfo.Camera_Bounding_Sphere_Radius);
            }

        }
        #endregion

        public Camera(string name, CameraType cameraType, Transform3D transform, ProjectionParameters projectionProperties, Viewport viewPort)
        {
            this.name = name;
            this.cameraType = cameraType;
            this.index = indexCount++;

            this.transform = transform;
            this.projectionProperties = projectionProperties;
            this.viewPort = viewPort;

            //by default all camera are active when instanciated
            this.activationType = ActivationType.On;
        }

        public void InflateViewPort()
        {

        }

        public void Update(GameTime gameTime)
        {
            if((cameraController != null) && (this.activationType != ActivationType.Off))
                cameraController.Update(gameTime, this);
        }

        //to do - dispose

        public object Clone()
        {
            return new Camera("clone: " + this.name, this.cameraType, (Transform3D)this.transform.Clone(),
                this.projectionProperties, this.viewPort);
        }


    }
}
