﻿using GDApp;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;

namespace GDLibrary
{
    //stores all game objects and calls update and draw
    public class ObjectManager : DrawableGameComponent
    {
        public static bool bDebugMode = true;

        #region Variables
        private Main game;

        //list of objects to be updated and removed
        private List<GameObject> list, removeList;
        private GameObject alphaGrass;
        private GameObject blockGrassL;
        private GameObject blockGrassR;
        //pause/unpause based on menu event
        private bool bPaused = false;
        private bool bSplitScreenMode;

        #endregion

        #region Properties
        public bool SplitScreenMode
        {
            get
            {
                return bSplitScreenMode;
            }
            set
            {
                bSplitScreenMode = value;
            }
        }
        public GameObject this[int index]
        {
            get
            {
                return this.list[index];
            }
        }
        #endregion

        //single-screen mode - normal game mode
        public ObjectManager(Main game)
            : this(game, false)
        {

        }

        //split-screen mode - developer mode?
        public ObjectManager(Main game, bool bSplitScreenMode)
            : base(game)
        {
            this.game = game;
            this.bSplitScreenMode = bSplitScreenMode;

            //objects to be drawn and updated
            this.list = new List<GameObject>();

            //objects to be removed at next update
            this.removeList = new List<GameObject>();

            //register for the menu events
            this.game.EventDispatcher.MainMenu += new MenuEventHandler(HandleMenu);
        }

        #region Event Handling
        //handle the relevant menu events
        private void HandleMenu(EventData eventData)
        {
            if (eventData.Event == EventData.EventType.Play)
                bPaused = false;
            else if (eventData.Event == EventData.EventType.Pause)
                bPaused = true;
        }
        #endregion

        public void Add(GameObject obj)
        {
            this.list.Add(obj);
        }

        public void setAlphaGrass(GameObject obj)
        {
            this.alphaGrass = obj;
        }

        public void setAlphaGrassL(GameObject obj)
        {
            this.blockGrassL = obj;
        }

        public void setAlphaGrassR(GameObject obj)
        {
            this.blockGrassR = obj;
        }

        public void Remove(GameObject obj)
        {
            this.removeList.Add(obj);
        }

        public int Size()
        {
            return this.list.Count;
        }

        public void Clear()
        {
            this.list.Clear();
        }

        public override void Update(GameTime gameTime)
        {
            RemoveOldObjects();

            //if not paused then update cameras
            if (!bPaused)
            {
                for (int i = 0; i < this.list.Count; i++)
                {
                    this.list[i].Update(gameTime);
                }
            }
            base.Update(gameTime);
        }

        /// <summary>
        /// Removes all objects during gameplay e.g. an ammo or health pickup
        /// </summary>
        private void RemoveOldObjects()
        {
            for(int i = 0; i < this.removeList.Count; i++)
            {
                var obj = this.removeList[i];
                //remove from object manager i.e. from draw and update
                this.list.Remove(obj);

                //remove from physics system i.e. physics, collision detection, and response
                if (obj is CollidableObject) 
                    this.game.PhysicsManager.PhysicsSystem.RemoveBody((obj as CollidableObject).Body);
                else if (obj is ZoneObject)
                    this.game.PhysicsManager.PhysicsSystem.RemoveBody((obj as ZoneObject).Body);
            }
        }

        public override void Draw(GameTime gameTime)
        {
            if (bSplitScreenMode)
                DrawSplitScreen(gameTime);
            else
                DrawSingleScreen(gameTime);

            base.Draw(gameTime);
        }

        //Draw view from only the active camera - called when we are in single-screen mode
        private void DrawSingleScreen(GameTime gameTime)
        {
            Camera camera = this.game.CameraManager.ActiveCamera;

            if (camera.Activation != ActivationType.Off)
            {
                DrawObjects(gameTime, camera);
                DrawObjectsAlpha(gameTime, camera);
            }
        }

        //Draw views from all the cameras in the camera manager - called when we are in split screen mode
        private void DrawSplitScreen(GameTime gameTime)
        {
            Camera camera = null;
            int size = this.game.CameraManager.Size();

            for (int i = 0; i < size; i++)
            {
                camera = this.game.CameraManager[i];

                if (camera.Activation != ActivationType.Off)
                    DrawObjects(gameTime, camera);
            }
        }

        private void DrawObjects(GameTime gameTime, Camera camera)
        {
            GameObject gameObject = null;
            //Remember this code from our initial aliasing problems with the Sky box?
            //enable anti-aliasing along the edges of the quad i.e. to remove jagged edges to the primitive
            this.game.GraphicsDevice.SamplerStates[0] = SamplerState.LinearWrap;

            //enable alpha blending for transparent objects i.e. trees
            this.game.GraphicsDevice.BlendState = BlendState.AlphaBlend;

            //disable to see what happens when we disable depth buffering - look at the boxes
            this.game.GraphicsDevice.DepthStencilState = DepthStencilState.Default;

            //set the correct viewport for the camera
            this.game.GraphicsDevice.Viewport = camera.Viewport;

            //draw the objects
            for (int i = 0; i < this.list.Count; i++)
            {
                gameObject = this.list[i];

                if(gameObject is AnimatedPlayerObject)
                {
                    DrawAnimatedModel(gameObject as AnimatedPlayerObject, camera);
                }
                else if ((gameObject is CollidableObject) || (gameObject is ModelObject))
                {
                    DrawModel(gameObject as ModelObject, camera);
                }
                else if (gameObject is PrimitiveObject)
                {
                    DrawPrimitive(gameObject as PrimitiveObject, camera);
                }

                //comment out for release
                DebugDrawCollisionSkin(gameObject);
            }  
        }

        private void DrawObjectsAlpha(GameTime gameTime, Camera camera)
        {
            //GameObject gameObject = null;
            //Remember this code from our initial aliasing problems with the Sky box?
            //enable anti-aliasing along the edges of the quad i.e. to remove jagged edges to the primitive
            this.game.GraphicsDevice.SamplerStates[0] = SamplerState.LinearWrap;

            //enable alpha blending for transparent objects i.e. trees
            this.game.GraphicsDevice.BlendState = BlendState.AlphaBlend;

            //disable to see what happens when we disable depth buffering - look at the boxes
            this.game.GraphicsDevice.DepthStencilState = DepthStencilState.Default;

            //set the correct viewport for the camera
            this.game.GraphicsDevice.Viewport = camera.Viewport;

            //draw the grass

            DrawModel(alphaGrass as ModelObject, camera);
            DrawModel(this.blockGrassL as ModelObject, camera);
            DrawModel(this.blockGrassR as ModelObject, camera);

        }

        //draw a primitive i.e. vertices (and possibly indices) defined by the user
        private void DrawPrimitive(PrimitiveObject primitiveObject, Camera camera)
        {
            BasicEffect effect = primitiveObject.Effect as BasicEffect;

            //W, V, P, Apply, Draw
            effect.World = primitiveObject.GetWorldMatrix();
            effect.View = camera.View;
            effect.Projection = camera.Projection;

            if (primitiveObject.Texture != null) //e.g. VertexPositionColor vertices will have no UV coordinates - so no texture
                effect.Texture = primitiveObject.Texture;

            effect.DiffuseColor = primitiveObject.ColorAsVector3;
            effect.CurrentTechnique.Passes[0].Apply();

            primitiveObject.IVertexData.Draw(effect);
        }

        //debug method to draw collision skins for collidable objects and zone objects
        private void DebugDrawCollisionSkin(GameObject gameObject)
        {
            if (bDebugMode)
            {
                if (gameObject is ZoneObject)
                {
                    ZoneObject zoneObject = gameObject as ZoneObject;
                    this.game.PhysicsManager.DebugDrawer.DrawDebug(zoneObject.Body, zoneObject.Collision);
                }
                else if (gameObject is CollidableObject)
                {
                    CollidableObject collidableObject = gameObject as CollidableObject;
                    this.game.PhysicsManager.DebugDrawer.DrawDebug(collidableObject.Body, collidableObject.Collision);
                }
            }
        }

        //draw a model object 
        private void DrawModel(ModelObject modelObject, Camera camera)
        {
            BasicEffect effect = modelObject.Effect as BasicEffect;

            effect.View = camera.View;
            effect.Projection = camera.Projection;
            effect.Texture = modelObject.Texture;
            effect.DiffuseColor = modelObject.ColorAsVector3;

            //apply or serialise the variables above to the GFX card
            effect.CurrentTechnique.Passes[0].Apply();

            foreach (ModelMesh mesh in modelObject.ModelData.Model.Meshes)
            {
                foreach (ModelMeshPart part in mesh.MeshParts)
                {
                    part.Effect = effect;
                }
                effect.World = modelObject.ModelData.BoneTransforms[mesh.ParentBone.Index] * modelObject.GetWorldMatrix();
                mesh.Draw();
            }
        }

        Matrix[] bones;
        Effect effect;
        Model model;
        private void DrawAnimatedModel(AnimatedPlayerObject animatedPlayerObject, Camera camera)
        {
            effect = animatedPlayerObject.Effect;

            model = animatedPlayerObject.ModelData.Model;

            //an array of the current positions of the model meshes
            bones = animatedPlayerObject.AnimationPlayer.GetSkinTransforms();




            //if (game.KeyboardManager.IsKeyDown(Microsoft.Xna.Framework.Input.Keys.Q))
            //{
            //    Console.WriteLine(bones[15].Translation);

            //}

            Matrix world =  animatedPlayerObject.GetWorldMatrix();

         //   Matrix world = Matrix.CreateTranslation(new Vector3(0, -30, 0)) * animatedPlayerObject.GetWorldMatrix();

            for (int i = 0; i < bones.Length; i++)
            {
                
                bones[i] *= world;
            }

            //remember we can move this code inside the first foreach loop below
            //and use mesh.Name to change the textures applied during the effect
            effect.CurrentTechnique = effect.Techniques["SimpleTexture"];
            effect.Parameters["DiffuseMapTexture"].SetValue(animatedPlayerObject.Texture);

            effect.Parameters["Bones"].SetValue(bones);
            effect.Parameters["View"].SetValue(camera.View);
            effect.Parameters["Projection"].SetValue(camera.Projection);

            foreach (ModelMesh mesh in model.Meshes)
            {
                //move code above here to change textures and settings based on mesh.Name
                //e.g. if(mesh.Name.Equals("head"))
                //then set technique and all params
              

                foreach (ModelMeshPart part in mesh.MeshParts)
                {
              
                    part.Effect = effect;
                }
                mesh.Draw();
            }
        }

        //to do - dispose, clone
    }
}
