﻿using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace GDLibrary
{
    public class MenuItem
    {
        #region Variables
        private String name, text;
        protected Color inactiveColor, activeColor;
        private Rectangle bounds;
        private Vector2 position;
        private Color drawColor;
        #endregion

        #region Properties
        public String Text
        {
            get
            {
                return text;
            }
            set
            {
                text = value;
            }
        }
        public Rectangle Bounds
        {
            get
            {
                return bounds;
            }
        }
        public string Name
        {
            get
            {
                return name;
            }
        }
        #endregion

        public MenuItem(String name, String text, Rectangle bounds, Color inactiveColor, Color activeColor)
        {
            this.name = name;
            this.text = text;
            this.inactiveColor = inactiveColor;
            this.activeColor = activeColor;
            this.drawColor = inactiveColor;
            this.bounds = bounds;
            this.position = new Vector2(bounds.X, bounds.Y);
        }

        public void SetActive(bool bActive)
        {
            if (bActive)
                drawColor = activeColor;
            else
                drawColor = inactiveColor;
        }

        public void Draw(SpriteBatch spriteBatch, SpriteFont menuFont)
        {
            spriteBatch.DrawString(menuFont, text, position, drawColor);
        }
    }
}
