﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using GDApp;


namespace GDLibrary
{
    public class MenuManager: DrawableGameComponent
    {
        #region Variables
        private List<MenuItem> menuItemList;
        private Main game;
        private SpriteFont menuFont;

        private Texture2D[] menuTextures;
        private Rectangle textureRectangle;

        private MenuItem menuPlay, menuExit, menuAudio, menuControl;
        private MenuItem menuCurrentVolume, menuVolumeUp, menuVolumeDown, mainMenuBack, menuBack2;
        private MenuItem statement, menuExitYes, menuExitNo;

        protected int currentMenuTextureIndex = 0; //0 = main, 1 = volume
        private bool bPaused;
        private MenuItem menuOption;
        private MenuItem menuBack3;
        #endregion

        #region Properties
        public bool Pause
        {
            get
            {
                return bPaused;
            }
            set
            {
                bPaused = value;
            }
        }
        #endregion

        #region Core menu manager - No need to change this code
        public MenuManager(Main game, Texture2D[] menuTextures, SpriteFont menuFont, Integer2 textureBorderPadding) 
            : base(game)
        {
            this.game = game;

            //load the textures
            this.menuTextures = menuTextures;

            //menu font
            this.menuFont = menuFont;

            //stores all menu item (e.g. Save, Resume, Exit) objects
            this.menuItemList = new List<MenuItem>();
               
            //set the texture background to occupy the entire screen dimension, less any padding
            this.textureRectangle = this.game.ScreenRectangle;

            //deflate the texture rectangle by the padding required
            this.textureRectangle.Inflate(-textureBorderPadding.X, -textureBorderPadding.Y);

            //show the menu
            ShowMenu();
        }
        public override void Initialize()
        {
            //add the basic items - "Resume", "Save", "Exit"
           InitialiseMenuOptions();

           //show the menu screen
           ShowMainMenuScreen();

           base.Initialize();
        }
                                                                                                                               
        public void Add(MenuItem theMenuItem) 
        {
            menuItemList.Add(theMenuItem);
        }

        public void Remove(MenuItem theMenuItem)
        {
            menuItemList.Remove(theMenuItem);
        }

        public void RemoveAll()
        {
            menuItemList.Clear();
        }

        public override void Update(GameTime gameTime)
        {
            TestIfPaused();

            //menu is not paused so show and process
            if (!bPaused)
                ProcessMenuItemList();

         
            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            if (!bPaused)
            {
                //enable alpha blending on the menu objects
                this.game.SpriteBatch.Begin(SpriteSortMode.Immediate, BlendState.AlphaBlend, null, DepthStencilState.None, null);
                //draw whatever background we expect to see based on what menu or sub-menu we are viewing
                game.SpriteBatch.Draw(menuTextures[currentMenuTextureIndex], textureRectangle, Color.White);

                //draw the text on top of the background
                for (int i = 0; i < menuItemList.Count; i++)
                {
                    menuItemList[i].Draw(game.SpriteBatch, menuFont);
                }

                game.SpriteBatch.End();
            }

            base.Draw(gameTime);
        }

        private void TestIfPaused()
        {
            //if menu is pause and we press the show menu button then show the menu
            if((bPaused) && this.game.KeyboardManager.IsFirstKeyPress(KeyInfo.Key_Pause_Show_Menu))
            {
                ShowMenu();
            }
        }

        private void ShowMenu()
        {
            //show the menu by setting pause to false
            bPaused = false;
            //generate an event to tell the object manager to pause
            EventDispatcher.Publish(new EventData(this, EventData.EventType.Pause));

            //if the mouse is invisible then show it
            if (!this.game.IsMouseVisible)
                this.game.IsMouseVisible = true;

            if(game.SoundManager.isCuePlaying("shipRadio"))
            {
                game.SoundManager.PauseCue("shipRadio");
            }

            if(game.SoundManager.BattleMusic)
            {
                GameInfo.IN_BATTLE = true;
                GameInfo.IN_TV = false;
            }
            else if(game.SoundManager.TVMusic)
            {
                GameInfo.IN_TV = true;
                GameInfo.IN_BATTLE = false;
            }
            else
            {
                GameInfo.IN_BATTLE = false;
                GameInfo.IN_TV = false;
            }

            game.SoundManager.changeMusic("menuMusic");
        }

        private void HideMenu()
        {
            //hide the menu by setting pause to true
            bPaused = true;
            //generate an event to tell the object manager to unpause
            EventDispatcher.Publish(new EventData(this, EventData.EventType.Play));

            //if the mouse is invisible then show it
            if (this.game.IsMouseVisible)
                this.game.IsMouseVisible = false;

            game.SoundManager.ResumeCue("shipRadio");

            if(GameInfo.IN_TV)
            {
                game.SoundManager.changeMusic("tvMusic");
            }
            else if(GameInfo.IN_BATTLE)
            {
                game.SoundManager.changeMusic("fightMusic");
            }
            else
            {
                game.SoundManager.changeMusic("lostBackgroundMusic");
            }
        }

        private void RestartGame()
        {
            //hide the menu by setting pause to true
            bPaused = true;

            //generate an event to tell the main method to restart
            EventDispatcher.Publish(new EventData(this, EventData.EventType.Restart));

            //if the mouse is invisible then show it
            if (this.game.IsMouseVisible)
                this.game.IsMouseVisible = false;
        }

        private void ExitGame()
        {
            //generate an event to tell the main method to exit
            EventDispatcher.Publish(new EventData(this, EventData.EventType.Exit));
        }

        private void AdjustVolume(bool increase)
        {
            if(increase)
            {
                GameInfo.GAME_VOLUME += GameInfo.VOLUME_STEP;
            }
            else
            {
                GameInfo.GAME_VOLUME -= GameInfo.VOLUME_STEP;
            }
            
            GameInfo.GAME_VOLUME = MathHelper.Clamp(GameInfo.GAME_VOLUME, 0f, 1f);
        }

        //iterate through each menu item and see if it is "highlighted" or "highlighted and clicked upon"
        private void ProcessMenuItemList()
        {
           for(int i = 0; i < menuItemList.Count; i++)
           {
               MenuItem item = menuItemList[i];

               //is the mouse over the item?
               if (this.game.MouseManager.Bounds.Intersects(item.Bounds)) 
               {
                   item.SetActive(true);

                   //is the left mouse button clicked
                   if (this.game.MouseManager.IsLeftButtonClickedOnce())
                   {
                       menuAction(menuItemList[i].Name);
                       break;
                   }
               }
               else
               {
                   item.SetActive(false);
               }
           }
        }

        //to do - dispose, clone
        #endregion

        #region Code specific to your application
        private void InitialiseMenuOptions()
        {
            //add the menu items to the list
            this.menuPlay = new MenuItem(MenuInfo.Menu_Play, MenuInfo.Menu_Play,
                MenuInfo.Menu_Play_Bounds, MenuInfo.Main_Menu_Inactive_Color, MenuInfo.Menu_Active_Color);
            this.menuOption = new MenuItem(MenuInfo.Menu_Option, MenuInfo.Menu_Option,
               MenuInfo.Menu_Option_Bounds, MenuInfo.Main_Menu_Inactive_Color, MenuInfo.Menu_Active_Color);
            this.menuExit = new MenuItem(MenuInfo.Menu_Exit, MenuInfo.Menu_Exit,
                MenuInfo.Menu_Exit_Bounds, MenuInfo.Main_Menu_Inactive_Color, MenuInfo.Menu_Active_Color);


            this.menuAudio = new MenuItem(MenuInfo.Menu_Audio, MenuInfo.Menu_Audio,
              MenuInfo.Menu_Audio_Bounds, MenuInfo.Main_Menu_Inactive_Color, MenuInfo.Menu_Active_Color);
            this.menuControl = new MenuItem(MenuInfo.Menu_Control, MenuInfo.Menu_Control,
               MenuInfo.Menu_Control_Bounds, MenuInfo.Main_Menu_Inactive_Color, MenuInfo.Menu_Active_Color);



            //second menu - audio settings
            this.menuVolumeUp = new MenuItem(MenuInfo.Menu_Volume_Up, MenuInfo.Menu_Volume_Up,
                MenuInfo.Menu_Volume_Up_Bounds, MenuInfo.Main_Menu_Inactive_Color, MenuInfo.Menu_Active_Color);
            this.menuVolumeDown = new MenuItem(MenuInfo.Menu_Volume_Down, MenuInfo.Menu_Volume_Down,
               MenuInfo.Menu_Volume_Down_Bounds, MenuInfo.Main_Menu_Inactive_Color, MenuInfo.Menu_Active_Color);
            this.mainMenuBack = new MenuItem(MenuInfo.Menu_Main_Back, MenuInfo.Menu_Main_Back,
                 MenuInfo.Menu_Back_Bounds, MenuInfo.Main_Menu_Inactive_Color, MenuInfo.Menu_Active_Color);

            //third menu - control settings
            this.menuBack2 = new MenuItem(MenuInfo.Menu_Back, MenuInfo.Menu_Back,
                 MenuInfo.Menu_Back2_Bounds, MenuInfo.Main_Menu_Inactive_Color, MenuInfo.Menu_Active_Color);

            this.menuBack3 = new MenuItem(MenuInfo.Menu_Back, MenuInfo.Menu_Back,
                 MenuInfo.Menu_Back3_Bounds, MenuInfo.Main_Menu_Inactive_Color, MenuInfo.Menu_Active_Color);

            //Fourth Menu - exit
            this.menuExitYes = new MenuItem(MenuInfo.Menu_ExitYes, MenuInfo.Menu_ExitYes,
             MenuInfo.Menu_Exit_Yes_Bounds, MenuInfo.Main_Menu_Inactive_Color, MenuInfo.Menu_Active_Color);
            this.menuExitNo = new MenuItem(MenuInfo.Menu_ExitNo, MenuInfo.Menu_ExitNo,
                MenuInfo.Menu_Exit_No_Bounds, MenuInfo.Main_Menu_Inactive_Color, MenuInfo.Menu_Active_Color);

            //add your new menu options here...
        }
        //perform whatever actions are listed on the menu
        private void menuAction(String name)
        {
            if (name.Equals(MenuInfo.Menu_Play))
            {
                HideMenu();
            }
            else if (name.Equals(MenuInfo.Menu_Exit))
            {
                ShowExitMenuScreen();
            }

            else if (name.Equals(MenuInfo.Menu_Option))
            {
                ShowOptionScreen();
            }
            else if (name.Equals(MenuInfo.Menu_Audio))
            {
                ShowAudioMenuScreen();
            }
            else if (name.Equals(MenuInfo.Menu_Control))
            {
                ShowControlMenuScreen();
            }
            else if (name.Equals(MenuInfo.Menu_Back))
            {
                ShowOptionScreen();
            }
            else if (name.Equals(MenuInfo.Menu_ExitYes))
            {
                ExitGame();
            }
            else if (name.Equals(MenuInfo.Menu_ExitNo) || name.Equals(MenuInfo.Menu_Main_Back))
            {
                ShowMainMenuScreen();
            }

            else if (name.Equals(MenuInfo.Menu_Volume_Up))
            {
                AdjustVolume(true);
            }
            else if (name.Equals(MenuInfo.Menu_Volume_Down))
            {
                AdjustVolume(false);
            }

            game.SoundManager.PlayCue("menuSound");

            //add your new menu actions here...
        }

        //add your ShowXMenuScreen() methods here...

        private void ShowMainMenuScreen()
        {
            RemoveAll();
            Add(menuPlay);
            Add(menuOption);
            Add(menuExit);
            currentMenuTextureIndex = MenuInfo.Menu_Main_Texture_Index;
        }

        private void ShowOptionScreen()
        {
            RemoveAll();
            Add(menuAudio);
            Add(menuControl);
            Add(mainMenuBack);
            currentMenuTextureIndex = MenuInfo.Menu_Options_Texture_Index;
        }

        private void ShowAudioMenuScreen()
        {
            RemoveAll();
            Add(menuVolumeUp);
            Add(menuVolumeDown);
            Add(menuBack2);
            currentMenuTextureIndex = MenuInfo.Menu_Audio_Texture_Index;
        }

        private void ShowControlMenuScreen()
        {
            RemoveAll();
            Add(menuBack3);
            currentMenuTextureIndex = MenuInfo.Menu_Control_Texture_Index;
        }

        private void ShowExitMenuScreen()
        {
            RemoveAll();
            Add(menuExitYes);
            Add(menuExitNo);
            currentMenuTextureIndex = MenuInfo.Menu_Exit_Texture_Index;
        }

        #endregion
    }
}
