﻿using GDApp;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System.Collections.Generic;

namespace GDLibrary
{
    //stores all UI objects and calls update and draw
    public class UIManager : DrawableGameComponent
    {
        #region Variables
        private Main game;

        //list of objects to be updated
        private List<UIObject> list;
        //pause/unpause based on menu event
        private bool bPaused = false;

        private int health, weapon, damage, fireRate;
        private Vector2 mouseDelta;
        private UIObject creditsObj;
        #endregion

        #region Properties
        public UIObject this[int index]
        {
            get
            {
                return this.list[index];
            }
        }
        public int Health
        {
            get
            {
                return health;
            }
            set
            {
                health = value;
            }
        }

        public int Weapon
        {
            get
            {
                return weapon;
            }
            set
            {
                weapon = value;
            }
        }
        #endregion

        public UIManager(Main game)
            : base(game)
        {
            this.game = game;
            this.list = new List<UIObject>();
            this.health = GameInfo.HEALTH_BAR_WIDTH;
            this.weapon = GameInfo.WEAPON_BAR_WIDTH;
            this.damage = GameInfo.HUD_GAUGE_DAMAGE_WIDTH;
            this.fireRate = GameInfo.HUD_GAUGE_FIRE_RATE_WIDTH;
            
            //register for the menu events
            this.game.EventDispatcher.MainMenu += new MenuEventHandler(HandleMenu);
        }

        #region Event Handling
        //handle the relevant menu events
        private void HandleMenu(EventData eventData)
        {
            if (eventData.Event == EventData.EventType.Play)
                bPaused = false;
            else if (eventData.Event == EventData.EventType.Pause)
                bPaused = true;
        }
        #endregion


        public void Add(UIObject obj)
        {
            this.list.Add(obj);
        }

        public void Remove(UIObject obj)
        {
            //if the object has not already been tagged for removal
            if (this.list.Contains(obj))
            {
                //remove from the list so that it will no longer be updated
                this.list.Remove(obj);
            }
        }

        public void Remove(int pos)
        {
            if(pos < this.list.Count)
            {
                this.list.RemoveAt(pos);
            }
        }

        public int Size()
        {
            return this.list.Count;
        }

        public void Clear()
        {
            this.list.Clear();
        }

        public void addCredits(UIObject obj)
        {
            this.creditsObj = obj;
        }

        public override void Update(GameTime gameTime)
        {
            if (!bPaused)
            {
                UITextureObject obj;

                if (this.list[2] is UITextureObject)
                {
                    obj = (UITextureObject)this.list[2];

                    obj.SourceRectangle = new Rectangle(0, 0, this.health, GameInfo.HEALTH_BAR_HEIGHT);
                }

                if (this.list[3] is UITextureObject)
                {
                    obj = (UITextureObject)this.list[3];

                    obj.SourceRectangle = new Rectangle(GameInfo.WEAPON_BAR_WIDTH - this.weapon, 0, GameInfo.WEAPON_BAR_WIDTH, GameInfo.WEAPON_BAR_HEIGHT);

                    obj.Position = obj.OriginalPos + new Vector2(GameInfo.WEAPON_BAR_WIDTH - this.weapon, 0);
                }

                if(this.list.Count > 9)
                {
                    if (this.list[8] is UITextureObject)
                    {
                        obj = (UITextureObject)this.list[8];

                        obj.SourceRectangle = new Rectangle(0, 0, GameInfo.HUD_GAUGE_DAMAGE_WIDTH, GameInfo.HUD_GAUGE_DAMAGE_HEIGHT);
                    }

                    if (this.list[9] is UITextureObject)
                    {
                        obj = (UITextureObject)this.list[9];

                        obj.SourceRectangle = new Rectangle(0, 0, GameInfo.HUD_GAUGE_FIRE_RATE_WIDTH, GameInfo.HUD_GAUGE_FIRE_RATE_HEIGHT);
                    }
                }
                // More buggy than originally noticed, fix for next time
                //for (int j = 4; j <= 6; j++)
                //{
                //    if (this.list[j] is UITextureObject)
                //    {

                //        obj = (UITextureObject)this.list[j];
                //        UITextureObject obj4 = (UITextureObject)this.list[4];

                //        if (game.MouseManager.HasMoved())
                //        {
                //            this.mouseDelta -= game.MouseManager.GetDeltaFromPosition(game.ScreenCentre) * gameTime.ElapsedGameTime.Milliseconds * GameInfo.COMPASS_SPEED;

                //            //System.Diagnostics.Debug.WriteLine(this.mouseDelta);

                //            obj.Position = obj.Position + new Vector2(mouseDelta.X, 0);

                //            if (obj.Position.X < obj4.OriginalPos.X - (GameInfo.COMPASS_WIDTH))
                //            {
                //                obj.Position = new Vector2(game.ScreenCentre.X + (GameInfo.COMPASS_WIDTH * 2), 0);
                //            }
                //            else if (obj.Position.X > obj4.OriginalPos.X + (GameInfo.COMPASS_WIDTH * 2))
                //            {
                //                obj.Position = new Vector2(game.ScreenCentre.X - (GameInfo.COMPASS_WIDTH), 0);
                //            }

                //            this.mouseDelta = Vector2.Zero;

                //        }

                //        //if(obj.Position.X < obj4.Position.X)
                //        //{
                //        //    obj.SourceRectangle = new Rectangle((int)(obj4.OriginalPos.X - obj.Position.X), 0, GameInfo.COMPASS_WIDTH, GameInfo.COMPASS_HEIGHT);
                //        //}
                //        //else
                //        //{
                //        //    obj.SourceRectangle = new Rectangle(0, 0, GameInfo.COMPASS_WIDTH - (int)(obj.OriginalPos.X - obj4.Position.X), GameInfo.COMPASS_HEIGHT);
                //        //}
                //    }
                //}
                if (this.creditsObj != null)
                {
                    this.creditsObj.Update(gameTime);
                }
                for (int i = 0; i < this.list.Count; i++)
                {
                    this.list[i].Update(gameTime);
                }
                
            }
            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            if (!bPaused)
            {
                //set the correct viewport for the camera
                this.game.GraphicsDevice.Viewport = this.Game.GraphicsDevice.Viewport;

                //enable alpha blending on the ui objects
                this.game.SpriteBatch.Begin(SpriteSortMode.Immediate, BlendState.AlphaBlend,null, DepthStencilState.None, null);

                //draw the objects
                

                for (int i = 0; i < this.list.Count; i++)
                {
                    this.list[i].Draw(gameTime);
                }
               if (this.creditsObj != null)
                {
                    this.creditsObj.Draw(gameTime);
                }
                this.game.SpriteBatch.End();
            }
        }

        //to do - dispose, clone
    }
}
