﻿using GDApp;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;

namespace GDLibrary
{
    //stores all game objects and calls update and draw
    public class CameraManager : GameComponent
    {
        #region Variables
        private Main game;
        private List<Camera> list;
        //pause/unpause based on menu event
        private bool bPaused = false;
        private int activeCameraIndex = -1;
        private Camera activeCamera;
        private bool bSplitScreenMode;
        #endregion

        #region Properties
        public bool SplitScreenMode
        {
            get
            {
                return bSplitScreenMode;
            }
            set
            {
                bSplitScreenMode = value;
            }
        }
        public int ActiveCameraIndex
        {
            get
            {
                return this.activeCameraIndex;
            }
        }
        public Camera ActiveCamera
        {
            get
            {
                return activeCamera;
            }
        }
        public Camera this[int index]
        {
            get
            {
                return this.list[index];
            }
        }
        #endregion

        //single-screen mode - normal game mode
        public CameraManager(Main game)
            : this(game, false)
        {
        }

        //split-screen mode - developer mode?
        public CameraManager(Main game, bool bSplitScreenMode)
            : base(game)
        {
            this.game = game;
            this.bSplitScreenMode = bSplitScreenMode;

            this.list = new List<Camera>();

            //register for the menu events
            this.game.EventDispatcher.MainMenu += new MenuEventHandler(HandleMenu);

            //register for the camera events
            this.game.EventDispatcher.Camera += new CameraEventHandler(HandleCamera);
        }

        #region Event Handling
        //handle a camera event to set the camera
        private void HandleCamera(EventData eventData)
        {
            CameraEventData cameraEventData = eventData as CameraEventData;

            //an event requesting that the active camera index is set
            if(cameraEventData.Event == EventData.EventType.SetActiveCameraIndex)
                SetActiveCameraIndex(cameraEventData.CameraIndex);
            //an event requesting that we cycle through the cameras in the list
            else if (cameraEventData.Event == EventData.EventType.CyleActiveCameraIndex)
                CycleActiveCamera(true);

            //else if reset camera...
        }

        //handle the relevant menu events
        private void HandleMenu(EventData eventData)
        {
            if (eventData.Event == EventData.EventType.Play)
                bPaused = false;
            else if (eventData.Event == EventData.EventType.Pause)
                bPaused = true;
        }
        #endregion

        //cycles through all cameras in the list - this allows the user to see the game world from different perspectives
        public void CycleActiveCamera(bool forward)
        {
            //move to next camera
            if(forward)
            {
                this.activeCameraIndex++;
            }
            else
            {
                if(this.activeCameraIndex == 0)
                {
                    this.activeCameraIndex = this.list.Count - 1;
                }
                else
                {
                    this.activeCameraIndex--;
                }
            }
            //ensure we dont run past the end of the list by cycling to the start
            this.activeCameraIndex %= this.list.Count;
            //set new active camera
            this.activeCamera = this.list[this.activeCameraIndex];

        }
        //set active camera by passing camera index
        public void SetActiveCameraIndex(int index)
        {
            this.activeCameraIndex = (index >= 0 && index < this.Size()) ? index : 0;
            //set new active camera
            this.activeCamera = this.list[this.activeCameraIndex];
        }

        //set active camera by passing camera name
        public void SetActiveCameraIndex(string name)
        {
            int index = GetCameraIndexByName(name);
            if (index != -1)
                SetActiveCameraIndex(index);
        }
        public int Size()
        {
            return this.list.Count;
        }

        public void Clear()
        {
            this.list.Clear();
        }

        public int GetCameraIndexByName(string name)
        {
            int index = -1;

            for (int i = 0; i < this.list.Count; i++)
            {
                if (this.list[i].Name.Equals(name))
                {
                    index = i;
                    break;
                }
            }

            return index;
        }

        //add a list of camera - useful for serialisation from XML
        public void Add(List<Camera> cameraList)
        {
            for (int i = 0; i < cameraList.Count; i++)
                Add(cameraList[i]);
        }

        public void Add(Camera obj)
        {
            this.list.Add(obj);

            //if no active camera set yet (i.e. first time in) then set first camera to be active
            if (this.activeCameraIndex == -1)
                SetActiveCameraIndex(0);
        }

        public void Remove(Camera obj)
        {
            this.list.Remove(obj);
        }

        public override void Update(GameTime gameTime)
        {
            //Console.WriteLine(this.activeCamera.ToString());

            //if not paused then update cameras
            if (!bPaused)
            {
                if (bSplitScreenMode)
                    UpdateAllCameras(gameTime);
                else
                    UpdateActiveCamera(gameTime);
             
            }
            base.Update(gameTime);
        }

        //single-screen mode
        private void UpdateActiveCamera(GameTime gameTime)
        {
            this.activeCamera.Update(gameTime);
        }

        //split-screen mode
        private void UpdateAllCameras(GameTime gameTime)
        {
            for (int i = 0; i < this.list.Count; i++)
            {
                this.list[i].Update(gameTime);
            }
        }

        //to do - dispose, clone
    }
}
