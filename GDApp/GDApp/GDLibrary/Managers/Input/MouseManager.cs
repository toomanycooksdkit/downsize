﻿using GDApp;
using JigLibX.Collision;
using JigLibX.Geometry;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

using Ray = Microsoft.Xna.Framework.Ray;

namespace GDLibrary
{
    class ImmovableSkinPredicate : CollisionSkinPredicate1
    {
        public override bool ConsiderSkin(CollisionSkin skin0)
        {
            if (skin0.Owner != null)
                return true;
            else
                return false;
        }
    }

    /// <summary>
    /// Provides methods to determine the state of the mouse.
    /// </summary>
    public class MouseManager : GameComponent
    {
        #region Variables
        private Main game;
        private MouseState newState, oldState;
        #endregion

        #region Properties
        public Microsoft.Xna.Framework.Rectangle Bounds
        {
            get
            {
                return new Microsoft.Xna.Framework.Rectangle(this.newState.X, this.newState.Y, 1, 1);
            }
        }
        public bool IsVisible
        {
            get
            {
                return game.IsMouseVisible;
            }
            set
            {
                game.IsMouseVisible = value;
            }
        }
        public Vector2 Position
        {
            get
            {
                return new Vector2(this.newState.X, this.newState.Y);
            }
        }
        #endregion

        public MouseManager(Main game, bool isVisible)
            : base(game)
        {
            this.game = game;
            game.IsMouseVisible = isVisible;
        }

        /// <summary>
        /// Allows the game component to perform any initialization it needs to before starting
        /// to run.  This is where it can query for any required services and load content.
        /// </summary>
        public override void Initialize()
        {
            // TODO: Add your initialization code here

            base.Initialize();
        }
     
        /// <summary>
        /// Allows the game component to update itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        public override void Update(GameTime gameTime)
        {
            //store the old state
            this.oldState = newState;

            //get the new state
            this.newState = Mouse.GetState();

            base.Update(gameTime);
        }


        public bool HasMoved()
        {
            float deltaPositionLength = new Vector2(newState.X - oldState.X, 
                newState.Y - oldState.Y).Length();

            return (deltaPositionLength > GameInfo.Mouse_Sensitivity) ? true : false;
        }
        public bool IsLeftButtonClickedOnce()
        {
            return ((newState.LeftButton.Equals(ButtonState.Pressed)) && (oldState.LeftButton.Equals(ButtonState.Released)));
        }
        public bool IsLeftButtonClicked()
        {
            return (newState.LeftButton.Equals(ButtonState.Pressed));
        }
        public bool IsMiddleButtonPressed()
        {
            return (newState.MiddleButton.Equals(ButtonState.Pressed));
        }

        public bool IsRightButtonClickedOnce()
        {
            return ((newState.RightButton.Equals(ButtonState.Pressed)) && (oldState.RightButton.Equals(ButtonState.Released)));
        }
        public bool IsRightButtonClicked()
        {
            return (newState.RightButton.Equals(ButtonState.Pressed));
        }

        //Calculates the mouse pointer distance (in X and Y) from a user-defined position
        public Vector2 GetDeltaFromPosition(Vector2 position)
        {
            return new Vector2(this.newState.X - position.X, 
            this.newState.Y - position.Y);
        }

        //has the mouse state changed since the last update?
        public bool IsStateChanged()
        {
            return (this.newState.Equals(oldState)) ? false : true;
        }

        //how much has the scroll wheel been moved since the last update?
        public int GetScrollWheelDelta()
        {
            if (IsStateChanged()) //if state changed then return difference
                return newState.ScrollWheelValue - oldState.ScrollWheelValue;

            return 0;
        }

        public void SetPosition(Vector2 position)
        {
            Mouse.SetPosition((int)position.X, (int)position.Y);
        }

        //get a ray positioned at the mouse's location on the screen - used for picking 
        public Ray GetMouseRay(Camera camera)
        {
            //get the positions of the mouse in screen space
            Vector3 near = new Vector3(this.newState.X, this.Position.Y, 0);
            Vector3 far = new Vector3(this.newState.X, this.Position.Y, 1);

            //convert from screen space to world space
            near = camera.Viewport.Unproject(near, camera.Projection, camera.View, Matrix.Identity);
            far = camera.Viewport.Unproject(far, camera.Projection, camera.View, Matrix.Identity);

            //generate a ray to use for intersection tests
            return new Microsoft.Xna.Framework.Ray(near, Vector3.Normalize(far - near));
        }

        //get a ray from a user-defined near position in world space and the mouse pointer
        public Ray GetMouseRayFromNearPosition(Camera camera, Vector3 near)
        {
            //get the positions of the mouse in screen space
            Vector3 far = new Vector3(this.newState.X, this.Position.Y, 1);

            //convert from screen space to world space
            far = camera.Viewport.Unproject(far, camera.Projection, camera.View, Matrix.Identity);

            //generate a ray to use for intersection tests
            return new Microsoft.Xna.Framework.Ray(near, Vector3.Normalize(far - near));
        }

        public Vector3 GetMouseRayDirection(Camera camera)
        {
            //get the positions of the mouse in screen space
            Vector3 near = new Vector3(this.newState.X, this.Position.Y, 0);
            Vector3 far = new Vector3(this.newState.X, this.Position.Y, 1);

            //convert from screen space to world space
            near = camera.Viewport.Unproject(near, camera.Projection, camera.View, Matrix.Identity);
            far = camera.Viewport.Unproject(far, camera.Projection, camera.View, Matrix.Identity);

            //generate a ray to use for intersection tests
            return far - near;
        }

        float frac; CollisionSkin skin;
        Vector3 pos, normal;
        public CollidableObject GetPickedObject(Camera camera, float distance)
        {
            Vector3 ray = GetMouseRayDirection(camera);
            ImmovableSkinPredicate pred = new ImmovableSkinPredicate();

            this.game.PhysicsManager.PhysicsSystem.CollisionSystem.SegmentIntersect(out frac, out skin, out pos, out normal,
                new Segment(camera.Transform.Translation, ray * distance), pred);

            if (skin != null && (skin.Owner != null)
                && (skin.Owner.ExternalData.GetType().Equals(typeof(CollidableObject))))
                    return skin.Owner.ExternalData as CollidableObject;

            return null;
        }

        //to do - dispose, clone
    }
}