﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using GDApp;

namespace GDLibrary
{

    public class SoundManager : GameComponent
    {
        private Main game;
        private bool bkgrndMusic, menuMusic, battleMusic, tvMusic;

        protected AudioEngine audioEngine;
        protected WaveBank waveBank;
        protected SoundBank soundBank;
        protected AudioCategory musicCategory, sfxCategory, vocalsCategory;

        protected List<Cue3D> cueList;
        protected AudioListener audioListener;

        //See http://rbwhitaker.wikidot.com/audio-tutorials
        //See http://msdn.microsoft.com/en-us/library/ff827590.aspx
        //See http://msdn.microsoft.com/en-us/library/dd940200.aspx

        public bool BkgrndMusic
        {
            get
            {
                return bkgrndMusic;
            }
            set
            {
                bkgrndMusic = value;
            }
        }

        public bool TVMusic
        {
            get
            {
                return tvMusic;
            }
            set
            {
                tvMusic = value;
            }
        }

        public bool MenuMusic
        {
            get
            {
                return menuMusic;
            }
            set
            {
                menuMusic = value;
            }
        }

        public bool BattleMusic
        {
            get
            {
                return battleMusic;
            }
            set
            {
                battleMusic = value;
            }
        }

        public SoundManager(Main game, string audioEngineStr, string waveBankStr, string soundBankStr)
            : base(game)
        {
            this.game = game;
            this.audioEngine = new AudioEngine(@"" + audioEngineStr);
            this.waveBank = new WaveBank(audioEngine, @"" + waveBankStr);
            this.soundBank = new SoundBank(audioEngine, @"" + soundBankStr);
            this.cueList = new List<Cue3D>();
            this.audioListener = new AudioListener();
            this.bkgrndMusic = false;
            this.menuMusic = false;
            this.battleMusic = false;
            musicCategory = audioEngine.GetCategory("Music");
            sfxCategory = audioEngine.GetCategory("SFX");
            vocalsCategory = audioEngine.GetCategory("Vocals");
         }

        public override void Initialize()
        {
            base.Initialize();
        }

        // Plays a non-3D cue e.g menu, game music etc
        public void PlayCue(String cueName)
        {
            this.soundBank.PlayCue(cueName);
        }

        // Plays a cue to be heard from the perspective of a player or camera in the game i.e. in 3D
        public void Play3DCue(String cueName, AudioEmitter audioEmitter)
        {
            Cue3D sound = new Cue3D();
            sound.Cue = soundBank.GetCue(cueName);
            sound.Emitter = audioEmitter;
            sound.Cue.Apply3D(audioListener, audioEmitter);
            sound.Cue.Play();
            this.cueList.Add(sound);
        }

        public void apply3D(string name, AudioEmitter audioEmitter)
        {
            for (int i = 0; i < cueList.Count; i++)
            {
                if (cueList[i].Cue.Name.Equals(name))
                {

                    cueList[i].Cue.Apply3D(audioListener, audioEmitter);
                }
            }
        }

        public bool isCuePlaying(string name)
        {
            for (int i = 0; i < cueList.Count; i++)
            {
                if (cueList[i].Cue.Name.Equals(name))
                {
                    if (cueList[i].Cue.IsPlaying)
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        public void PauseCue(string name)
        {
            for (int i = 0; i < cueList.Count; i++)
            {
                if(cueList[i].Cue.Name.Equals(name))
                {
                    cueList[i].Cue.Pause();
                }
            }
        }

        public void StopCue(string name)
        {
            for (int i = 0; i < cueList.Count; i++)
            {
                if (cueList[i].Cue.Name.Equals(name))
                {
                    cueList[i].Cue.Stop(AudioStopOptions.Immediate);
                }
            }
        }

        public bool ResumeCue(string name)
        {
            for (int i = 0; i < cueList.Count; i++)
            {
                if (cueList[i].Cue.Name.Equals(name))
                {
                    if(cueList[i].Cue.IsPaused)
                    {
                        cueList[i].Cue.Resume();
                        return true;
                    }
                }
            }

            return false;
        }

        //Called by the listener to update relative positions
        public void UpdateListenerPosition(Vector3 position)
        {
            this.audioListener.Position = position;
        }

        public override void Update(GameTime gameTime)
        {
            UpdateSoundVolume();
            this.audioEngine.Update();
            for (int i = 0; i < cueList.Count; i++)
            {
                if (this.cueList[i].Cue.IsStopped)
                    this.cueList.RemoveAt(i--);
                else
                    this.cueList[i].Cue.Apply3D(audioListener, this.cueList[i].Emitter);
            }
            base.Update(gameTime);
        }

        public void UpdateSoundVolume()
        {
            musicCategory.SetVolume(GameInfo.GAME_VOLUME);
            sfxCategory.SetVolume(1f);
            vocalsCategory.SetVolume(1f);

        }


        public void changeMusic(string name)
        {

            game.SoundManager.BkgrndMusic = false;
            game.SoundManager.BattleMusic = false;
            game.SoundManager.MenuMusic = false;
            game.SoundManager.TVMusic = false;

            if(name.Equals("lostBackgroundMusic"))
            {
                BkgrndMusic = true;
            }
            else if(name.Equals("fightMusic"))
            {
                BattleMusic = true;
            }
            else if(name.Equals("menuMusic"))
            {
                MenuMusic = true;
            }
            else if(name.Equals("tvMusic"))
            {
                TVMusic = true;
            }
        }
        //to do - dispose, clone
    }
}
