﻿using System;
using Microsoft.Xna.Framework;


namespace GDLibrary
{
    public class MenuInfo
    {
        #region MENU_STRINGS;
        //all the strings shown to the user through the menu
        //title screen - 3
        public static String Menu_Play = "Play";
        public static string Menu_Option = "Option";
        public static String Menu_Exit = "Exit";

        //Option -3
        public static string Menu_Control = "Control";
        public static String Menu_Audio = "Audio";
        public static String Menu_Main_Back = "Return";



        // sound - 3
        public static String Menu_Volume_Up = "Volume Up";
        public static String Menu_Volume_Down = "Volume Down";
        public static String Menu_Back = "Back";

        // exit - 3
        public static string Menu_ExitYes = "Yes";
        public static string Menu_ExitNo = "No";

        //Pause -3
        public static string Menu_Resume = "Resume";
        public static string Menu_ExitStatement = "Exit";
        #endregion;


        #region Colours, Array Indices and Bounds
        //the hover colours for menu items
        public static Color Main_Menu_Inactive_Color = Color.White;
        public static Color Menu_Inactive_Color = Color.Black;
        public static Color Menu_Active_Color = Color.Red;


        //the position of the texture in the array of textures provided to the menu manager
        public static int Menu_Main_Texture_Index = 0;
        public static int Menu_Options_Texture_Index = 1;
        public static int Menu_Audio_Texture_Index = 2;
        public static int Menu_Control_Texture_Index = 3;
        public static int Menu_Pause_Texture_Index = 4;
        public static int Menu_Exit_Texture_Index = 5;

         public static float scaleMoveSides = 1.6f;
         public static float scaleMoveDown = 1.53f;
        //bounding rectangles used to detect mouse over
        public static Rectangle Menu_Play_Bounds = new Rectangle((int)(580*scaleMoveSides), (int)(280*scaleMoveDown), 70, 40); //x, y, width, height
        public static Rectangle Menu_Option_Bounds = new Rectangle((int)(580*scaleMoveSides), (int)(380*scaleMoveDown), 90, 40);
        public static Rectangle Menu_Exit_Bounds = new Rectangle((int)(580*scaleMoveSides),(int)( 480*scaleMoveDown), 70, 40);

        public static Rectangle Menu_Audio_Bounds = new Rectangle((int)(85*scaleMoveSides), (int)(185*scaleMoveDown), 90, 40);
        public static Rectangle Menu_Control_Bounds = new Rectangle((int)(70*scaleMoveSides), (int)(310*scaleMoveDown), 90, 40);
        public static Rectangle Menu_Back_Bounds = new Rectangle((int)(70*scaleMoveSides), (int)(440*scaleMoveDown), 70, 40);

        //bounding rectangles used to detect mouse over music/control
        public static Rectangle Menu_Volume_Up_Bounds = new Rectangle((int)(50*scaleMoveSides),(int)( 150*scaleMoveDown), 150, 40);
        public static Rectangle Menu_Volume_Down_Bounds = new Rectangle((int)(50*scaleMoveSides), (int)(290*scaleMoveDown), 180, 40);
        public static Rectangle Menu_Back2_Bounds = new Rectangle((int)(50*scaleMoveSides), (int)(430*scaleMoveDown), 70, 40);

        public static Rectangle Menu_Back3_Bounds = new Rectangle((int)(520*scaleMoveSides), (int)(500*scaleMoveDown), 70, 40);

        //bounding rectangles used to detect mouse over exit
        public static Rectangle Menu_Exit_Yes_Bounds = new Rectangle((int)(385*scaleMoveSides), (int)(295*scaleMoveDown), 50, 40);
        public static Rectangle Menu_Exit_No_Bounds = new Rectangle((int)(395 * scaleMoveSides), (int)(377 * scaleMoveDown), 35, 40);

        #endregion


        #region UI Menu
        public static String UI_Menu_StartLog = "Start Log:";
        public static string UI_Menu_Mission1 = "1) Locate\nFuse (TV)";
        public static string UI_Menu_EndMission1 = "Return to\nthe ship";
        public static string UI_Menu_ReturnedMission1 = "Ships power\nrestored.";
        public static string UI_Menu_EndLog = "End Log.";
        public static string UI_Menu_Weapon_Stats = "Ammo Stats:";
        public static string UI_Menu_Damage = "Damage:";
        public static string UI_Menu_Fire_Rate = "Fire Rate:";

        public static Rectangle UI_Menu_StartLog_Bounds = new Rectangle(10, 690, 90, 20);
        public static Rectangle UI_Menu_Mission1_Bounds = new Rectangle(10, 710, 120, 40);
        public static Rectangle UI_Menu_EndLog_Bounds = new Rectangle(10, 750, 90, 20);
        public static Rectangle UI_Menu_Weapon_Stats_Bounds = new Rectangle(1165, 690, 90, 20);
        public static Rectangle UI_Menu_Damage_Bounds = new Rectangle(1165, 710, 90, 20);
        public static Rectangle UI_Menu_Fire_Rate_Bounds = new Rectangle(1165, 770, 90, 20);
        #endregion
    }
}
