﻿using Microsoft.Xna.Framework;

namespace GDLibrary
{
    public class GameInfo
    {
        //move and strafe speed multiplier applied to the camera
        public static float Camera_Move_Speed = 0.08f;
        public static float Camera_Strafe_Speed = 0.06f;
        public static float Camera_Rotation_Speed = MathHelper.ToRadians(0.01f);
        public static float Camera_Bounding_Sphere_Radius = 4;

        //move and strafe speed multiplier applied to the player
        public static float Player_Move_Speed = 3f;
        public static float Player_Strafe_Speed = 2f;
        public static float Player_Rotation_Speed = 2f; //in degrees - See Transform3D::rotateBy()
        public static float Player_Jump_Height = 12;

        //move speed multiplier applied to the ant
        public static float Ant_Move_Speed = 2f;
        


        public static float Mouse_Sensitivity = 1;
        public static float SkyDome_Rotation_Speed = MathHelper.ToRadians(0.006f);

        public static int HEALTH_BAR_HEIGHT = 62;
        public static int HEALTH_BAR_WIDTH = 163;

        public static int WEAPON_BAR_HEIGHT = 62;
        public static int WEAPON_BAR_WIDTH = 163;

        public static int COMPASS_HEIGHT = 47;
        public static int COMPASS_WIDTH = 283;

        public static float COMPASS_SPEED = MathHelper.Pi / COMPASS_WIDTH;

        public static float LEFT_HUD_BORDER_HEIGHT = 346;
        public static float LEFT_HUD_BORDER_WIDTH = 180;
        public static float LEFT_HUD_BORDER_Y_SCALE = 0.5f;
        public static float LEFT_HUD_BORDER_X_SCALE = GameInfo.HEALTH_BAR_WIDTH / GameInfo.LEFT_HUD_BORDER_WIDTH;

        public static int HUD_GAUGE_DAMAGE_HEIGHT = 50;
        public static int HUD_GAUGE_DAMAGE_WIDTH = 30;
        public static int HUD_GAUGE_FIRE_RATE_HEIGHT = 50;
        public static int HUD_GAUGE_FIRE_RATE_WIDTH = 60;
        public static float HUD_GAUGE_X_SCALE = 1;
        public static float HUD_GAUGE_Y_SCALE = 1;

        //gun fire rate speed will be changet with auguments
        public static readonly int GUN_FIRE_RATE_MS = 500;
        public static readonly int BULLET_DAMAGE = 1;

        public static float GAME_VOLUME = 0.5f;
        public static readonly float VOLUME_STEP = 0.05f;

        public static bool IN_BATTLE = false;
        public static bool IN_TV = false;

        public static double RADIO = 6000;

    }
}
