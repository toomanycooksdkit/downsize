﻿
using Microsoft.Xna.Framework;
namespace GDLibrary
{
    public class TextureInfo
    {
        //source rectangles for mouse icons - see Main::InitialiseUIObjects()
        public static Rectangle SOURCE_RECTANGLE_MOUSE_CROSS_HAIR = new Rectangle(0, 0, 128, 128);
        public static Rectangle SOURCE_RECTANGLE_MOUSE_SQUARE = new Rectangle(128, 0, 128, 128);
        public static Rectangle SOURCE_RECTANGLE_MOUSE_CROSS_HAIR_RETICULE = new Rectangle(256, 0, 128, 128);
        public static Rectangle SOURCE_RECTANGLE_MOUSE_ICON = new Rectangle(0, 0, 101, 101);
        //source rectangles for HUD
        public static Rectangle SOURCE_RECTANGLE_HUD_HEALTH_BAR = new Rectangle(0, 0, GameInfo.HEALTH_BAR_WIDTH, GameInfo.HEALTH_BAR_HEIGHT);
        public static Rectangle SOURCE_RECTANGLE_HUD_WEAPON_BAR = new Rectangle(0, 0, GameInfo.WEAPON_BAR_WIDTH, GameInfo.WEAPON_BAR_HEIGHT);
        public static Rectangle SOURCE_RECTANGLE_HUD_COMPASS = new Rectangle(0, 0, GameInfo.COMPASS_WIDTH, GameInfo.COMPASS_HEIGHT);
        public static Rectangle SOURCE_RECTANGLE_HUD_GAUGE_DAMAGE = new Rectangle(0, 0, GameInfo.HUD_GAUGE_DAMAGE_WIDTH, GameInfo.HUD_GAUGE_DAMAGE_HEIGHT);
        public static Rectangle SOURCE_RECTANGLE_HUD_GAUGE_FIRE_RATE = new Rectangle(0, 0, GameInfo.HUD_GAUGE_FIRE_RATE_WIDTH, GameInfo.HUD_GAUGE_FIRE_RATE_HEIGHT);
        public static Rectangle SOURCE_RECTANGLE_CUTSCENE = new Rectangle(0, 0, 0, 0);
    }
}
