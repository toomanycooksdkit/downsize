﻿using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GDLibrary
{
    public class KeyInfo
    {
        public static Keys[] Key_Array_Free_Look_Camera = { Keys.Up, Keys.Down, Keys.Left, Keys.Right };
        public static Keys[] Key_Array_Player = { Keys.W, Keys.S, Keys.A, Keys.D, Keys.Space, Keys.NumPad0, Keys.NumPad1};
        public static ushort Key_Array_Index_Forward = 0;
        public static ushort Key_Array_Index_Backward = 1;
        public static ushort Key_Array_Index_Left = 2;
        public static ushort Key_Array_Index_Right = 3;
        public static ushort Key_Array_Index_Jump = 4;
        public static ushort Key_Array_Index_RotateLeft = 5;
        public static ushort Key_Array_Index_RotateRight = 6;

        //press Escape to pause/unpause the menu
        public static Keys Key_Pause_Show_Menu = Keys.P;
        public static Keys Key_Pause_Show_UI_Menu = Keys.LeftControl;
    }
}
