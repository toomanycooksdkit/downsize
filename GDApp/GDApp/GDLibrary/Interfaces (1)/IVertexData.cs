﻿using Microsoft.Xna.Framework.Graphics;

namespace GDLibrary
{
    public interface IVertexData
    {
        void Draw(Effect effect);
    }
}
