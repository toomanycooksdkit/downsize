﻿using GDApp;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;


namespace GDLibrary
{
    public class UIMouseObject : UIObject
    {
        #region Variables
        private Texture2D texture;
        private Rectangle sourceRectangle;
        private Color color;
        private float rotation;
        private float scale;
        private Vector2 origin;

        //local vars
        private Vector2 position;
        private SpriteFont font;
        private CollidableObject pickedObject;
        private Vector2 textOffset;
        private Main game;



        #endregion

        #region Properties
        public CollidableObject PickedObject
        {
            get { return pickedObject; }
            set { pickedObject = value; }
        }
        #endregion

        

        public UIMouseObject(Main game, SpriteFont font, Texture2D texture, Rectangle sourceRectangle, Color color,
            float rotation, float scale)
            : base(game, null)
        {
            this.game = game;
            this.texture = texture;
            this.sourceRectangle = sourceRectangle;
            this.color = color;
            this.rotation = rotation;
            this.origin = new Vector2(sourceRectangle.Width / 2, sourceRectangle.Height / 2);
            this.scale = scale;

            this.font = font;
            this.textOffset = new Vector2(-40, -45);
        }

        public override void Update(GameTime gameTime)
        {
            //DoPick();
        }
        public override void Draw(GameTime gameTime)
        {

            if (game.MouseManager.IsRightButtonClicked())
            {
                position = this.Game.MouseManager.Position;
                this.Game.SpriteBatch.Draw(texture, position, sourceRectangle, color, rotation,
                    origin, scale, SpriteEffects.None, 1);
            }
            if (this.pickedObject != null)
                this.Game.SpriteBatch.DrawString(font, "Target: " + this.pickedObject.ObjectType, position + this.textOffset, this.color);
        }

        public void CursorOnPickup()
        {
            this.rotation += MathHelper.ToRadians(1);
            this.color = Color.RoyalBlue;
        }

        public void CursorOnMissionPickup()
        {
            this.rotation += MathHelper.ToRadians(1);
            //this.scale += 0.01f;
            this.color = Color.LawnGreen;
        }

        public void CursorNormall()
        {
            this.rotation = 0;
            this.color = Color.MediumAquamarine;
        }


        private void DoPick()
        {
            this.pickedObject = this.Game.MouseManager.GetPickedObject(this.Game.CameraManager.ActiveCamera, 1000);

            //valid and pickup type - this could also be ammo, health etc
            if (this.pickedObject != null && this.pickedObject.ObjectType == ObjectType.MissionPickup)
            {
                this.CursorOnMissionPickup();
            }
            else
            {
                this.CursorNormall();
                this.pickedObject = null;
            }
        }

    }
}
