﻿using GDApp;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GDLibrary
{
    public class UIObject : ICloneable
    {
        #region Variables
        private Main game;
        private Transform2D transform;
        #endregion

        #region Properties
        public Transform2D Transform
        {
            get
            {
                return transform;
            }
            set
            {
                transform = value;
            }
        }
        protected Main Game
        {
            get
            {
                return game;
            }
        }
        #endregion

        public UIObject(Main game, Transform2D transform)
        {
            this.game = game;
            this.transform = transform;
        }

        public virtual void Update(GameTime gameTime)
        {

        }

        public virtual void Draw(GameTime gameTime)
        {

        }

        public object Clone()
        {
            return new UIObject(this.game, (Transform2D)this.transform.Clone());
        }
    }
}
