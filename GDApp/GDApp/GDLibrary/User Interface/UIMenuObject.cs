﻿using GDApp;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;

namespace GDLibrary
{
    /// <summary>
    /// Draws a list of menu items on screen and performs an action based on the option chosen from left mouse click.
    /// </summary>
    public class UIMenuObject : UIObject
    {
        private List<MenuItem> menuList;
        private SpriteFont menuFont;
        private Color menuTextureBlendColor;
        private Texture2D menuTexture;
        private bool bPaused;

        public UIMenuObject(Main game, Transform2D transform,
            Texture2D menuTexture, SpriteFont menuFont, Color menuTextureBlendColor)
            : base(game, transform)
        {
            this.menuTexture = menuTexture;
            this.menuFont = menuFont;
            this.menuTextureBlendColor = menuTextureBlendColor;

            this.menuList = new List<MenuItem>();
        }

        public void Add(MenuItem menuItem)
        {
            //test if already present - this is where MenuItem::Equals and GetHashCode are useful
            if (!this.menuList.Contains(menuItem))
                this.menuList.Add(menuItem);
        }

        public void Remove(MenuItem menuItem)
        {
            if (this.menuList.Contains(menuItem))
                this.menuList.Remove(menuItem);
        }

        public void Clear()
        {
            this.menuList.Clear();
        }

        public override void Update(GameTime gameTime)
        {
            TestIfPaused();

            if (!bPaused)
            {
                //get clicked menu item
                MenuItem menuItem = MenuUtility.GetClickedMenuItem(this.menuList, this.Game.MouseManager);

                if (menuItem != null)
                {
                    DoMenuAction(menuItem.Name);

                    //to hide menu uncomment this line
                    //bPaused = !bPaused;
                }
            }

            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            if (!bPaused)
            {
                //draw whatever background we expect to see based on what menu or sub-menu we are viewing
                this.Game.SpriteBatch.Draw(menuTexture,
                    this.Transform.Translation, null, this.menuTextureBlendColor,
                    this.Transform.Rotation, Vector2.Zero, this.Transform.Scale, SpriteEffects.None, 1);

                //draw the text on top of the background
                for (int i = 0; i < this.menuList.Count; i++)
                {
                    this.menuList[i].Draw(this.Game.SpriteBatch, menuFont);
                }
            }
            base.Draw(gameTime);
        }


        private void TestIfPaused()
        {
            //if menu is pause and we press the show menu button then show the menu
            if (this.Game.KeyboardManager.IsFirstKeyPress(KeyInfo.Key_Pause_Show_UI_Menu))
            {
                bPaused = !bPaused;
                Game.SoundManager.PlayCue("menuSound");
            }   
        }

        //perform whatever actions are listed on the menu
        private void DoMenuAction(String name)
        {
            EventData eventData = new EventData(this);

            if (name.Equals(MenuInfo.UI_Menu_StartLog))
            {
                eventData.Event = EventData.EventType.AddHouse;
            }
            else if (name.Equals(MenuInfo.UI_Menu_Mission1))
            {
                eventData.Event = EventData.EventType.AddBarracks;
            }
            else if (name.Equals(MenuInfo.UI_Menu_EndLog))
            {
                eventData.Event = EventData.EventType.AddFence;
            }

            EventDispatcher.Publish(eventData);
        }
    }
}
