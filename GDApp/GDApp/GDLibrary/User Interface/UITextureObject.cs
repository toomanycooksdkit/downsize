﻿using GDApp;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GDLibrary
{
    public class UITextureObject : UIObject
    {

        private Texture2D texture;
        private Rectangle sourceRectangle;
        private Color color;
        private float rotation;
        private float scale;
        private Vector2 origin;
        private Vector2 position, originalPos;
        private SpriteFont font;
        private Vector2 textOffset;
        private Main game;
        private bool bPaused;

        public Rectangle SourceRectangle
        {
            get
            {
                return sourceRectangle;
            }
            set
            {
                sourceRectangle = value;
            }
        }
        public Vector2 OriginalPos
        {
            get
            {
                return originalPos;
            }
        }

        public Vector2 Position
        {
            get
            {
                return position;
            }
            set
            {
                position = value;
            }
        }

        public UITextureObject(Main game, Texture2D texture, Rectangle sourceRectangle, Color color, float rotation, float scale, SpriteFont font, Vector2 position)
            : base(game, null)
        {
            this.game = game;
            this.texture = texture;
            this.sourceRectangle = sourceRectangle;
            this.color = color;
            this.rotation = rotation;
            this.origin = new Vector2(sourceRectangle.Width / 2, sourceRectangle.Height / 2);
            this.scale = scale;
            this.originalPos = position;
            this.position = position;
            this.font = font;
            this.textOffset = new Vector2(-40, -45);
        }

        public override void Draw(GameTime gameTime)
        {
            this.Game.SpriteBatch.Draw(texture, position, sourceRectangle, color, rotation, origin, scale, SpriteEffects.None, 1);
        }

    }
}
