﻿using GDApp;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace GDLibrary
{
    public class UIDebugObject : UIObject
    {
        private SpriteFont font;
        private int horizontalOffset, verticalOffset;

        //local vars
        private Camera camera = null;
        private Vector2 textPos;
        private Color color;
        string text;

        public UIDebugObject(Main game, 
            int horizontalOffset, int verticalOffset, 
            SpriteFont font, Color color)
            : base(game, null)
        {
            this.horizontalOffset = horizontalOffset;
            this.verticalOffset = verticalOffset;

            this.font = font;
            this.color = color;
        }

        public override void Draw(GameTime gameTime)
        {
            //camera = this.Game.CameraManager.ActiveCamera;
            //text = camera.Name + ":" + MathUtility.Round(camera.Transform.Translation);
            //textPos = new Vector2(camera.Viewport.X + horizontalOffset, camera.Viewport.Y + verticalOffset);
            //this.Game.SpriteBatch.DrawString(font, text, textPos, color);
        }

    }
}
