﻿using GDApp;
using GDLibrary;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;

namespace GDLibrary
{
    public class GameObject : ICloneable
    {
        #region Variables
        private Main game;
        private Transform3D transform;

        //all objects are by default decorators unless we specify when we construct
        private ObjectType objectType = ObjectType.Decorator;
        #endregion

        #region Properties
        public Transform3D Transform
        {
            get
            {
                return transform;
            }
            set
            {
                transform = value;
            }
        }
        protected Main Game
        {
            get
            {
                return game;
            }
        }
        public ObjectType ObjectType
        {
            get
            {
                return objectType;
            }
            set
            {
                objectType = value;
            }
        }
        #endregion

        public GameObject(Main game, Transform3D transform)
        {
            this.game = game;
            this.transform = transform;
        }

        public virtual void Update(GameTime gameTime)
        {

        }

        //call to remove an object from the object manager and, where necessary, the physics system
        public virtual void Remove()
        {
            this.game.ObjectManager.Remove(this);
        }

        public virtual void Draw(GameTime gameTime, Camera camera)
        {

        }

        public object Clone()
        {
            return new GameObject(this.game, (Transform3D)this.transform.Clone());
        }

        //to do - intersects (frustum and ray), dispose
        public virtual bool Intersects(Ray ray)
        {
            return false;
        }

        public virtual bool Intersects(BoundingFrustum frustum)
        {
            return false;
        }

        public virtual bool Intersects(BoundingSphere sphere)
        {
            return false;
        }

        public virtual Matrix GetWorldMatrix()
        {
            return this.Transform.World;
        }

    }
}
