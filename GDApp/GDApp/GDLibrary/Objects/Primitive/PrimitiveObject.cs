﻿using GDApp;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;

namespace GDLibrary
{
    public class PrimitiveObject : GameObject, ICloneable
    {
        #region Variables
        private Effect effect;
        private Texture2D texture;
        private Color color;
        private Vector3 colorAsVector3;
        private IVertexData vertexData;
        #endregion

        #region Properties
        public IVertexData IVertexData
        {
            get
            {
                return vertexData;
            }
            set
            {
                vertexData = value;
            }
        }
        public Effect Effect
        {
            get
            {
                return effect;
            }
            set
            {
                effect = value;
            }
        }
        public Texture2D Texture
        {
            get
            {
                return texture;
            }
            set
            {
                texture = value;
            }
        }
        public Color Color
        {
            get
            {
                return color;
            }
            set
            {
                color = value;
                colorAsVector3 = color.ToVector3();
            }
        }
        public Vector3 ColorAsVector3
        {
            get
            {
                return colorAsVector3;
            }
        }
        #endregion

        //used to draw primitives that do not have a texture i.e. use VertexPositionColor vertex types only
        public PrimitiveObject(Main game, Transform3D transform, Effect effect, IVertexData vertexData, Color color)
            : this(game, transform, effect, vertexData, null, color)
        {
        }

        //used to draw primitives that have a texture 
        public PrimitiveObject(Main game, Transform3D transform, Effect effect, IVertexData vertexData, Texture2D texture, Color color)
            : base(game, transform)
        {
            this.effect = effect;
            this.vertexData = vertexData;
            this.texture = texture;
            this.Color = color;
        }

        public new object Clone()
        {
            return new PrimitiveObject(this.Game, (Transform3D)this.Transform.Clone(), this.effect, this.vertexData, this.texture, this.color);
        }
    }
}
