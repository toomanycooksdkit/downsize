﻿using GDLibrary;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GDApp.GDLibrary.Objects.ADownsizeObjects
{
    public class MoveableModelObject : ModelObject
    {
        private Main game;
        private Vector2 mouseDelta;
        private Matrix cameraRotation;
        private Matrix objectRotation;
        private float rotationSpeed;



        public MoveableModelObject(Main game, Transform3D transform, BasicEffect effect, 
            ModelData modelData, Texture2D texture, Color color)
            : base(game, transform,effect,modelData,texture,color)
        {
            this.game = game;
            rotationSpeed = MathHelper.ToRadians(1f);
            game.MouseManager.SetPosition(game.ScreenCentre);
        }


        public override void Update(GameTime gameTime)
        {

            Move(gameTime);
            HandleMouseInput(gameTime);
            base.Update(gameTime);
            this.game.MouseManager.SetPosition(game.ScreenCentre); //ask which place is better
        }


        private void HandleMouseInput(GameTime gameTime)
        {
            //update rotation if the mouse position changes
            if (game.MouseManager.HasMoved())
            {
                this.mouseDelta -= game.MouseManager.GetDeltaFromPosition(game.ScreenCentre) * gameTime.ElapsedGameTime.Milliseconds * this.rotationSpeed;
                //calculate matrix rotation
                this.objectRotation = Matrix.CreateFromYawPitchRoll(mouseDelta.X, 0, 0);
                this.Transform.RotateBy(mouseDelta.X);

                this.mouseDelta = Vector2.Zero;

            }
        }

        private void Move(GameTime gameTime)
        {
            if (game.KeyboardManager.IsKeyDown(Keys.Up))
            {
                this.Transform.MoveBy(0.02f * gameTime.ElapsedGameTime.Milliseconds * this.Transform.Look);
                //this.Color = Color.Red;
            }
            else if (game.KeyboardManager.IsKeyDown(Keys.Down))
            {
                this.Transform.MoveBy(-0.02f * gameTime.ElapsedGameTime.Milliseconds * this.Transform.Look);
                //this.Color = Color.Blue;
            }

            if (game.KeyboardManager.IsKeyDown(Keys.Left))
                this.Transform.MoveBy(-0.02f * gameTime.ElapsedGameTime.Milliseconds * this.Transform.Right);
            else if (game.KeyboardManager.IsKeyDown(Keys.Right))
                this.Transform.MoveBy(0.02f * gameTime.ElapsedGameTime.Milliseconds * this.Transform.Right);

            if (game.KeyboardManager.IsKeyDown(Keys.O))
                this.Transform.RotateBy(2f);
            else if (game.KeyboardManager.IsKeyDown(Keys.I))
                this.Transform.RotateBy(-2f);

        }


    }
}
