﻿using GDLibrary;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GDApp.GDLibrary.Objects.ADownsizeObjects
{
    //this will be all the objects which are around the world not mowing Eg: TV
    public class WorldCollidableObject : CollidableObject
    {

        public WorldCollidableObject(Main game,
            Transform3D transform, BasicEffect effect, ModelData modelData, Texture2D texture, Color color)
            : base(game, transform, effect, modelData, texture, color)
        {
            
        }

    }
}
