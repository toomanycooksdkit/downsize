﻿using GDLibrary;
using JigLibX.Collision;
using JigLibX.Geometry;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GDApp.GDLibrary.Objects.ADownsizeObjects
{
    public class WorldTriangleMeshObject : TriangleMeshObject
    {

        public WorldTriangleMeshObject(Main game,
            Transform3D transform, BasicEffect effect, ModelData modelData, 
            Texture2D texture, Color color, MaterialProperties materialProperties)
            : base(game, transform, effect, modelData, texture, color, materialProperties)
        {
           
        }

    }
}
