﻿using GDLibrary;
using JigLibX.Collision;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GDApp.GDLibrary.Objects.ADownsizeObjects
{
    public class BulletCollidableObject : CollidableObject
    {
        private Main game;
        private Vector3 direction;


        private double disposeTime;
        private static double DISPOSE_TIME = 2000;
        private float scaleFactor;
        private int damega;
        //private Ray directionRay;

        public int Damega
        {
            get { return damega; }
            set { damega = value; }
        }

        public Vector3 Direction
        {
            get { return direction; }
            set { direction = value; }
        }
        public float ScaleFactor
        {
            get { return scaleFactor; }
            set { scaleFactor = value; }
        }
 

        public BulletCollidableObject(Main game, Transform3D transform, Effect effect, ModelData modelData, Texture2D texture, Color color, float scaleFactor)
            : base(game, transform, effect, modelData, texture, color)
        {
            this.disposeTime = DISPOSE_TIME;
            this.game = game;
            this.damega = GameInfo.BULLET_DAMAGE;

            this.Body.CollisionSkin.callbackFn += CollisionSkin_callbackFn;
            this.scaleFactor = scaleFactor;
            
        }

        bool CollisionSkin_callbackFn(CollisionSkin collider, CollisionSkin collidee)
        {
            if (collider.Owner.ExternalData is BulletCollidableObject)
            {
                //enable these lines after you have observed the game running once
                BulletCollidableObject c = (BulletCollidableObject)collider.Owner.ExternalData;
            }

            if (collidee.Owner.ExternalData is EnemyCollidableObject)
            {
                EnemyCollidableObject c = (EnemyCollidableObject)collidee.Owner.ExternalData;
                c.TakeDamage(GameInfo.BULLET_DAMAGE);
            }

            return true;
        }


        public override void Update(GameTime gameTime)
        {

            MovementOfBullet(gameTime);
            DisposeAfterCertainTime(gameTime);
            base.Update(gameTime);
        }

        private void MovementOfBullet(GameTime gameTime)
        {
            direction.Normalize();
            this.Body.Velocity += direction + new Vector3(0,0.174f,0);
        }


        private void DisposeAfterCertainTime(GameTime gameTime)
        {
            disposeTime -= gameTime.ElapsedGameTime.TotalMilliseconds;
            if (disposeTime <= 0)
            {
                this.Remove();
                disposeTime = DISPOSE_TIME; //reset time
            }
        }

        public new object Clone()
        {

            return new BulletCollidableObject(this.game, (Transform3D)this.Transform.Clone(), this.Effect, this.ModelData, this.Texture, this.Color, this.scaleFactor);


        }

    }
}
