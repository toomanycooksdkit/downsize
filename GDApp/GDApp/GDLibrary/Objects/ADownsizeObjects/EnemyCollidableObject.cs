﻿using GDLibrary;
using JigLibX.Collision;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GDApp.GDLibrary.Objects.ADownsizeObjects
{
    public class EnemyCollidableObject : CharacterObject
    {
        private Main game;
        private float health;
        private PathFindingEngine pathEngine;
        Vector3 lookToPosition;
        Vector3 movingToPos;
        private bool lookLocked;
        private float distance;
        private int atNode;
        private List<Node> pathNodeList;
        private string[] nodeNames;
        private string startOfNewPath;
        private static Random rand = new Random();
        private bool playerInZone;
        private PlayerCollidableObject player;
        private double respawnRate;
        private static double staticRespawnRate = 1000;
        private double hitTime;
        private static double HIT_TIME = 3000;
        private AudioEmitter audioEmitter;

        public EnemyCollidableObject(Main game,
            Transform3D transform, BasicEffect effect, ModelData modelData, Texture2D texture, Color color, PathFindingEngine pathEngine, string[] nodeNames,float radius, float height, float accelerationRate, float decelerationRate)
            : base(game, transform, effect, modelData, texture, color,radius,  height, accelerationRate,  decelerationRate)
        {
            this.health = 50;
            this.game = game;
            this.pathEngine = pathEngine;
            this.lookLocked = false;
            this.atNode = 0;
            this.nodeNames = nodeNames;
            this.pathNodeList = null;
            this.startOfNewPath = nodeNames[0];

            this.respawnRate = 1000;
            this.hitTime = 3000;
            this.playerInZone = false;
            //register for the antzone events
            this.game.EventDispatcher.AntZone += new AntZoneEventHandler(HandlePlayerInZone);
            this.game.EventDispatcher.StopFollowZone += EventDispatcher_StopFollowZone;
            this.game.EventDispatcher.HandleDeath += EventDispatcher_HandleDeath;

            this.audioEmitter = new AudioEmitter();
        }

        void EventDispatcher_HandleDeath(EventData eventData)
        {
            this.playerInZone = false;
            this.player = game.CharacterObject;
        }

        void EventDispatcher_StopFollowZone(EventData eventData)
        {
            this.playerInZone = false;
            this.player = game.CharacterObject;
            game.SoundManager.BkgrndMusic = true;
            game.SoundManager.BattleMusic = false;
            game.SoundManager.MenuMusic = false;
            game.SoundManager.TVMusic = false;
        }

        private void HandlePlayerInZone(EventData eventData)
        {
            this.playerInZone = true;
            this.player = game.CharacterObject;
            game.SoundManager.BkgrndMusic = false;
            game.SoundManager.BattleMusic = true;
            game.SoundManager.MenuMusic = false;
            game.SoundManager.TVMusic = false;
        }

        
        public override void Update(GameTime gameTime)
        {
            this.audioEmitter.Position = this.Transform.Translation;
            this.audioEmitter.Forward = this.Transform.Look;

            CheckForDeath(gameTime);

            if (playerInZone == false)
            {
                Patroll(gameTime);
            }
            else
            {
                FindPlayerToAttack(gameTime);
                
            }
            base.Update(gameTime);
        }

        private void FindPlayerToAttack(GameTime gameTime)
        {
            this.GetLookToPos(this.player.CharacterBody.Position);//- (this.Transform.Look * 5)
            //this.GetLookToPos(this.player.CharacterBody.Position);
            this.MoveToLookToPlayerPos(gameTime);//maybe we will need some offset that the enemy wont go inside player
           
        }


        private void Move(GameTime gameTime)
        {
            if (game.KeyboardManager.IsKeyDown(Keys.R))
            {
                this.Body.Position += this.Transform.Look * GameInfo.Player_Move_Speed;
            }
        }



        private void GetLookToPos(Vector3 pos)
        {
            this.lookToPosition = pos - this.CharacterBody.Position;
            if(this.lookToPosition == Vector3.Zero)
            {
                Console.WriteLine("error lookToPos == 0");
            }
            
            this.distance = this.lookToPosition.Length();
            lookToPosition.Normalize();


            float myDot = Vector3.Dot(this.Transform.Look, this.lookToPosition);
            float rotAngleInRad = (float)Math.Atan2(lookToPosition.Z, -lookToPosition.X);//Math.Acos(myDot);

            if(this.game.KeyboardManager.IsKeyDown(Keys.R))
            {
                Console.WriteLine("in Rad =:" + rotAngleInRad + "in deg" + MathHelper.ToDegrees((float)rotAngleInRad));
            }

            this.Transform.RotateTo(MathHelper.ToDegrees( rotAngleInRad));

            this.Transform.Look = this.lookToPosition;
     
            if(this.playerInZone==false)
            {
                this.lookLocked = true;
            }
            else
            {
                this.lookLocked = false;
            }
            
        }

        private void MoveToLookToPlayerPos(GameTime gameTime)
        {
            this.CharacterBody.Velocity += this.Transform.Look * GameInfo.Ant_Move_Speed;
            ////check if at location and need to get new node
            this.distance -= 0.2f;
            if (this.distance <= 10)
            {
                this.AttackPlayer(gameTime);
            }
        }


        private void MoveToLookToPos()
        {

            this.CharacterBody.Velocity += this.Transform.Look * GameInfo.Ant_Move_Speed;
            ////check if at location and need to get new node
            this.distance -= 0.2f;
            if (this.distance <= 0)
            {
                this.lookLocked = false;      
            }
        }

        private void Patroll(GameTime gameTime)
        {
            GetPathToRandomNode(gameTime);
        }

        private void GetPathToRandomNode(GameTime gameTime)
        {

            if (this.pathNodeList == null)//if we dont have path get one
            {
                //choose random path              
                int randNode = EnemyCollidableObject.rand.Next(nodeNames.Length);
                this.pathNodeList = this.pathEngine.Find(startOfNewPath,nodeNames[randNode]);
            }

            if(lookLocked == false)//if we dont have look Vector get one
            {
                GetNextNode();
            }
            else
            {
                this.MoveToLookToPos();
            }
      
        }

        private void GetNextNode()
        {
            
            if (this.atNode < this.pathNodeList.Count)//when we can move to the next node continue
            {
                this.GetLookToPos(pathNodeList[this.atNode].Position);
                this.atNode++;
            }
            else
            {
                startOfNewPath = this.pathNodeList[this.pathNodeList.Count - 1].ID.ToString();
                this.pathNodeList = null;
                this.atNode = 0;
            }

        }

        private void AttackPlayer(GameTime gameTime)
        {
            //we will need to add delay
            hitTime -= gameTime.ElapsedGameTime.TotalMilliseconds;
            if (hitTime <= 0)
            {
                this.CharacterBody.DoJump(GameInfo.Player_Jump_Height);
                hitTime = HIT_TIME;
            } 
            
        }

        private void CheckForDeath(GameTime gameTime)
        {
            if(this.health <= 0)
            {
                this.Respawn();
                this.Remove();
            }
        }

        private void RespawnWithDelay(GameTime gameTime)
        {
            respawnRate -= gameTime.ElapsedGameTime.TotalMilliseconds;
            if (respawnRate <= 0)
            {
                Respawn();
                respawnRate = staticRespawnRate; //reset firerate
            }

        }

        private void Respawn()
        {
            EventDispatcher.Publish(new EventData(this, EventData.EventType.AntRespawn));
        }


        public void TakeDamage(float amount)
        {
            if (this.audioEmitter != null)
            {
                game.SoundManager.Play3DCue("splat", this.audioEmitter);
            }

            this.health -= amount;
        }




    }
}
