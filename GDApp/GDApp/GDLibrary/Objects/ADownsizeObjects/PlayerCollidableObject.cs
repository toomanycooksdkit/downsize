﻿using GDLibrary;
using JigLibX.Collision;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GDApp.GDLibrary.Objects.ADownsizeObjects
{
    public class PlayerCollidableObject : AnimatedPlayerObject
    {
        private Main game;
        private Vector2 mouseDelta;
        private Matrix objectRotation;
        private float rotationSpeed;
        private Keys[] keys;
        private GunCollidableObject playerGun;
        private bool isHit, isJump;
        private double hitTime, regenTime, jumpTime;
        private static double HIT_TIME = 1000, REGEN_TIME = 1200, JUMP_TIME = 1000;
        private string takeName;
        private bool animLockJump;
        private bool animRunLock;
        private bool animIdleLock;
        private AudioEmitter audioEmitter;

        #region Properties
        public Keys[] Keys
        {
            get
            {
                return keys;
            }
            set
            {
                keys = value;
            }
        }
        #endregion

        public PlayerCollidableObject(Main game,Transform3D transform, Effect effect, ModelData modelData, Texture2D texture, Color color,
            Keys[] keys, float radius, float height, float accelerationRate, float decelerationRate, string takeName,Vector3 translationOffset, GunCollidableObject playerGun)
            : base(game, transform, effect, modelData, texture, color, radius, height, accelerationRate, decelerationRate, takeName, translationOffset)
        {
            this.takeName = takeName;
            this.playerGun = playerGun;
            this.keys = keys;
            this.game = game;
            rotationSpeed = MathHelper.ToRadians(1f);
            isHit = false;
            isJump = false;
            this.Body.CollisionSkin.callbackFn += CollisionSkin_callbackFn;
            this.game.EventDispatcher.Mission1End += EventDispatcher_Mission1End;
            this.animLockJump = false;
            this.animRunLock = false;
            this.animIdleLock = false;
            this.audioEmitter = new AudioEmitter();
        }

        void EventDispatcher_Mission1End(EventData eventData)
        {
            this.game.MenuMission.Text = MenuInfo.UI_Menu_ReturnedMission1;
        }

       

        bool CollisionSkin_callbackFn(CollisionSkin collider, CollisionSkin collidee)
        {
            if (collidee.Owner.ExternalData is EnemyCollidableObject)
            {
                this.removeHealth();
                regenTime = (REGEN_TIME * 5);
            }

            return true;
        }

        public void removeHealth()
        {
            if(!isHit)
            {
                game.UIManager.Health -= 10;

                if (game.UIManager.Health < 0)
                {
                    //add the respawn code here
                    game.UIManager.Health = GameInfo.HEALTH_BAR_WIDTH;
                    EventDispatcher.Publish(new EventData(this, EventData.EventType.PlayerDeath));
                }
                
                game.SoundManager.Play3DCue("hit" + MathUtility.RandomExcludeRange(1, 2, 3), this.audioEmitter);    
                this.isHit = true;
            }
        }

        public void regen(GameTime gameTime)
        {
            regenTime -= gameTime.ElapsedGameTime.Milliseconds;

            if(regenTime <= 0)
            {
                game.UIManager.Health += 10;

                if (game.UIManager.Health > GameInfo.HEALTH_BAR_WIDTH)
                {
                    game.UIManager.Health = GameInfo.HEALTH_BAR_WIDTH;
                }

                regenTime = REGEN_TIME;
            }
        }

        public void resetHit(GameTime gameTime)
        {
            hitTime -= gameTime.ElapsedGameTime.TotalMilliseconds;
            if (hitTime <= 0)
            {
                this.isHit = false;
                hitTime = HIT_TIME;
            } 
        }

        private void ResetJump(GameTime gameTime)
        {
            jumpTime -= gameTime.ElapsedGameTime.TotalMilliseconds;

            if (jumpTime <= 0)
            {
                isJump = false;
                jumpTime = JUMP_TIME;
            }
        }

        public override void Update(GameTime gameTime)
        {

            userInput(gameTime);
            updateGunPosition(gameTime);

            if(isHit)
            {
                resetHit(gameTime);
            }

            if(isJump)
            {
                ResetJump(gameTime);
            }

            regen(gameTime);

            base.Update(gameTime);
        }

        private void updateGunPosition(GameTime gameTime)
        {
            Vector3 myRot = new Vector3(0, this.Transform.Rotation.Y, 0);
            //Vector3 myTrans = new Vector3(this.Transform.Translation.X + this.Transform.Look.X, this.Transform.Translation.Y + 0.3f, this.Transform.Translation.Z);
            Vector3 newRight = this.Transform.Right * 0.27f;
            Vector3 myTrans2 = this.Transform.Translation + newRight;
            myTrans2 = new Vector3(myTrans2.X, myTrans2.Y+0.55f, myTrans2.Z);
            this.playerGun.Transform.Translation = myTrans2;
            this.playerGun.Transform.Rotation = myRot;
            this.playerGun.Transform.Scale = this.Transform.Scale;
            this.playerGun.Transform.Up = this.Transform.Up;
            this.playerGun.Transform.Look = this.Transform.Look;

        }

        private void userInput(GameTime gameTime)
        {
            move(gameTime);
            HandleMouseInputWhenClicked(gameTime);
        }

        private void HandleMouseInputWhenClicked(GameTime gameTime)
        {
            //update rotation if the mouse position changes
            if (!game.MouseManager.IsRightButtonClicked())
            {
                this.game.MouseManager.SetPosition(game.ScreenCentre);

                if (game.MouseManager.HasMoved())
                {
                    //get mouse delta from view port centre and multiply by the time elapsed in ms since last update
                    this.mouseDelta -= game.MouseManager.GetDeltaFromPosition(game.ScreenCentre) * gameTime.ElapsedGameTime.Milliseconds * this.rotationSpeed;
                    this.objectRotation = Matrix.CreateFromYawPitchRoll(mouseDelta.X, 0, 0);
                    this.Transform.RotateBy(mouseDelta.X);
                    this.mouseDelta = Vector2.Zero;

                }
            }

        }

        private void HandleMouseInput(GameTime gameTime)
        {
            //update rotation if the mouse position changes
            if (game.MouseManager.HasMoved())
            {
                //get mouse delta from view port centre and multiply by the time elapsed in ms since last update
                this.mouseDelta -= game.MouseManager.GetDeltaFromPosition(game.ScreenCentre) * gameTime.ElapsedGameTime.Milliseconds * this.rotationSpeed;

                //calculate matrix rotation
                this.objectRotation = Matrix.CreateFromYawPitchRoll(mouseDelta.X, 0, 0);
                this.Transform.RotateBy(mouseDelta.X);
                this.mouseDelta = Vector2.Zero;

            }
        }


        private void move(GameTime gameTime)
        {

            //jump
            if (this.Game.KeyboardManager.IsKeyDown(this.Keys[KeyInfo.Key_Array_Index_Jump]))
            {
                this.CharacterBody.DoJump(GameInfo.Player_Jump_Height);

                if(!isJump)
                {
                    game.SoundManager.Play3DCue("jump", this.audioEmitter);
                    isJump = true;
                }

                this.DoJumpAnimation();

            }

            if (this.game.KeyboardManager.IsKeyPressed() == true)
            {
                if (game.KeyboardManager.IsKeyDown(this.Keys[KeyInfo.Key_Array_Index_Forward]))
                {
                    //this.Color = Color.Red;
                    this.CharacterBody.Velocity += this.Transform.Look * GameInfo.Player_Move_Speed;
                    if (this.CharacterBody.DoJumpB == false)
                    {
                        this.DoRunAnimation();
                    }
                }
                else if (game.KeyboardManager.IsKeyDown(this.Keys[KeyInfo.Key_Array_Index_Backward]))
                {
                    this.CharacterBody.Velocity -= this.Transform.Look * GameInfo.Player_Move_Speed;
                    if (this.CharacterBody.DoJumpB == false)
                    {
                        this.DoRunAnimation();
                    }
                    //this.Color = Color.Blue;
                }
           
                if (game.KeyboardManager.IsKeyDown(this.Keys[KeyInfo.Key_Array_Index_Left]))
                {
                    //this.Transform.MoveBy(-0.02f * gameTime.ElapsedGameTime.Milliseconds * this.Transform.Right);
                    this.CharacterBody.Velocity -= this.Transform.Right * GameInfo.Player_Move_Speed;
                    if (this.CharacterBody.DoJumpB == false)
                    {
                        this.DoRunAnimation();
                    }
                    //this.Color = Color.Yellow;
                }
                else if (game.KeyboardManager.IsKeyDown(this.Keys[KeyInfo.Key_Array_Index_Right]))
                {
                   // this.Transform.MoveBy(0.02f * gameTime.ElapsedGameTime.Milliseconds * this.Transform.Right);
                    this.CharacterBody.Velocity += this.Transform.Right * GameInfo.Player_Move_Speed;
                    if (this.CharacterBody.DoJumpB == false)
                    {
                        this.DoRunAnimation();
                    }
                    //this.Color = Color.Green;
                }
            }
            else //decelerate to zero when not pressed
            {
                this.CharacterBody.DesiredVelocity = Vector3.Zero;
                if (this.CharacterBody.DoJumpB == false)
                {
                    this.DoIdleAnimation();
                }
            }

        }

        private void DoIdleAnimation()
        {

            if (this.animIdleLock == false)
            {
                this.ModelData = this.game.ModelDataDictionary["Red_Idle"];
                this.InitialiseAnimationPlayer();
                this.animIdleLock = true;
                this.animRunLock = false;
                this.animLockJump = false;
            }
        }

        private void DoRunAnimation()
        {
            if (this.animRunLock == false)
            {
                this.ModelData = this.game.ModelDataDictionary["RedRun4"];
                this.InitialiseAnimationPlayer();
                this.animRunLock = true;
                this.animIdleLock = false;
                this.animLockJump = false;
            }

        }

        private void DoJumpAnimation()
        {
                if (this.animLockJump == false)
                {
                    this.ModelData = this.game.ModelDataDictionary["Red_Jump"];
                    this.InitialiseAnimationPlayer();
                    this.animLockJump = true;
                    this.animIdleLock = false;
                    this.animRunLock = false;
                }
        }
    }
}
