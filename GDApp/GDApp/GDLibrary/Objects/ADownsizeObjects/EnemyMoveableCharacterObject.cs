﻿using GDLibrary;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace GDApp.GDLibrary.Objects.ADownsizeObjects
{
    public class EnemyMoveableCharacterObject : CharacterObject
    {
        private Main game;


        public EnemyMoveableCharacterObject(Main game,
         Transform3D transform, BasicEffect effect, ModelData modelData, Texture2D texture, Color color,
            Keys[] keys,
         float radius, float height, float accelerationRate, float decelerationRate)
            : base(game, transform, effect, modelData, texture, color, radius, height, accelerationRate, decelerationRate)
        {
           
            this.game = game;
            
        }


    }
}
