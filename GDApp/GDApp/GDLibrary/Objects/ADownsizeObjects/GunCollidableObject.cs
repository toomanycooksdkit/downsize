﻿

using GDLibrary;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
namespace GDApp.GDLibrary.Objects.ADownsizeObjects
{
    public class GunCollidableObject : ModelObject
    {
        private BulletCollidableObject bullet;
        private Main game;
        private Ray gunToCursorDirectionRay;
        private double resetRate;
        private double fireRate;
        private CollidableObject pickedObject;
        private int augumentedDamage;
        private double weaponTime;
        private static double REGEN_TIME = 1000;
    

        #region Properties
        public double FireRate
        {
            get
            {
                return fireRate;
            }
            set
            {
                fireRate = value;
            }
        }
        #endregion

        public GunCollidableObject(Main game,
            Transform3D transform, BasicEffect effect, ModelData modelData, Texture2D texture, Color color, BulletCollidableObject bullet)
            : base(game, transform, effect, modelData, texture, color)
        {
            this.game = game;
            this.bullet = bullet;
            this.fireRate = GameInfo.GUN_FIRE_RATE_MS;
            this.augumentedDamage = GameInfo.BULLET_DAMAGE;
        }

        public override void Update(GameTime gameTime)
        {

            if (game.MouseManager.IsLeftButtonClicked())
            {
                ShootWithDelay(gameTime);
            }
            DoPickAugument();
            regen(gameTime);
            base.Update(gameTime);
        }

        private void DoPickAugument()
        {
            this.pickedObject = this.Game.MouseManager.GetPickedObject(this.Game.CameraManager.ActiveCamera, 1000);

            //valid and pickup type - this could also be ammo, health etc
            if (this.pickedObject != null && this.pickedObject.ObjectType == ObjectType.AugumentCogWheel)
            {
                this.game.MouseUI.CursorOnPickup();
                //remove the object after pressed the f key and change the fire rate
                DoAugumentationCogWheel();
            }
            else if(this.pickedObject != null && this.pickedObject.ObjectType == ObjectType.AugumentSpring)
            {
                this.game.MouseUI.CursorOnPickup();
                DoAugumentationSpring();
            }
            else if (this.pickedObject != null && this.pickedObject.ObjectType == ObjectType.MissionPickup)
            {
                this.game.MouseUI.CursorOnMissionPickup();
                this.game.MouseUI.PickedObject = this.pickedObject;
                DoMissionPick();
            }
            else
            {
                this.game.MouseUI.PickedObject = null;
                this.game.MouseUI.CursorNormall();
            }

        }

        private void DoAugumentationSpring()
        {
            if (this.game.KeyboardManager.IsKeyDown(Keys.F))
            {
                this.pickedObject.Remove();
                this.augumentedDamage = 20;
                this.bullet.ScaleFactor = 0.25f;
                PlayPickupSound();
                GameInfo.HUD_GAUGE_DAMAGE_WIDTH += 60;
            }
        }

        private void DoAugumentationCogWheel()
        {
            if (this.game.KeyboardManager.IsKeyDown(Keys.F))
            {
                this.fireRate = 150;
                this.pickedObject.Remove();
                PlayPickupSound();
                GameInfo.HUD_GAUGE_FIRE_RATE_WIDTH += 60;
            }
        }

        private void DoMissionPick()
        {
            if (this.game.KeyboardManager.IsKeyDown(Keys.F))
            {
                this.pickedObject.Remove();
                //change mission log
                this.game.MenuMission.Text = MenuInfo.UI_Menu_EndMission1;
                this.game.FusePicked = true;
                PlayPickupSound();
            }
        }

        private void PlayPickupSound()
        {
            game.SoundManager.Play3DCue("pickup" + MathUtility.RandomExcludeRange(1, 4, 5), game.AudioEmitter);
        }

        private void ShootWithDelay(GameTime gameTime)
        {
            resetRate -= gameTime.ElapsedGameTime.TotalMilliseconds;
            if (resetRate <= 0)
            {
                int w = game.UIManager.Weapon;

                if(w > 0)
                {
                    Shoot();

                    weaponTime = REGEN_TIME;
                    game.UIManager.Weapon -= 10;
                    w = game.UIManager.Weapon;
                }

                if (w < 0)
                {
                    game.UIManager.Weapon = 0;
                }

                resetRate = fireRate; //reset firerate
            }

        }

        public void regen(GameTime gameTime)
        {
            weaponTime -= gameTime.ElapsedGameTime.Milliseconds;

            if (weaponTime <= 0)
            {
                game.UIManager.Weapon += 10;
                int w = game.UIManager.Weapon;

                if (w > GameInfo.WEAPON_BAR_WIDTH)
                {
                    game.UIManager.Weapon = GameInfo.WEAPON_BAR_WIDTH;
                }

                weaponTime = REGEN_TIME;
            }
        }

        //here is the bullet created as is shoot from the gun
        private void Shoot()
        {

            game.SoundManager.Play3DCue("lazer" + MathUtility.RandomExcludeRange(1, 3, 4), game.AudioEmitter);

            BulletCollidableObject firedBullet = (BulletCollidableObject)this.bullet.Clone();
            firedBullet.Transform.Translation = this.Transform.Translation + this.Transform.Look*1.7f;//here is the offset of fired bullet


            gunToCursorDirectionRay = this.game.MouseManager.GetMouseRayFromNearPosition(game.CameraManager[0], this.Transform.Translation);

            //firedBullet.Direction = gunToCursorDirectionRay.Direction; //direction of vector from camera to cursor
            firedBullet.Direction = this.Transform.Look; //direction of look vector

            firedBullet.Transform.Scale = firedBullet.Transform.Scale * firedBullet.ScaleFactor;
            JigLibX.Geometry.Sphere primitiveSphere = new JigLibX.Geometry.Sphere(firedBullet.Transform.Translation, firedBullet.ScaleFactor);
            JigLibX.Collision.MaterialProperties matProperties = new JigLibX.Collision.MaterialProperties(0.2f, 0.8f, 0.7f);
            firedBullet.AddPrimitive(primitiveSphere, matProperties);
            firedBullet.Enable(false, 1);
            firedBullet.ObjectType = ObjectType.Pickup;

            firedBullet.Damega = this.augumentedDamage;

            game.ObjectManager.Add(firedBullet);
        }



    }
}
