﻿using GDApp;
using JigLibX.Collision;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using SkinnedModel;
using System;

namespace GDLibrary
{
    public class AnimatedPlayerObject : CharacterObject
    {
        #region Variables
        private AnimationPlayer animationPlayer;
        private SkinningData skinningData;

        private string takeName;
        private Vector3 translationOffset;
        #endregion

        #region Properties
        public AnimationPlayer AnimationPlayer
        {
            get
            {
                return animationPlayer;
            }
        }
        #endregion


        public AnimatedPlayerObject(Main game, Transform3D transform, Effect effect, ModelData modelData, Texture2D texture, Color color,
             float radius, float height, float accelerationRate, float decelerationRate, string takeName, Vector3 translationOffset)
            : base(game, transform, effect, modelData, texture, color
            , radius, height, accelerationRate, decelerationRate)
        {
            //set initial animation played when player instanciated
            this.takeName = takeName;

            //allows developer to move drawn player with reference to collision capsule
            this.translationOffset = translationOffset;

            //load animation player with initial take e.g. idle
            InitialiseAnimationPlayer();

        }

        public void InitialiseAnimationPlayer()
        {
            // Look up our custom skinning information.
            skinningData = this.ModelData.Model.Tag as SkinningData;

            if (skinningData == null)
                throw new InvalidOperationException
                    ("This model does not contain a SkinningData tag.");

            // Create an animation player, and start decoding an animation clip.
            animationPlayer = new AnimationPlayer(skinningData);

            //set initial clip
            setClip(takeName);
        }


        public override void Update(GameTime gameTime)
        {
          


            //update player to return bone transforms for the appropriate frame in the animation
            animationPlayer.Update(gameTime.ElapsedGameTime, true, Matrix.Identity);

            base.Update(gameTime);
        }

        public void setClip(string takeName)
        {
            animationPlayer.StartClip(skinningData.AnimationClips[takeName]);
        }

        public override Matrix GetWorldMatrix()
        {
            return Matrix.CreateScale(Transform.Scale) *
                this.Collision.GetPrimitiveLocal(0).Transform.Orientation *
                this.Body.Orientation *
                this.Transform.Orientation *
                Matrix.CreateTranslation(this.Body.Position + translationOffset);
        }


    }
}
