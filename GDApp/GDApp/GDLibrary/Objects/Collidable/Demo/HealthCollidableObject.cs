﻿using GDApp;
using JigLibX.Collision;
using JigLibX.Geometry;
using JigLibX.Math;
using JigLibX.Physics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace GDLibrary
{
    /// <summary>
    /// This class demonstrates what happens if the collision callback function returns false
    /// </summary>
    public class HealthCollidableObject : CollidableObject
    {
        #region Variables
        #endregion

        #region Properties
        #endregion

        public HealthCollidableObject(Main game,
            Transform3D transform, BasicEffect effect, 
            ModelData modelData, Texture2D texture, Color color)
            : base(game, transform, effect, modelData, texture, color)
        {
            this.Body.CollisionSkin.callbackFn += CollisionSkin_callbackFn;
        }

        public bool CollisionSkin_callbackFn(CollisionSkin collider, CollisionSkin collidee)
        {
            //try returning false instead and watch what happens when the health object hits another collidable surface
            return true;
            //can return false go through objects
        }
    }
}
