﻿using GDApp;
using JigLibX.Collision;
using JigLibX.Geometry;
using JigLibX.Math;
using JigLibX.Physics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace GDLibrary
{
    public class AmmoCollidableObject : CollidableObject
    {
        #region Variables
        #endregion

        #region Properties
        #endregion

        public AmmoCollidableObject(Main game,
            Transform3D transform, BasicEffect effect, 
            ModelData modelData, Texture2D texture, Color color)
            : base(game, transform, effect, modelData, texture, color)
        {
            this.Body.CollisionSkin.callbackFn += CollisionSkin_callbackFn;
        }

        public bool CollisionSkin_callbackFn(CollisionSkin collider, CollisionSkin collidee)
        {
            if(collider.Owner.ExternalData is CollidableObject)
            {
                //enable these lines after you have observed the game running once
                CollidableObject c = (CollidableObject)collider.Owner.ExternalData;

                //last test
                c.Color = Color.Red;
            }

            if (collidee.Owner.ExternalData is CollidableObject)
            {
                CollidableObject c = (CollidableObject)collidee.Owner.ExternalData;
                if (c.ObjectType == GDLibrary.ObjectType.Pickup)
                {
                    //enable this lines after you have observed the game running once
                    c.Color = Color.Green;

                    //Bug when removing objects from the PhysicsSystem - the problem is in the PhysicsSystem::Integrate() method
                    //this.Game.ObjectManager.Remove(c);
                    //this.Game.PhysicsManager.PhysicsSystem.RemoveBody(collidee.Owner);
                }
            }

             return true;
        }
    }
}
