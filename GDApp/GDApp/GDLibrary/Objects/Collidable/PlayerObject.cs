﻿using GDApp;
using JigLibX.Collision;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace GDLibrary
{
    /// <summary>
    /// Represents your player in the game. 
    /// </summary>
    public class PlayerObject : CharacterObject
    {
        #region Variables
        private Keys[] keys;
        #endregion

        #region Properties
        public Keys[] Keys
        {
            get
            {
                return keys;
            }
            set
            {
                keys = value;
            }
        }
        #endregion

        public PlayerObject(Main game,
         Transform3D transform, Effect effect, ModelData modelData, Texture2D texture, Color color,
            Keys[] keys,
         float radius, float height, float accelerationRate, float decelerationRate)
            : base(game, transform, effect, modelData, texture, color, radius, height, accelerationRate, decelerationRate)
        {
            this.keys = keys;
        }

        public override void Update(GameTime gameTime)
        {
            HandleKeyboardInput(gameTime);
            HandleMouseInput(gameTime);
            base.Update(gameTime);
        }

        protected virtual void HandleMouseInput(GameTime gameTime)
        {
          
        }

        protected virtual void HandleKeyboardInput(GameTime gameTime)
        {

        }


     
    }
}
