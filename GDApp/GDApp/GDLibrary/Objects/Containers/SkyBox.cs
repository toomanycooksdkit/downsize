﻿using GDApp;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;


namespace GDLibrary
{
    /// <summary>
    /// Draws a sky box by creating 5 textured quads and translating and rotating each quad into the appropriate position.
    /// </summary>
    public class SkyBox : DrawableGameComponent
    {
        PrimitiveObject[] skyBoxObjects = new PrimitiveObject[5];
        private Main game;
        private BasicEffect effect;

        public SkyBox(Main game, BasicEffect effect, IVertexData vertexData, Texture2D[] textures, float scale, Color color)
            : base(game)
        {

            this.game = game;
            this.effect = effect;
            PrimitiveObject cloneObject = null;
            PrimitiveObject texturedQuadPrimitiveObject = new PrimitiveObject(game, Transform3D.ZeroUnitX, effect, vertexData, Color.White);
                
            //back
            cloneObject = (PrimitiveObject)texturedQuadPrimitiveObject.Clone();
            cloneObject.Transform = new Transform3D(new Vector3(0, 0, -scale / 2.0f), Vector3.Zero, scale * Vector3.One, Vector3.Zero, Vector3.Zero);
            cloneObject.Texture = textures[0];
            game.ObjectManager.Add(cloneObject);
            skyBoxObjects[0] = cloneObject;

            //right
            cloneObject = (PrimitiveObject)texturedQuadPrimitiveObject.Clone();
            cloneObject.Transform = new Transform3D(new Vector3(scale / 2.0f, 0, 0), new Vector3(0, -90, 0), scale * Vector3.One, Vector3.Zero, Vector3.Zero);
            cloneObject.Texture = textures[1];
            game.ObjectManager.Add(cloneObject);
            skyBoxObjects[1] = cloneObject;

            //left
            cloneObject = (PrimitiveObject)texturedQuadPrimitiveObject.Clone();
            cloneObject.Transform = new Transform3D(new Vector3(-scale / 2.0f, 0, 0), new Vector3(0, 90, 0), scale * Vector3.One, Vector3.Zero, Vector3.Zero);
            cloneObject.Texture = textures[2];
            game.ObjectManager.Add(cloneObject);
            skyBoxObjects[2] = cloneObject;

            //front
            cloneObject = (PrimitiveObject)texturedQuadPrimitiveObject.Clone();
            cloneObject.Transform = new Transform3D(new Vector3(0, 0, scale / 2.0f), new Vector3(0, 180, 0), scale * Vector3.One, Vector3.Zero, Vector3.Zero);
            cloneObject.Texture = textures[3];
            game.ObjectManager.Add(cloneObject);
            skyBoxObjects[3] = cloneObject;

            //top
            cloneObject = (PrimitiveObject)texturedQuadPrimitiveObject.Clone();
            cloneObject.Transform = new Transform3D(new Vector3(0, scale / 2.0f, 0), new Vector3(90, -90, 0), scale * Vector3.One, Vector3.Zero, Vector3.Zero);
            cloneObject.Texture = textures[4];
            game.ObjectManager.Add(cloneObject);
            skyBoxObjects[4] = cloneObject;
        }
    
        public override void Update(GameTime gameTime)
        {
            for(int i = 0; i < skyBoxObjects.Length; i++)
                skyBoxObjects[i].Transform.World *= Matrix.CreateRotationY(GameInfo.SkyDome_Rotation_Speed);

            base.Update(gameTime);
        }



      
    }
}
