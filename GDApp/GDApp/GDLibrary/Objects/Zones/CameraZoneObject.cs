﻿using GDApp;
using GDApp.GDLibrary.Objects.ADownsizeObjects;
using JigLibX.Collision;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GDLibrary
{
    public class CameraZoneObject : ZoneObject
    {
        #region Variables
        private int cameraIndex;
        private bool bHasTargetEnteredZone;
        private CollidableObject targetObject;
        private string cueName;
        #endregion

        #region Properties
        public CollidableObject TargetObject
        {
            get
            {
                return targetObject;
            }
            set
            {
                targetObject = value;
            }
        }

        public string CueName
        {
            get
            {
                return cueName;
            }
            set
            {
                cueName = value;
            }
        }
        public int CameraIndex
        {
            get
            {
                return cameraIndex;
            }
            set
            {
                cameraIndex = value;
            }
        }
        #endregion

        public CameraZoneObject(Main game, Transform3D transform, int cameraIndex, CollidableObject targetObject, string cueName)
            : base (game, transform)
        {

            Set(cameraIndex, targetObject);

            //register for callback collision to see who just walked into the zone
            this.Body.CollisionSkin.callbackFn += CollisionSkin_callbackFn;

            this.cueName = cueName;
        }

        public CameraZoneObject(Main game, Transform3D transform, int cameraIndex, CollidableObject targetObject)
            : this(game, transform, cameraIndex, targetObject, null)
        {
        }

        //use this method to reset the zone i.e. player hasnt entered yet
        public void Set(int cameraIndex, CollidableObject targetObject)
        {
            //if index is valid then set
            if (cameraIndex < this.Game.CameraManager.Size())
            {
                //camera to set when target object enters
                this.cameraIndex = cameraIndex;
            }
            else //otherwise set to zero
            {
                this.cameraIndex = 0;
            }

            //object that triggers the zone
            this.targetObject = targetObject;

            //target has not yet entered zone
            this.bHasTargetEnteredZone = false;
        }

        public bool CollisionSkin_callbackFn(CollisionSkin collider, CollisionSkin collidee)
        {
            DetectTarget(collidee);
            //always return FALSE in a zone object, otherwise the other drawn objects will bounce off it.
            return false;
        }

        void EventDispatcher_ChangeMusic(EventData eventData)
        {
            Game.SoundManager.changeMusic(cueName);
        }

        private void DetectTarget(CollisionSkin collidee)
        {
            if (collidee.Owner.ExternalData is PlayerCollidableObject)
            {
                PlayerCollidableObject playerObject = collidee.Owner.ExternalData as PlayerCollidableObject;

                //is this the player we are interested in and has it just entered this zone for the first time
                if ((playerObject == this.targetObject) && (!bHasTargetEnteredZone))
                {
                    //Do something...
                    EventDispatcher.Publish(new CameraEventData(this, EventData.EventType.SetActiveCameraIndex, cameraIndex));

                    if(cueName != null)
                    {
                        Game.SoundManager.changeMusic(cueName);
                    }

                    //set so we don't repeat if the player stays in the zone
                    bHasTargetEnteredZone = true;
                }
            }
            else
            {
                //reset so the target can re-enter the zone and re-set the camera again
                bHasTargetEnteredZone = false;
            }
        }
    }
}
