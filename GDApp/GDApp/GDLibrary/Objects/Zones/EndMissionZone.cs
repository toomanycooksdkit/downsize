﻿using GDLibrary;
using JigLibX.Collision;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GDApp.GDLibrary.Objects.ADownsizeObjects
{
    public class EndMissionZone : ZoneObject
    {
        private CollidableObject targetObject;
        private bool bHasTargetEnteredZone;
        private Main game;

        public EndMissionZone(Main game, Transform3D transform, CollidableObject targetObject)
            : base(game, transform)
        {

            this.game = game;
            //object that triggers the zone
            this.targetObject = targetObject;

            //target has not yet entered zone
            this.bHasTargetEnteredZone = false;
            //register for callback collision to see who just walked into the zone
            this.Body.CollisionSkin.callbackFn += CollisionSkin_callbackFn;
        }

        public bool CollisionSkin_callbackFn(CollisionSkin collider, CollisionSkin collidee)
        {
            DetectTarget(collidee);
            //always return FALSE in a zone object, otherwise the other drawn objects will bounce off it.
            return false;
        }

        private void DetectTarget(CollisionSkin collidee)
        {
            if (collidee.Owner.ExternalData is PlayerCollidableObject)
            {
                PlayerCollidableObject playerObject = collidee.Owner.ExternalData as PlayerCollidableObject;

                //is this the player we are interested in and has it just entered this zone for the first time
                if ((playerObject == this.targetObject) && (!bHasTargetEnteredZone))
                {
                    //Do something...
                   if(this.game.FusePicked == true)
                   {
                        EventDispatcher.Publish(new EventData(this, EventData.EventType.Mission1End));
                   }
                    
                    //set so we don't repeat if the player stays in the zone
                    bHasTargetEnteredZone = true;
                }
            }
            else
            {
                //reset so the target can re-enter the zone and re-set the camera again
                bHasTargetEnteredZone = false;
            }
        }
    }





}