﻿using GDApp;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;

namespace GDLibrary
{
    public class ModelObject : GameObject, ICloneable
    {
        #region Variables
        private Effect effect;
        private ModelData modelData;

        private Texture2D texture;
        private Color color;
        private Vector3 colorAsVector3;


        #endregion

        #region Properties
        public Effect Effect
        {
            get
            {
                return effect;
            }
            set
            {
                effect = value;
            }
        }
        public ModelData ModelData
        {
            get
            {
                return modelData;
            }
            set
            {
                modelData = value;
            }
        }
        public Texture2D Texture
        {
            get
            {
                return texture;
            }
            set
            {
                texture = value;
            }
        }
        public Color Color
        {
            get
            {
                return color;
            }
            set
            {
                color = value;
                colorAsVector3 = color.ToVector3();
            }
        }
        public Vector3 ColorAsVector3
        {
            get
            {
                return colorAsVector3;
            }
        }
        #endregion

        public ModelObject(Main game, Transform3D transform, Effect effect, 
            ModelData modelData, Texture2D texture, Color color)
            : base(game, transform)
        {
            this.effect = effect;
            this.modelData = modelData;
            this.texture = texture;
            this.Color = color;
        }

        public new object Clone()
        {
            return new ModelObject(this.Game, (Transform3D)this.Transform.Clone(), this.effect, this.modelData, this.texture, this.color);
        }
    }
}
