﻿using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GDLibrary
{
    public class PrimitiveFactory
    {
        public enum GeometryType : sbyte
        {
            WireframeLine,
          //  WireFrameQuad,      //to do
          //  WireFrameCube,      //to do
            WireframeCircle,
          //  WireFrameCamera,    //to do
            WireFrameOriginHelper,
            FilledQuad,
            FilledCube,
            TexturedQuad//add a new primitive type e.g. WireframeCamera etc
        }

        public enum StorageType : sbyte
        {
            Nonbuffered,
            Buffered,
            Indexed
        }

        public static IVertexData GetVertexDataInstance(GraphicsDevice graphicsDevice, GeometryType geometryType,
            StorageType storageType) 
        {
            IVertexData vertexData = null;

            switch (geometryType)
            {
                case GeometryType.WireframeLine:
                    vertexData = GetVertexData<VertexPositionColor>(graphicsDevice, storageType, VertexInfo.GetVerticesPositionColorLine(1), PrimitiveType.LineList, 1);
                    break;

                case GeometryType.WireframeCircle:
                    vertexData = GetVertexData<VertexPositionColor>(graphicsDevice, storageType, VertexInfo.GetVerticesPostionColorCircle(1, 15), PrimitiveType.LineStrip, 24);
                    break;

                case GeometryType.WireFrameOriginHelper:
                    vertexData = GetVertexData<VertexPositionColor>(graphicsDevice, storageType, VertexInfo.GetVerticesPositionColorOriginHelper(), PrimitiveType.LineList, 10);
                    break;

                case GeometryType.FilledQuad:
                    vertexData = GetVertexData<VertexPositionColor>(graphicsDevice, storageType, VertexInfo.GetVerticesPositionColorQuad(1), PrimitiveType.TriangleStrip, 2);
                    break;

                case GeometryType.FilledCube:
                    vertexData = GetVertexData<VertexPositionColor>(graphicsDevice, storageType, VertexInfo.GetVerticesPositionColorCube(1), PrimitiveType.TriangleList, 12);
                    break;

                case GeometryType.TexturedQuad:
                    vertexData = GetVertexData<VertexPositionColorTexture>(graphicsDevice, storageType, VertexInfo.GetVerticesPositionColorTextureQuad(1), PrimitiveType.TriangleStrip, 2);
                    break;

                //add a case for every primitive type e.g. WireframeCamera etc
            }

            return vertexData;
        }

        //public static IVertexData GetVertexDataInstance<T>(T[] vertices, StorageType storageType) where T : struct, IVertexType
        //{
        //    //if you want a filled quad then get the vertices and pass to 
        //    //the appropriate storage type

        //    //VertexData vData
        //    //    = new VertexData(vertices, PrimitiveType.TriangleList, 1);

        //    //BufferedVertexData<VertexPositionColor> vData
        //    //  = new BufferedVertexData<VertexPositionColor>(graphics.GraphicsDevice,
        //    //      vertices, PrimitiveType.TriangleList, 1);

        //    return null;
        //}

        private static IVertexData GetVertexData<T>(GraphicsDevice graphicsDevice, StorageType storageType, T[] vertices,
            PrimitiveType primitiveType, int primitiveCount) where T : struct, IVertexType
        {
            IVertexData vertexData = null;


            switch (storageType)
            {
                case StorageType.Nonbuffered:
                    vertexData = new VertexData<T>(vertices, primitiveType, primitiveCount);
                    break;

                case StorageType.Buffered:
                    vertexData = new BufferedVertexData<T>(graphicsDevice, vertices, primitiveType, primitiveCount);
                    break;

                case StorageType.Indexed:
                    vertexData = new IndexedVertexData<T>(graphicsDevice, vertices, primitiveType, primitiveCount);
                    break;
            }

            return vertexData;
        }
    }
}
