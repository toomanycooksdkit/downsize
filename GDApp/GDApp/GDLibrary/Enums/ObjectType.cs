﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GDLibrary
{
    public enum ObjectType : sbyte
    {
        Zone,
        Pickup,
        Decorator,
        Player,
        Gate,
        Ladder,
        AugumentCogWheel,
        AugumentSpring,
        MissionPickup
    }
}
