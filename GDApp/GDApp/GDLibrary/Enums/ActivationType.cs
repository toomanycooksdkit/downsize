﻿
namespace GDLibrary
{
    public enum ActivationType : sbyte
    {
        Off,            //inactive, so will not be updated
        On,             //active, so updated 
    }
}
