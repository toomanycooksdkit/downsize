﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GDLibrary
{
    public enum CameraType : sbyte
    {
        Static,
        FreeLookCamera,
        FirstPersonCamera,
        ThirdPersonCamera,
        RailCamera,
        TrackCamera,
        OrbitCamera,
        ArcCamera
    }
}
