﻿using GDApp;
using Microsoft.Xna.Framework;
using System.Collections.Generic;

namespace GDLibrary
{
    public delegate void MenuEventHandler(EventData eventData);
    //add more delegates here...
    public delegate void CameraEventHandler(EventData eventData);

    public delegate void AntZoneEventHandler(EventData eventData);

    public delegate void PlayerEventHandler(EventData eventData);

    public class EventDispatcher : GameComponent
    {
        //event collection
        private static Stack<EventData> stack = new Stack<EventData>();

        //events
        public event MenuEventHandler MainMenu, UIMenu;
        //add more events here...
        public event CameraEventHandler Camera;

        public event AntZoneEventHandler AntZone, StopFollowZone, RespawnAntEvent;

        public event PlayerEventHandler HandleDeath, Mission1End;
  
        public EventDispatcher(Main game)
            : base(game)
        {
            
        }

        //called by any entity wishing to add an event to the stack for processing
        public static void Publish(EventData eventData)
        {
            //if we would like to ensure uniqueness of events we can use a HashSet to identify duplicates
            stack.Push(eventData);
        }

        //process all events in the stack
        //how will stack size affect the time taken to process any one event?
        public override void Update(GameTime gameTime)
        {
            for (int i = 0; i < stack.Count; i++)
            {
                Process(stack.Pop());
            }

            base.Update(gameTime);
        }

        private void Process(EventData eventData)
        {
            //if one of these types then generate a menu event
            if ((eventData.Event == EventData.EventType.Play)
                || (eventData.Event == EventData.EventType.Restart)
                    || (eventData.Event == EventData.EventType.Pause)
                        || (eventData.Event == EventData.EventType.Exit))
            {
                OnMainMenu(eventData);
            }
            else if((eventData.Event == EventData.EventType.SetActiveCameraIndex)
                  || (eventData.Event == EventData.EventType.CyleActiveCameraIndex)
                 || (eventData.Event == EventData.EventType.ResetActiveCamera))
            {
                OnCamera(eventData);
            }
            else if ((eventData.Event == EventData.EventType.SetActiveCameraIndex)
                              || (eventData.Event == EventData.EventType.CyleActiveCameraIndex)
                             || (eventData.Event == EventData.EventType.ResetActiveCamera))
            {
                OnCamera(eventData);
            }
            #region Demo
            else if ((eventData.Event == EventData.EventType.AntFindPlayer))//ant zone events processed
            {
                OnAntZone(eventData);
            }
            else if ((eventData.Event == EventData.EventType.AntStopFollow))
            {
                OnAntStopFollowZone(eventData);
            }
            else if ((eventData.Event == EventData.EventType.AntRespawn))
            {
                OnAntRespawn(eventData);
            }
            else if((eventData.Event == EventData.EventType.PlayerDeath))
            {
                OnPlayerDeath(eventData);
            }
            else if ((eventData.Event == EventData.EventType.Mission1End))
            {
                OnMission1End(eventData);
            }
            #endregion
            //add conditional statements for more event types here...
        }

        private void OnMission1End(EventData eventData)
        {
            if (Mission1End != null)
                Mission1End(eventData);
        }

        private void OnPlayerDeath(EventData eventData)
        {
            if (HandleDeath != null)
                HandleDeath(eventData);
        }


        private void OnAntRespawn(EventData eventData)
        {
            if (RespawnAntEvent != null)
                RespawnAntEvent(eventData);
        }

        private void OnAntStopFollowZone(EventData eventData)
        {
            if (StopFollowZone != null)
                StopFollowZone(eventData);
        }

        protected virtual void OnAntZone(EventData eventData)
        {
            if (AntZone != null)
                AntZone(eventData);
        }

        //remove all events - we could use this for a restart - see Main::DoRestart()
        public static void Clear()
        {
            if (stack.Count > 0)
                stack.Clear();
        }


        //called when a menu event needs to be generated
        protected virtual void OnUIMenu(EventData eventData)
        {
            //non-null if an object has subscribed to this event
            if (UIMenu != null)
                UIMenu(eventData);
        }


        //called when a menu event needs to be generated
        protected virtual void OnMainMenu(EventData eventData)
        {
            //non-null if an object has subscribed to this event
            if (MainMenu != null)
                MainMenu(eventData);
        }

        //called when a camera event needs to be generated
        protected virtual void OnCamera(EventData eventData)
        {
            //non-null if an object has subscribed to this event
            if (Camera != null)
                Camera(eventData);
        }
    }
}
