﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GDLibrary
{
    public class CameraEventData : EventData
    {
        #region Variables
        private int cameraIndex;
        #endregion

        #region Properties
        public int CameraIndex
        {
            get
            {
                return cameraIndex;
            }
        }
        #endregion

        //used by cycle camera event generators
        public CameraEventData(object sender, EventType eventType)
            : this(sender, eventType, 0)
        {
        }

        //used when we wish to specify a camera to be set as active
        public CameraEventData(object sender, EventType eventType, int cameraIndex)
            : base(sender, eventType)
        {
            this.cameraIndex = cameraIndex;
        }

        //could use this to detect duplicate events in the event dispatcher
        public override bool Equals(object obj)
        {
            CameraEventData other = obj as CameraEventData;
            return ((this.cameraIndex == other.CameraIndex) && (base.Equals(obj)));
        }

        public override int GetHashCode()
        {
            int seed = 31;
            return seed * this.cameraIndex + base.GetHashCode();
        }

        public object Clone()
        {
            return new CameraEventData(this.Sender, this.Event, this.CameraIndex);
        }

        public override string ToString()
        {
            return base.ToString() + "CameraEventData - Index: " + this.CameraIndex;
        }
    }
}
