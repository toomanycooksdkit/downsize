﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GDLibrary
{
    public class EventData 
    {
        #region Variables
        public enum EventType : sbyte
        {
            //default used when instanciating a blank event 
            Empty,

            //listeners - object manager
            Play,
            Restart,
            Pause,
            Exit,

            //listeners - sound manager
            AudioUp,
            AudioDown,
            AudioMute,

            //listeners - camera manager
            SetActiveCameraIndex,
            CyleActiveCameraIndex,
            ResetActiveCamera,

            #region Demo
            //listeners - content generator
            AddHouse,
            AddBarracks,
            AddFence,
            #endregion

            //add mode event types here...
            AntFindPlayer,
            AntStopFollow,
            AntRespawn,
            PlayerDeath,
            Mission1End,
            ChangeMusic
        }

        private object sender;
        private EventType eventType;
        #endregion 

        #region Properties
        public object Sender
        {
            get
            {
                return sender;
            }
            set
            {
                sender = value;
            }
        }
        public EventType Event
        {
            get
            {
                return eventType;
            }
            set
            {
                eventType = value;
            }
        }
        #endregion

        public EventData(object sender, EventType eventType)
        {
            this.sender = sender;
            this.eventType = eventType;
        }

        //useful when we intend to clone a blank event - See UIMenuObject::DoMenuAction
        public EventData(object sender)
            : this(sender, EventType.Empty)
        {
        }

        
        //could use this to detect duplicate events in the event dispatcher
        public override bool Equals(object obj)
        {
            EventData other = obj as EventData;
            return ((this.sender == other.Sender) && (this.eventType == other.Event));
        }

        public override int GetHashCode()
        {
            int seed = 31;
            return seed * this.sender.GetHashCode() + this.eventType.GetHashCode();
        }

        public override string ToString()
        {
            return "EventData - Sender: " + this.Sender + ", Event: " + this.Event;
        }
    }
}
