﻿using GDApp.GDLibrary.Objects.ADownsizeObjects;
using GDLibrary;
using JigLibX.Collision;
using JigLibX.Geometry;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Threading;

#region

/* -----------------------------------------------------
 * 
 * Version V1.9.3
 * 
 * HUD Fixed
 * 
 * ---------------------------------------------------
 * 
 * Version V1.9.2
 * 
 * Gun is positioned in players hands and rotate accordingly
/* 
 * ----------------------------------------------------
 * 
 * Version V1.9.1
 * 
 * Added Ship Sound with Distance
 * Added UIMenu Sound
 * Fixed Exit Menu Bug
 * Ship sound pauses/resumes when menu is shown/hidden
 * Removed compass image
 * 
 * Bugs -
 *      Ship sound not perfect
 *      
 * To do -
 *      footsteps, ant grunts, hum from fuse
 * 
 * Bugs: The update of the position of the gun is sometimes slower when the player moves so it looks like it is floating.
 * Version V1.9
 * 
 * ------------------------------------
 * 
 * Version V1.9
 * 
 * ------------------------------------
 * 
 * Sounds Added
 * 
 * --------------------------------------
 * 
 * Version V1.8.1
 * 
 * Fixed Camera Bug
 * 
 * * Version V1.8
 * 
 * added animation (test squirl from Nicola) with 3 different phases where i am swapping the model object inside the animated caracter
 * 
 * * Version V1.7.4
 * 
 * added animated character which is not working properly needs to be redone in 3ds max 
 * repositioned zones
 * 
 * 
 * 
/*
 * Version V1.7.3
 * 
 * ---------------------------------------------
 * 
 * Fixed Camera Zones
 * Fixed Camera Pickup Bug
 * 
 * ---------------------------------------------
 * 
 * * Version V1.7.2
 * 
 * Added rotation of the ants towards look vector

 * 
 * Version V1.7.1
 * 
 * --------------------------------------------------
 * 
 * Added Volume Increase and decrease
 * Changed MainMenu image
 * 
 * To do -
 *      Visual output of volume level (slider?)
 * 
 * --------------------------------------------------
 * 
 * Version V1.7
 * -------------------------------------------------
 * Added end mission zone and changed mission log text when you have fuse and walk into it
 * Added player respawn after the death by ants 
 * Ants are changed into character objects so they can move and are collidable 
 * 
 * Bugs-
 * when the player is respawned the zones wont register the collision with it
 * 
 * 
 * the normal vector of ants is rotating but there is something wrong with it
 * ---------------------------------------------------------------------------------
/* Version V1.6.1
 * 
 * ---------------------------------------------------------------
 * 
 * Added Menus
 * Disabled CameraZones for now
 * 
 * Bugs - 
 *      Framerate issues with CameraZones
 * 
 * ---------------------------------------------------------------
 * 
 * Version V1.6
 * -------------------------------------------------------------
 * 
 * Camera Zones added into TV
 * 
 * Bugs - 
 *      Camera doesn't always switch back to 3rd Person on exit
 *      
 * To do - 
 *      Split the ramp zone into two seperate zones to prevent spilling over to fuse zone
 * 
 * -------------------------------------------------------
 * 
 * Version V1.5.1
 * ------------------------------------------------
 * Health Bar descreases when player comes in contact with Ants
 * Weapon Bar descreases when player shoots and stops shooting when empty
 * Weapon and Health Bar regenerate over time
 * 
 * Bugs
 *      Framerate issue when player is standing inside the AntStopZone and an Ant has respawned
 * 
 * To Do
 *      Make Ants jump at character when close
 * 
 * Version V1.5
 * ------------------------------------------------
 * Added respawn event dispatcher where when event is triggered in main we have function which will be called and add new ant into object manager
 * Added zone where when player walks in the ants go back to their paths around anthill
 * Improvements the positions of respawn and zone needs to be adjusted current positions are only for testing purposes
 * 
 * Version V1.4
 * -----------------------------
 *Added path system for ants the rotation of look is not working properly.
 *With velocity added to the position the ants start speed up too fast.
 * 
 * 
 * -----------------------------
 * Version V1.31
 * 
 * -----------------------------
 * 
 * Added Camera Switch
 * 
 * ------------------------------
 * 
 * Version V1.3
 * 
 *  ------------------
 *  Added auguments pickups and effect of them on bullets
 *  improved camera:  when you click right button you can now move camera with mouse around (scrool wheel can zoom in and out)
 *  the mouse is now free to move around the screen when you are not rotating camera
 * 
 * 
 * bugs: the look vector of gun is used to fire bulets there is bug with ray from position to cursor
 *       
 * /
/*
 * Version V1.2
 * 
 *  ------------------
 * Added collidable player object with jumping 
 * Added gunObject which is taking position from player object and fires bulets in direction of look vector lifted about 10degrees
 * Added collidableAntObject which is changing color to yellow upon impact with bullet (not all ants are collidable)
 * 
 * All changes are implemented in main map 
 * 
 * 
 * bugs: the gun is rotating aroun its own axis not players one
 *       
 * /
/*
 * Version V1.1
 * 
 * ------------------
 * 
 * Added UITextureObject
 * Added UI Textures
 * Added HealthWeaponDemo()
 * Edited LoadTextures()
 * Edited UIManager heavily
 * Edited GameInfo and TextureInfo
 * 
 * Bugs:
 *      Compass jumping to wrong place
 *      Compass not culling
 * 
 */





#endregion



/*
 * Version 10 (Week 7 - End)
 * =========
 * Added TriangleMeshObject to support collision detection on complex surfaces - note TriangleMeshObjects are STATIC after creation
 * Added an EventDispatcher to allow entities to publish events
 * Added MenuManager & MenuItem
 * Added collision callback examples - AmmoCollidableObject and HealthCollidableObject - change true to false in HealthCollidableObject::CollisionSkin_callbackFn()
 * 
 * Bugs:
 * - Bug when removing objects from the PhysicsSystem - see AmmoCollidableObject::CollisionSkin_callbackFn - the problem is in the PhysicsSystem::Integrate() method
 * - Bug when rotating a model that is not originally (i.e. in 3DS Max) centered around the origin
 * 
 * To Do:
 * Add PlayerObject
 * Add AnimatedPlayerObject
 * Add code to Main::DoRestart() and all affected objects (e.g. PhysicsSystem, ObjectManager etc) to restart the game
 *
 * 
 * Version 9.1 (Week 6 - End)
 * =========
 * Added hexagon demo to illustrate the use of multiple (badly fitted) primitive skins in one object
 * Added pathfinding based on Dijkstra's Algorithm - see Main::LoadPathFindingDemo()
 * Fixed rotation on collidable objects
 * 
 * Version 9 (Week 6 - Start)
 * =========
 * Added picking to MouseManager and demo in UIMouseObject
 * Added CD/CR using JigLibX - See CollidableObject
 * Added SoundManager, AudioDemoObject, and demo audio methods in main on P key press
 * 
* To do:
 * Add character controller for physics library
 * Add MenuManager class and associated sub-classes
 * Add AnimationPlayer
 * Add Lighting & Custom Effects 
 * 
 * Version 8 (Week 5 - Start)
 * =========
 * Removed duplication across some controller classes by creating the TargetCameraController. Organised controllers into folders.
 * 
 * Version 7 (Week 5 - Start)
 * =========
 * Added textures, fonts, and model dictionaries and loaded content
 * Added UIMouseObject to allow developer to create a custom mouse icon
 * Removed TextureDictionary - it was unnecessary and its functionality could be achieved with GenericDictionary
 * Added a temporary picking demo to Main::Update()
 * 
 * Version 6 (Week 4 - End)
 * =========
 * Cleaned up the Main::InitialiseCamera() method to make it more readable
 * Added ThirdPersonCameraController and dampening lerpFactor
 * Added viewport deflate to ScreenUtility and look vector methods to CameraUtility - See Utility folder
 * Added TrackCameraController
 * Added track and rail dictionaries to allow the user to store for later use with a camera controller
 * Added UIObject and UIOBjectManager to allow developer to begin adding menu
 * Added UIDebugObject to allow developer to add camera positions, object draw count etc
 * Added Camera::Frustum to allow us to cull draw list for those objects outside the camera frustum
 * 
 * Version 5 (Week 4)
 * =========
 * Added ModelData to store model data and draw
 * Added VertexData to store vertices and draw 
 * Added VertexFactory to generate vertex data for primitives
 * Added PrimitiveObject to load and draw vertex data
 * 
 * Added ObjectManager to store objects and draw split screen
 * Added CameraManager to store cameras and support split screen
 * Added Camera::viewport to support split screen
 * Added Camera::ActivationType to allow cameras to be enabled/disabled
 * Completed the RailCameraController class
 * Added MouseManager::GetMouseRay() to create a ray based on mouse position - useful for picking
 * Implemented ICloneable for Camera, Transform3D, and GameObject classes
 * 
 * Version 4 (Week 3)
 * =========
 * Added ProjectionProperties, Transform3D
 * Added GameObject and ModelObject
 * Added RailParameters to support rail camera
 * Added Camera Controllers - FreeLook/FirstPerson, Rail
 * Added MouseManager::hasMoved()
 * Imported GenericDictionary, TextureDictionary, and Curve1D
 *  
 * Version 3
 * =========
 * Added MouseManager and KeyboardManager to capture user input.
 * Added FreeLookCamera which allows the player to move freely in X, Y, and Z axis
 * 
 * Version 2
 * =========
 * Demonstrates the use of the VertexPositionColor, VertexPositionColorTexture, and Vertex Buffer.
 * Creates an origin helper to allow the user to orient themselves within the game.
 * Creates a skybox (5 textured quads) and a grass plane (1 textured quad).
 */

namespace GDApp
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Main : Microsoft.Xna.Framework.Game
    {  
        #region Variables
        private GraphicsDeviceManager graphics;
        private SpriteBatch spriteBatch;
        private BasicEffect wireframeEffect, texturedEffect, modelEffect;

        private GenericDictionary<string, RailParameters> railDictionary;
        private GenericDictionary<string, Transform3DCurve> trackDictionary;
        private GenericDictionary<string, SpriteFont> fontDictionary;
        private GenericDictionary<string, Texture2D> textureDictionary;
        private GenericDictionary<string, ModelData> modelDataDictionary;

        private KeyboardManager keyboardManager;
        private MouseManager mouseManager;
        private ObjectManager objectManager;
        private CameraManager cameraManager;
        private UIManager hudManager;
        private PhysicsManager physicsManager;
        private SoundManager soundManager;

        private EventDispatcher eventDispatcher;

        //screen dimensions for mouse reset
        private Vector2 screenCentre;
        private Microsoft.Xna.Framework.Rectangle screenRectangle;

        //audio demo
        private AudioEmitter audioEmitter;
        private List<AudioEmitter> extraAudEmi;
        private PathFindingEngine pathEngine;
        private MenuManager menuManager;
        private PlayerCollidableObject characterObject;
        private GunCollidableObject modelObjectGun;
        private UIMouseObject mouseUI;
        private Camera thirdpersonCamera;

        private int screenWidth;
        private int screenHeigth;
        private PathFindingEngine pathEngineAnt;
        private string[] nodeNames;
        private int antCounter = 0;
        private double respawnRate = 20000;
        private double staticRespawnRate = 5000;
        private UIMenuObject uiMenuObject;
        private MenuItem menuMission;
        private bool fusePicked, hudOnScreen;
        private Effect customEffect;
        private UIMenuObject uiPoster;
        private bool mission1End;
        private UITextureObject textU1;
        private UITextureObject textU2;
        /// <summary>
        /// this is the V1 for demo of Downsize game for bitbucket
        /// </summary>

        #endregion

        #region Properties
        public GenericDictionary<string, ModelData> ModelDataDictionary
        {
            get
            {
                return modelDataDictionary;
            }
        }
        public bool FusePicked
        {
            get
            {
                return fusePicked;
            }
            set
            {
                fusePicked = value;
            }
        }
        public MenuItem MenuMission
        {
            get
            {
                return menuMission;
            }
        }
        public UIMenuObject UiMenuObject
        {
            get
            {
                return uiMenuObject;
            }
        }
        public PlayerCollidableObject CharacterObject
        {
            get
            {
                return characterObject;
            }
        }
        public SoundManager SoundManager
        {
            get
            {
                return soundManager;
            }
        }

        public AudioEmitter AudioEmitter
        {
            get
            {
                return audioEmitter;
            }
        }

        public UIMouseObject MouseUI
        {
            get
            {
                return mouseUI;
            }
        }
        public EventDispatcher EventDispatcher
        {
            get
            {
                return eventDispatcher;
            }
        }
        public SpriteBatch SpriteBatch
        {
            get
            {
                return spriteBatch;
            }
        }
        public Vector2 ScreenCentre
        {
            get
            {
                return screenCentre;
            }
        }
        public Microsoft.Xna.Framework.Rectangle ScreenRectangle
        {
            get
            {
                return screenRectangle;
            }
        }
        public UIManager UIManager
        {
            get
            {
                return hudManager;
            }
        }
        public ObjectManager ObjectManager
        {
            get
            {
                return objectManager;
            }
        }
        public CameraManager CameraManager
        {
            get
            {
                return cameraManager;
            }
        }
        public MouseManager MouseManager
        {
            get
            {
                return mouseManager;
            }
        }
        public KeyboardManager KeyboardManager
        {
            get
            {
                return keyboardManager;
            }
        }
        public PhysicsManager PhysicsManager
        {
            get
            {
                return physicsManager;
            }
        }
        #endregion

        #region Constructor & Initialization
        public Main()
            : base()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            this.screenWidth = 1280;
            this.screenHeigth = 920;
            this.fusePicked = false;
            this.hudOnScreen = true;
            this.mission1End = false;
            //add event dispatcher and register for any events
            InitializeEventDispatcher();
            RegisterForEvents();

            //demos
            LoadSoundDemo();
            LoadPathFindingDemo();

            //add dictionaries
            InitializeDictionaries();

            //load assets, rails, and tracks
            LoadTextures();

            LoadDownsizeModels();
            LoadDownsizeTextures();

            //LoadModels();
            LoadFonts();

            LoadCameraRails();
            LoadCameraTracks();

            //set the centre of the screen based on resolution
            InitializeScreen(screenWidth, screenHeigth);

            //initialize keyboard, mouse, and other managers
            InitializeManagers();
            
            //initialize the effect objects used to render primitives
            InitializeEffects();

            //load cameras, UI, objects
            ClearGame();
            LoadGame();

            base.Initialize();
        }

        private void ClearGame()
        {
            //here we could reset the eventdispatcher, uimanager, cameramanager, and objectmanager lists
        }

        private void LoadGame()
        {
            //add UI
            InitializeHUD();

            //add sky
            float worldScale = 1000;
            AddSkyBox(worldScale);

            //set to false to disable the collision skins
            ObjectManager.bDebugMode = false;

            InitialisePathFindingEngineAnt();
           // AddOriginHelper();
            
            AddDownsizeObjects();
            AddCharacterObject();
            AddZones();

            InitializeMultiCameraByZone(this.graphics.GraphicsDevice.Viewport, 0, 0);

            AddCameraZones();

            #region UIMenu Demo
            EventDispatcher.UIMenu += new MenuEventHandler(HandleUIMenu);
            #endregion

            EventDispatcher.RespawnAntEvent += EventDispatcher_RespawnAntEvent;

            EventDispatcher.HandleDeath += EventDispatcher_HandleDeath;

            EventDispatcher.Mission1End += EventDispatcher_Mission1End;

        }

        void EventDispatcher_Mission1End(EventData eventData)
        {
            if (uiPoster == null)
            {
                this.mission1End = true;
                uiPoster = new UIMenuObject(this,
                    new Transform2D(new Vector2(0, 0), 0, new Vector2(1f, 1f)),
                    this.textureDictionary["credits"],
                    this.fontDictionary["uimenu"],
                    new Color(1f, 1f, 1f, 1f));

                this.hudManager.addCredits(uiPoster);
            }
        }

        void EventDispatcher_HandleDeath(EventData eventData)
        {

            this.characterObject.CharacterBody.Position = new Vector3(-268, 6, 190);


            SoundManager.changeMusic("lostBackgroundMusic");
        }

        void EventDispatcher_RespawnAntEvent(EventData eventData)
        {
            Console.WriteLine("adding ant");
            this.antCounter++;
        }

        private void HandleUIMenu(EventData eventData)
        {
           // Console.WriteLine(eventData);
        }
        #endregion

        #region Event Dispatch & Event Registration
        //create the dispatcher to handle all events
        private void InitializeEventDispatcher()
        {
            this.eventDispatcher = new EventDispatcher(this);
            Components.Add(eventDispatcher);
        }

        //register for anything that this class is interested in e.g. Menu - Exit
        private void RegisterForEvents()
        {
            //register for the menu events
            this.eventDispatcher.MainMenu += new MenuEventHandler(HandleMenu);
        }


        //handle the relevant events
        private void HandleMenu(EventData eventData)
        {
            if (eventData.Event == EventData.EventType.Exit)
            {
                DoExit();
            }
            else if (eventData.Event == EventData.EventType.Restart)
            {
                DoRestart();
            }
            //add other event clauses here...

        }

        //call this method to re-start your game
        private void DoRestart()
        {
            throw new NotImplementedException("Restart functionality has not yet been added.");
            //to do - this could clear and re-set the contents of all managers and the event dispatcher
            //ClearGame();
            //LoadGame();
        }

        //call this method to exit your game
        private void DoExit()
        {
            this.Exit();
        }

   
        #endregion

        #region Demos
        private void LoadEventDispatcherDemo()
        {
            //add the code to be a listener
            this.eventDispatcher.MainMenu += eventDispatcher_Menu;

            //add some time delay to our demo - obviously this wouldnt normally be added
            Thread.Sleep(100);

            //generate an event like a sender
            EventDispatcher.Publish(new EventData(this, EventData.EventType.Play));

        }

        void eventDispatcher_Menu(EventData eventData)
        {
            Console.WriteLine(eventData.Sender + ":" + eventData.Event);
        }

        private void LoadSoundDemo()
        {
            this.audioEmitter = new AudioEmitter();
            this.extraAudEmi = new List<AudioEmitter>();
            this.extraAudEmi.Add(new AudioEmitter());
        }

        private void LoadKeywordsAndEventsDemo()
        {
            Console.WriteLine("=========== Demo: out keyword ===========");
            Integer4 a = new Integer4(1, 1, 1, 1);
            Integer4 b = new Integer4(2, 2, 2, 2);
            Integer4 result;
            float distance;
            Integer4.Add(a, b, out result, out distance);

            Console.WriteLine("=========== Demo: operator overloading ===========");
            Integer4 multipliedInt1 = new Integer4(1, 2, 3, 4) * 6;
            Console.WriteLine(multipliedInt1);

            Integer4 multipliedInt2 = 6 * new Integer4(1, 2, 3, 4);
            Console.WriteLine(multipliedInt2);

            Console.WriteLine("=========== Demo: Events & Custom EventArgs ===========");
            //instanciate the object
            EventSender sender = new EventSender();
            //register for the event
            sender.Updated += sender_Updated;

            //for the purposes of the demo we are manually creating this event from the listener class. obviously, this would normally happen inside the sender class
            sender.GenerateEvent();
        }

        void sender_Updated(object sender, MyEventArgs m)
        {
            //Console.WriteLine(m);
        }

        //private void InitialisePathFindingEngine()
        //{
        //    this.pathEngine = new PathFindingEngine("enemy path system");

        //    #region RANDOMISE NODES
        //    Random rand = new Random();
        //    Node tempNode = null;
        //    int RANDMAX = 200;
        //    string[] nodeNames = { "a", "b", "c", "d", "e" };
        //    #endregion

        //    #region RANDOMISE NODES
        //    int x = 0, z = 0;
        //    for (int i = 0; i < nodeNames.Length; i++)
        //    {
        //        x = rand.Next(RANDMAX) - RANDMAX / 2;
        //        z = rand.Next(RANDMAX) - RANDMAX / 2;

        //        tempNode = new Node(nodeNames[i], new Vector3(x, 10, z));
        //        pathEngine.AddNode(tempNode);
        //    }
        //    #endregion
            
        //    pathEngine.AddEdge("a", "b", 1);
        //    pathEngine.AddEdge("b", "c", 1);
        //    pathEngine.AddEdge("b", "e", 1);
        //    pathEngine.AddEdge("c", "d", 1);
        //    pathEngine.AddEdge("d", "e", 1);
        //}

      

        private void LoadPathFindingDemo()
        {
            //InitialisePathFindingEngine();
            //RunPathTest();
        }

        private void RunPathTest()
        {
            //test of path finding
            List<Node> nodeList = this.pathEngine.Find("e", "a");
            PathFindingEngine.Print(nodeList);
        }

        #endregion

        #region Initialization & Asset Load
        private void InitializeDictionaries()
        {
            //game assets
            this.fontDictionary = new GenericDictionary<string, SpriteFont>("fonts");
            this.textureDictionary = new GenericDictionary<string, Texture2D>("textures");
            this.modelDataDictionary = new GenericDictionary<string, ModelData>("models");

            //camera related
            this.trackDictionary = new GenericDictionary<string, Transform3DCurve>("camera tracks");
            this.railDictionary = new GenericDictionary<string, RailParameters>("camera rails");
        }
        
        private void InitializeScreen(int width, int height)
        {
            graphics.PreferredBackBufferWidth = width;
            graphics.PreferredBackBufferHeight = height;
            graphics.ApplyChanges();

            this.screenCentre = new Vector2(graphics.PreferredBackBufferWidth / 2, (graphics.PreferredBackBufferHeight / 2));
            this.screenRectangle = new Microsoft.Xna.Framework.Rectangle(0, 0, graphics.PreferredBackBufferWidth, graphics.PreferredBackBufferHeight);

        }
        
        private void InitializeManagers()
        {
            //physics
            this.physicsManager = new PhysicsManager(this, true);
            Components.Add(physicsManager);

            //IO
            this.keyboardManager = new KeyboardManager(this);
            Components.Add(keyboardManager);

            this.mouseManager = new MouseManager(this, false);
            Components.Add(mouseManager);

            //camera
            this.cameraManager = new CameraManager(this);
            Components.Add(cameraManager);

            //objects
            this.objectManager = new ObjectManager(this);
            Components.Add(objectManager);

            //UI
            this.hudManager = new UIManager(this);
            Components.Add(hudManager);

            //sound
            this.soundManager = new SoundManager(this,
                 "Content\\Assets\\Audio\\Demo2DSound.xgs",
                 "Content\\Assets\\Audio\\WaveBank1.xwb",
                "Content\\Assets\\Audio\\SoundBank1.xsb");
            Components.Add(soundManager);

            //menu
            Texture2D[] menuTexturesArray = { 
                                                this.textureDictionary["title"], 
                                                this.textureDictionary["options"],
                                                this.textureDictionary["volumeMenu"],
                                                this.textureDictionary["control"], 
                                                 this.textureDictionary["controlMenu"], 
                                                this.textureDictionary["exitGame"]
                                            };
            this.menuManager = new MenuManager(this, menuTexturesArray, this.fontDictionary["menu"], new Integer2(0, 0));
            Components.Add(this.menuManager);
        }
        
        private void InitializeEffects()
        {
            //A default effect used to draw primitives with the GFX card
            this.wireframeEffect = new BasicEffect(graphics.GraphicsDevice);

            //Enable colour on the vertices passed to the GFX card
            this.wireframeEffect.VertexColorEnabled = true;

            //A second effect used to draw textured primitives
            this.texturedEffect = new BasicEffect(graphics.GraphicsDevice);

            //we must enable texture rendering for this effect
            this.texturedEffect.TextureEnabled = true;

            //A third effect used to draw objects
            this.modelEffect = new BasicEffect(graphics.GraphicsDevice);

            //we must enable texture rendering for this effect
            this.modelEffect.TextureEnabled = true;

            //enable the use of directional lighting in the BasicEffect
            this.modelEffect.EnableDefaultLighting();

            //used now for animation but later we will also use this shader for custom lighting 
            this.customEffect = Content.Load<Effect>("Assets/Effects/CustomLighting");

        }

        private void LoadDownsizeModels()
        {

            this.modelDataDictionary.Add("BoxTest", new ModelData(Content.Load<Model>("Assets/DownsizeModels/BoxTest")));           
            this.modelDataDictionary.Add("antHill", new ModelData(Content.Load<Model>("Assets/DownsizeModels/AntHill")));          
            this.modelDataDictionary.Add("gardenPath", new ModelData(Content.Load<Model>("Assets/DownsizeModels/GardenPathway")));
            this.modelDataDictionary.Add("oldTv", new ModelData(Content.Load<Model>("Assets/DownsizeModels/OldTv")));
            this.modelDataDictionary.Add("player", new ModelData(Content.Load<Model>("Assets/DownsizeModels/player")));
            this.modelDataDictionary.Add("playerCenterred", new ModelData(Content.Load<Model>("Assets/DownsizeModels/playerCenterred")));
            this.modelDataDictionary.Add("Fence1", new ModelData(Content.Load<Model>("Assets/DownsizeModels/Fence1")));
            this.modelDataDictionary.Add("Fence2", new ModelData(Content.Load<Model>("Assets/DownsizeModels/Fence2")));
            this.modelDataDictionary.Add("Fence3", new ModelData(Content.Load<Model>("Assets/DownsizeModels/Fence3")));
            this.modelDataDictionary.Add("Fence4", new ModelData(Content.Load<Model>("Assets/DownsizeModels/Fence4")));
            this.modelDataDictionary.Add("HouseWall", new ModelData(Content.Load<Model>("Assets/DownsizeModels/HouseWall")));
            this.modelDataDictionary.Add("crashedShip1", new ModelData(Content.Load<Model>("Assets/DownsizeModels/crashedShip1")));
            this.modelDataDictionary.Add("crashedShip2", new ModelData(Content.Load<Model>("Assets/DownsizeModels/crashedShip2")));
            this.modelDataDictionary.Add("crashedShip3", new ModelData(Content.Load<Model>("Assets/DownsizeModels/crashedShip3")));
            this.modelDataDictionary.Add("crashedGroung", new ModelData(Content.Load<Model>("Assets/DownsizeModels/crashedGround")));
            this.modelDataDictionary.Add("logBlock", new ModelData(Content.Load<Model>("Assets/DownsizeModels/logBlock")));
            this.modelDataDictionary.Add("logBlockCenterred", new ModelData(Content.Load<Model>("Assets/DownsizeModels/logBlockCenterred")));
            this.modelDataDictionary.Add("flowerPots", new ModelData(Content.Load<Model>("Assets/DownsizeModels/flowerPots")));
            this.modelDataDictionary.Add("Ants", new ModelData(Content.Load<Model>("Assets/DownsizeModels/Ants")));
            this.modelDataDictionary.Add("spider", new ModelData(Content.Load<Model>("Assets/DownsizeModels/spider")));
            this.modelDataDictionary.Add("antCenterred", new ModelData(Content.Load<Model>("Assets/DownsizeModels/antCenterred")));
            this.modelDataDictionary.Add("gunCenterred", new ModelData(Content.Load<Model>("Assets/DownsizeModels/gunCenterred")));
            this.modelDataDictionary.Add("augumentCogWheel", new ModelData(Content.Load<Model>("Assets/DownsizeModels/augumentCogWheel")));
            this.modelDataDictionary.Add("augumentSpring", new ModelData(Content.Load<Model>("Assets/DownsizeModels/augumentSpring")));
            this.modelDataDictionary.Add("fuse", new ModelData(Content.Load<Model>("Assets/DownsizeModels/fuse")));
            this.modelDataDictionary.Add("sphereTest", new ModelData(Content.Load<Model>("Assets/DownsizeModels/sphere")));
            this.modelDataDictionary.Add("grassBlockLeft", new ModelData(Content.Load<Model>("Assets/DownsizeModels/grassBlockLeft")));
            this.modelDataDictionary.Add("grassBlockRight", new ModelData(Content.Load<Model>("Assets/DownsizeModels/grassBlockRight")));
            this.modelDataDictionary.Add("OldTvV2", new ModelData(Content.Load<Model>("Assets/DownsizeModels/OldTvV2")));
            this.modelDataDictionary.Add("BoxTV1", new ModelData(Content.Load<Model>("Assets/DownsizeModels/ObjectsInTV/BoxTV1")));
            this.modelDataDictionary.Add("PlankSideTV", new ModelData(Content.Load<Model>("Assets/DownsizeModels/ObjectsInTV/PlankSideTV")));
            this.modelDataDictionary.Add("PlankTV1", new ModelData(Content.Load<Model>("Assets/DownsizeModels/ObjectsInTV/PlankTV1")));
            this.modelDataDictionary.Add("PlankTV2", new ModelData(Content.Load<Model>("Assets/DownsizeModels/ObjectsInTV/PlankTV2")));
            this.modelDataDictionary.Add("cathodeRayTube", new ModelData(Content.Load<Model>("Assets/DownsizeModels/ObjectsInTV/cathodeRayTube")));
            this.modelDataDictionary.Add("Plugs", new ModelData(Content.Load<Model>("Assets/DownsizeModels/ObjectsInTV/Plugs")));
            this.modelDataDictionary.Add("SpiderWeb", new ModelData(Content.Load<Model>("Assets/DownsizeModels/ObjectsInTV/SpiderWeb")));
            this.modelDataDictionary.Add("Switch", new ModelData(Content.Load<Model>("Assets/DownsizeModels/ObjectsInTV/Switch")));
            this.modelDataDictionary.Add("bottomTV", new ModelData(Content.Load<Model>("Assets/DownsizeModels/ObjectsInTV/bottomTV")));
            this.modelDataDictionary.Add("characterAnimation", new ModelData(Content.Load<Model>("Assets/DownsizeModels/Animation/characterAnimation")));
            this.modelDataDictionary.Add("RedRun4", new ModelData(Content.Load<Model>("Assets/DownsizeModels/Animation/RedRun4")));
            this.modelDataDictionary.Add("Red_Jump", new ModelData(Content.Load<Model>("Assets/DownsizeModels/Animation/Red_Jump")));
            this.modelDataDictionary.Add("Red_Idle", new ModelData(Content.Load<Model>("Assets/DownsizeModels/Animation/Red_Idle")));
            this.modelDataDictionary.Add("gardenGrass", new ModelData(Content.Load<Model>("Assets/DownsizeModels/GardenGrass")));//alpha
        }

        private void LoadDownsizeTextures()
        {
            this.textureDictionary.Add("boxGrass", Content.Load<Texture2D>("Assets/DownsizeTextures/boxGrass"));
            this.textureDictionary.Add("antHillTexture", Content.Load<Texture2D>("Assets/DownsizeTextures/antHilltexture"));
            this.textureDictionary.Add("gardenPathTexture", Content.Load<Texture2D>("Assets/DownsizeTextures/gardenPathTexture"));
            this.textureDictionary.Add("tvTexture", Content.Load<Texture2D>("Assets/DownsizeTextures/tvTexture"));
            this.textureDictionary.Add("fenceTexture", Content.Load<Texture2D>("Assets/DownsizeTextures/fenceTexture"));
            this.textureDictionary.Add("HouseWallBricksTexture", Content.Load<Texture2D>("Assets/DownsizeTextures/HouseWallBricksTexture"));
            this.textureDictionary.Add("checkerTexture", Content.Load<Texture2D>("Assets/DownsizeTextures/checkerTexture"));
            this.textureDictionary.Add("SpaceshipTexture", Content.Load<Texture2D>("Assets/DownsizeTextures/SpaceshipTexture"));
            this.textureDictionary.Add("planksInTvTexture", Content.Load<Texture2D>("Assets/DownsizeTextures/planksInTvTexture"));
            this.textureDictionary.Add("cylinderInTvTexture", Content.Load<Texture2D>("Assets/DownsizeTextures/cylinderInTvTexture"));
            this.textureDictionary.Add("characterTexture", Content.Load<Texture2D>("Assets/DownsizeTextures/characterTexture"));
            this.textureDictionary.Add("cathodeRayTube", Content.Load<Texture2D>("Assets/DownsizeTextures/cathodeRayTube"));
            this.textureDictionary.Add("MetalBare", Content.Load<Texture2D>("Assets/DownsizeTextures/MetalBare"));
            this.textureDictionary.Add("plugsTexture", Content.Load<Texture2D>("Assets/DownsizeTextures/plugsTexture"));
            this.textureDictionary.Add("TVmotherboard", Content.Load<Texture2D>("Assets/DownsizeTextures/TVmotherboard"));
            this.textureDictionary.Add("spiderWeb", Content.Load<Texture2D>("Assets/DownsizeTextures/spiderWeb"));
            this.textureDictionary.Add("antTexture", Content.Load<Texture2D>("Assets/DownsizeTextures/antTexture"));
            this.textureDictionary.Add("spiderTexture", Content.Load<Texture2D>("Assets/DownsizeTextures/spiderTexture"));
            this.textureDictionary.Add("motherboardTexture", Content.Load<Texture2D>("Assets/DownsizeTextures/motherboardTexture"));
            this.textureDictionary.Add("goldCogWheelTexture", Content.Load<Texture2D>("Assets/DownsizeTextures/goldCogWheelTexture"));
            this.textureDictionary.Add("fuseTexture", Content.Load<Texture2D>("Assets/DownsizeTextures/fuseTexture"));
            this.textureDictionary.Add("logTexture2", Content.Load<Texture2D>("Assets/DownsizeTextures/logTexture2"));
            this.textureDictionary.Add("flowerpotTexture", Content.Load<Texture2D>("Assets/DownsizeTextures/flowerpotTexture"));
            this.textureDictionary.Add("bulletTexture", Content.Load<Texture2D>("Assets/DownsizeTextures/bulletTexture"));
            this.textureDictionary.Add("gunTexture", Content.Load<Texture2D>("Assets/DownsizeTextures/gunTexture"));
        }

        private void LoadTextures()
        {
            //mouse
            this.textureDictionary.Add("mouseicons", Content.Load<Texture2D>("Assets/Textures/UI/mouseicons"));
            this.textureDictionary.Add("mouseIcon", Content.Load<Texture2D>("Assets/Textures/UI/mouseIcon"));
            //UI
            this.textureDictionary.Add("health", Content.Load<Texture2D>("Assets/Textures/UI/Health"));
            this.textureDictionary.Add("weapon", Content.Load<Texture2D>("Assets/Textures/UI/Weapon"));
            this.textureDictionary.Add("healthBar", Content.Load<Texture2D>("Assets/Textures/UI/Health_Bar"));
            this.textureDictionary.Add("weaponEnergyBar", Content.Load<Texture2D>("Assets/Textures/UI/Weapon_Energy_Bar"));
            this.textureDictionary.Add("compass", Content.Load<Texture2D>("Assets/Textures/UI/Compass"));
            this.textureDictionary.Add("leftHUDBorder", Content.Load<Texture2D>("Assets/Textures/UI/AHUDLeft"));
            this.textureDictionary.Add("rightHUDBorder", Content.Load<Texture2D>("Assets/Textures/UI/AHUDRight"));
            this.textureDictionary.Add("gauge", Content.Load<Texture2D>("Assets/Textures/UI/gauge"));


         
            this.textureDictionary.Add("ml", Content.Load<Texture2D>("Assets/Textures/Game/Debug/ml"));
   

            //skybox
            this.textureDictionary.Add("back", Content.Load<Texture2D>("Assets/Textures/Game/Skybox/back"));
            this.textureDictionary.Add("right", Content.Load<Texture2D>("Assets/Textures/Game/Skybox/right"));
            this.textureDictionary.Add("left", Content.Load<Texture2D>("Assets/Textures/Game/Skybox/left"));
            this.textureDictionary.Add("front", Content.Load<Texture2D>("Assets/Textures/Game/Skybox/front"));
            this.textureDictionary.Add("sky", Content.Load<Texture2D>("Assets/Textures/Game/Skybox/sky"));

            //menu
            this.textureDictionary.Add("title", Content.Load<Texture2D>("Assets/Textures/Menu/title"));
            this.textureDictionary.Add("exitGame", Content.Load<Texture2D>("Assets/Textures/Menu/exitGame"));
            this.textureDictionary.Add("control", Content.Load<Texture2D>("Assets/Textures/Menu/control"));
            this.textureDictionary.Add("volumeMenu", Content.Load<Texture2D>("Assets/Textures/Menu/volumeMenu"));
            this.textureDictionary.Add("controlMenu", Content.Load<Texture2D>("Assets/Textures/Menu/controlMenu"));
            this.textureDictionary.Add("credits", Content.Load<Texture2D>("Assets/Textures/Menu/credits"));
            this.textureDictionary.Add("options", Content.Load<Texture2D>("Assets/Textures/Menu/options"));
            //useful
            this.textureDictionary.Add("white", Content.Load<Texture2D>("Assets/Textures/Game/white"));

        }
        
        private void LoadFonts()
        {
            this.fontDictionary.Add("hud", Content.Load<SpriteFont>("Assets/Fonts/hud"));
            this.fontDictionary.Add("uimouse", Content.Load<SpriteFont>("Assets/Fonts/uimouse"));
            this.fontDictionary.Add("menu", Content.Load<SpriteFont>("Assets/Fonts/menu"));
            this.fontDictionary.Add("uimenu", Content.Load<SpriteFont>("Assets/Fonts/uimenu"));
        }
        
        private void LoadCameraTracks()
        {
            //track 1
            Transform3DCurve transform3DCurve = new Transform3DCurve(CurveLoopType.Oscillate);
            transform3DCurve.Add(new Vector3(0, 10, 0), -Vector3.UnitY, Vector3.UnitZ, 0);
            transform3DCurve.Add(new Vector3(50, 100, 0), Vector3.UnitZ, Vector3.UnitY, 4000);
            this.trackDictionary.Add("vertigo track", transform3DCurve);
        }
        
        private void LoadCameraRails()
        {
            //add rail 1
            RailParameters r1 = new RailParameters("rail1", new Vector3(-20, 15, 30), new Vector3(20, 15, 30));
            this.railDictionary.Add(r1.Name, r1);
        }
        #endregion

        #region Primitives, Collidables & Helpers

        private void AddCameraZones()
        {
            Transform3D transform3D = null;
            CameraZoneObject cameraZoneObject = null;

            //Zone 1 & 2 - Switch Back to 3rd Person Camera
            transform3D = new Transform3D(new Vector3(143, 2.5f, -101.5f),
                new Vector3(0, 0, 0),
                5 * Vector3.One, -Vector3.UnitX, Vector3.UnitY); //look and up are irrelevant
            cameraZoneObject = new CameraZoneObject(this, transform3D, 0, this.characterObject);
            cameraZoneObject.ObjectType = ObjectType.Zone;
            cameraZoneObject.AddPrimitive(new Box(transform3D.Translation, Matrix.Identity, transform3D.Scale));
            cameraZoneObject.Enable();
            cameraZoneObject.CueName = "lostBackgroundMusic";
            this.objectManager.Add(cameraZoneObject);

            transform3D = new Transform3D(new Vector3(170, 17, -101.5f),
                new Vector3(0, 0, 0),
                new Vector3(10, 20, 5), -Vector3.UnitX, Vector3.UnitY); //look and up are irrelevant
            cameraZoneObject = new CameraZoneObject(this, transform3D, 0, this.characterObject);
            cameraZoneObject.ObjectType = ObjectType.Zone;
            cameraZoneObject.AddPrimitive(new Box(transform3D.Translation, Matrix.Identity, transform3D.Scale));
            cameraZoneObject.Enable();
            cameraZoneObject.CueName = "lostBackgroundMusic";
            this.objectManager.Add(cameraZoneObject);

            //zone 3 - Entrance to TV
            transform3D = new Transform3D(new Vector3(145.5f, 2.5f, -108),
                new Vector3(0, -90, 0),
                new Vector3(5, 5, 5), -Vector3.UnitX, Vector3.UnitY); //look and up are irrelevant
            cameraZoneObject = new CameraZoneObject(this, transform3D, 1, this.characterObject);
            cameraZoneObject.ObjectType = ObjectType.Zone;
            cameraZoneObject.AddPrimitive(new Box(transform3D.Translation, Matrix.Identity, transform3D.Scale));
            cameraZoneObject.Enable();
            cameraZoneObject.CueName = "tvMusic";
            this.objectManager.Add(cameraZoneObject);

            //zone 4 - Camera 2 Inside TV
            transform3D = new Transform3D(new Vector3(155, 2.5f, -124.5f),
                new Vector3(0, 0, 0),
                new Vector3(5, 5, 16), -Vector3.UnitX, Vector3.UnitY); //look and up are irrelevant
            cameraZoneObject = new CameraZoneObject(this, transform3D, 2, this.characterObject);
            cameraZoneObject.ObjectType = ObjectType.Zone;
            cameraZoneObject.AddPrimitive(new Box(transform3D.Translation, Matrix.Identity, transform3D.Scale));
            cameraZoneObject.Enable();
            this.objectManager.Add(cameraZoneObject);

            //zone 5 - Ramp Camera
            transform3D = new Transform3D(new Vector3(182, 2.5f, -123),
                new Vector3(0, 0, 0),
                new Vector3(5, 5, 5), -Vector3.UnitX, Vector3.UnitY); //look and up are irrelevant
            cameraZoneObject = new CameraZoneObject(this, transform3D, 3, this.characterObject);
            cameraZoneObject.ObjectType = ObjectType.Zone;
            cameraZoneObject.AddPrimitive(new Box(transform3D.Translation, Matrix.Identity, transform3D.Scale));
            cameraZoneObject.Enable();
            this.objectManager.Add(cameraZoneObject);

            //zone 6 - Upper Level 1 Camera
            transform3D = new Transform3D(new Vector3(180, 13.75f, -136.5f),
                new Vector3(0, 0, 0),
                new Vector3(5, 5, 5), -Vector3.UnitX, Vector3.UnitY); //look and up are irrelevant
            cameraZoneObject = new CameraZoneObject(this, transform3D, 4, this.characterObject);
            cameraZoneObject.ObjectType = ObjectType.Zone;
            cameraZoneObject.AddPrimitive(new Box(transform3D.Translation, Matrix.Identity, transform3D.Scale));
            cameraZoneObject.Enable();
            this.objectManager.Add(cameraZoneObject);

            //zone 7 - Upper Level 2 Camera
            transform3D = new Transform3D(new Vector3(169, 13.75f, -135),
                new Vector3(0, 0, 0),
                new Vector3(5, 5, 5), -Vector3.UnitX, Vector3.UnitY); //look and up are irrelevant
            cameraZoneObject = new CameraZoneObject(this, transform3D, 5, this.characterObject);
            cameraZoneObject.ObjectType = ObjectType.Zone;
            cameraZoneObject.AddPrimitive(new Box(transform3D.Translation, Matrix.Identity, transform3D.Scale));
            cameraZoneObject.Enable();
            this.objectManager.Add(cameraZoneObject);

            //zone 8 - Fuse Location Camera
            transform3D = new Transform3D(new Vector3(166, 8, -112f),
                new Vector3(0, 0, 0),
                new Vector3(5, 5, 5), -Vector3.UnitX, Vector3.UnitY); //look and up are irrelevant
            cameraZoneObject = new CameraZoneObject(this, transform3D, 6, this.characterObject);
            cameraZoneObject.ObjectType = ObjectType.Zone;
            cameraZoneObject.AddPrimitive(new Box(transform3D.Translation, Matrix.Identity, transform3D.Scale));
            cameraZoneObject.Enable();
            this.objectManager.Add(cameraZoneObject);

            //zone 9 - Exit Location Camera
            transform3D = new Transform3D(new Vector3(175, 13.75f, -113),
                new Vector3(0, 0, 0),
                new Vector3(5, 5, 5), -Vector3.UnitX, Vector3.UnitY); //look and up are irrelevant
            cameraZoneObject = new CameraZoneObject(this, transform3D, 7, this.characterObject);
            cameraZoneObject.ObjectType = ObjectType.Zone;
            cameraZoneObject.AddPrimitive(new Box(transform3D.Translation, Matrix.Identity, transform3D.Scale));
            cameraZoneObject.Enable();
            this.objectManager.Add(cameraZoneObject);
        }

        //private void AddOriginHelper()
        //{
        //    Transform3D transform = null;
        //    //used to draw the origin XYZ helper
        //    IVertexData originHelperVertexData = PrimitiveFactory.GetVertexDataInstance(graphics.GraphicsDevice, PrimitiveFactory.GeometryType.WireFrameOriginHelper, PrimitiveFactory.StorageType.Buffered);
        //    //draw an origin at (0, 0, 0) and scale the primitive up to be 4 times larger in size
        //    transform = new Transform3D(new Vector3(0, 20, 0), Vector3.Zero, 4 * Vector3.One, Vector3.Zero, Vector3.Zero);
        //    this.objectManager.Add(new PrimitiveObject(this, transform, this.wireframeEffect, originHelperVertexData, null, Color.White));
        //}
        
        private void AddSkyBox(float worldScale)
        {
            //used to draw a textured quad
            IVertexData texturedQuadVertexData = PrimitiveFactory.GetVertexDataInstance(graphics.GraphicsDevice, PrimitiveFactory.GeometryType.TexturedQuad, PrimitiveFactory.StorageType.Buffered);

            //sky box
            Texture2D[] textures = new Texture2D[5];
            textures[0] = this.textureDictionary["back"];
            textures[1] = this.textureDictionary["right"];
            textures[2] = this.textureDictionary["left"];
            textures[3] = this.textureDictionary["front"];
            textures[4] = this.textureDictionary["sky"];
            Components.Add(new SkyBox(this, this.texturedEffect, texturedQuadVertexData, 
                textures, worldScale, Color.White));
        }

        private void AddGroundCollidableObject(float worldScale)
        {
            CollidableObject collidableObject = null;
            Transform3D transform3D = null;
            ModelData modelData = null;
            Texture2D texture = null;

            modelData = this.modelDataDictionary["cube"];
            texture = this.textureDictionary["grass1"];
            transform3D = new Transform3D(Vector3.Zero, new Vector3(0, 0, 0), new Vector3(worldScale, 1, worldScale), Vector3.UnitX, Vector3.UnitY);
            collidableObject = new CollidableObject(this, transform3D, this.modelEffect, modelData, texture, Color.White);
            collidableObject.AddPrimitive(
                new Box(transform3D.Translation, Matrix.Identity, transform3D.Scale),
                new MaterialProperties(0.8f, 0.8f, 0.7f));
            collidableObject.Enable(true, 1);
            this.objectManager.Add(collidableObject);
        }

        private void AddDownsizeObjects()
        {
            //2.54mesurement difference for unscaled models in 3ds max to XNA
            CollidableObject collidableObject = null;
            ModelObject modelObject = null;
            Transform3D transform3D = null;
            ModelData modelData = null;
            Texture2D texture = null;
            Box primitiveBox = null; 
            MaterialProperties matProperties;

            #region NON MOVEABLE TRIANGLE MESHES
            /////////////////////////////////////////////////////////COLLIDABLE TRIANGLE MESHER///////////////////////////////

            modelData = this.modelDataDictionary["grassBlockLeft"];
            texture = this.textureDictionary["boxGrass"];
            transform3D = new Transform3D(new Vector3(0, 0, 0), new Vector3(0, 0, 0), Vector3.One, Vector3.UnitX, Vector3.UnitY);
            matProperties = new MaterialProperties(0.2f, 0.8f, 0.7f);
            collidableObject = new WorldTriangleMeshObject(this, transform3D, this.modelEffect, modelData, texture, Color.White, matProperties);
            collidableObject.Enable(true, 1);
            collidableObject.ObjectType = ObjectType.Pickup;
            this.objectManager.setAlphaGrassL(collidableObject);

            modelData = this.modelDataDictionary["grassBlockRight"];
            texture = this.textureDictionary["boxGrass"];
            transform3D = new Transform3D(new Vector3(0, 0, 0), new Vector3(0, 0, 0), Vector3.One, Vector3.UnitX, Vector3.UnitY);
            matProperties = new MaterialProperties(0.2f, 0.8f, 0.7f);
            collidableObject = new WorldTriangleMeshObject(this, transform3D, this.modelEffect, modelData, texture, Color.White, matProperties);
            collidableObject.Enable(true, 1);
            collidableObject.ObjectType = ObjectType.Pickup;
            this.objectManager.setAlphaGrassR(collidableObject);

            modelData = this.modelDataDictionary["flowerPots"];
            texture = this.textureDictionary["flowerpotTexture"];
            transform3D = new Transform3D(new Vector3(0, 0, 0), new Vector3(0, 0, 0), Vector3.One, Vector3.UnitX, Vector3.UnitY);
            matProperties = new MaterialProperties(0.2f, 0.8f, 0.7f);
            collidableObject = new WorldTriangleMeshObject(this, transform3D, this.modelEffect, modelData, texture, Color.White, matProperties);
            collidableObject.Enable(true, 1);
            collidableObject.ObjectType = ObjectType.Pickup;
            this.objectManager.Add(collidableObject);

            modelData = this.modelDataDictionary["logBlock"];
            texture = this.textureDictionary["logTexture2"];
            transform3D = new Transform3D(new Vector3(0, 0, 0), new Vector3(0, 0, 0), Vector3.One, Vector3.UnitX, Vector3.UnitY);
            matProperties = new MaterialProperties(0.2f, 0.8f, 0.7f);
            collidableObject = new WorldTriangleMeshObject(this, transform3D, this.modelEffect, modelData, texture, Color.White, matProperties);
            collidableObject.Enable(true, 1);
            collidableObject.ObjectType = ObjectType.Pickup;
            this.objectManager.Add(collidableObject);


            modelData = this.modelDataDictionary["gardenPath"];
            texture = this.textureDictionary["gardenPathTexture"];
            transform3D = new Transform3D(new Vector3(0, 0, 0), new Vector3(0, 0, 0), Vector3.One, Vector3.UnitX, Vector3.UnitY);
            matProperties = new MaterialProperties(0.2f, 0.8f, 0.7f);
            collidableObject = new WorldTriangleMeshObject(this, transform3D, this.modelEffect, modelData, texture, Color.White, matProperties);
            collidableObject.Enable(true, 1);
            collidableObject.ObjectType = ObjectType.Pickup;
            this.objectManager.Add(collidableObject);

            modelData = this.modelDataDictionary["gardenGrass"];
            texture = this.textureDictionary["boxGrass"];
            transform3D = new Transform3D(new Vector3(0, 0, 0), new Vector3(0, 0, 0), Vector3.One, Vector3.UnitX, Vector3.UnitY);
            matProperties = new MaterialProperties(0.2f, 0.8f, 0.7f);
            collidableObject = new WorldTriangleMeshObject(this, transform3D, this.modelEffect, modelData, texture, Color.White, matProperties);
            collidableObject.Enable(true, 1);
            collidableObject.ObjectType = ObjectType.Pickup;
            this.objectManager.setAlphaGrass(collidableObject);

            modelData = this.modelDataDictionary["antHill"];
            texture = this.textureDictionary["antHillTexture"];
            transform3D = new Transform3D(new Vector3(0, 0, 0), new Vector3(0, 0, 0), Vector3.One, Vector3.UnitX, Vector3.UnitY);
            matProperties = new MaterialProperties(0.2f, 0.8f, 0.7f);
            collidableObject = new WorldTriangleMeshObject(this, transform3D, this.modelEffect, modelData, texture, Color.White, matProperties);
            collidableObject.Enable(true, 1);
            collidableObject.ObjectType = ObjectType.Pickup;
            this.objectManager.Add(collidableObject);

            #region oldTV

            modelData = this.modelDataDictionary["OldTvV2"];
            texture = this.textureDictionary["tvTexture"];
            transform3D = new Transform3D(new Vector3(0, 0, 0), new Vector3(0, 0, 0), Vector3.One, Vector3.UnitX, Vector3.UnitY);
            matProperties = new MaterialProperties(0.2f, 0.8f, 0.7f);
            collidableObject = new WorldTriangleMeshObject(this, transform3D, this.modelEffect, modelData, texture, Color.White, matProperties);
            collidableObject.Enable(true, 1);
            collidableObject.ObjectType = ObjectType.Pickup;
            this.objectManager.Add(collidableObject);

            modelData = this.modelDataDictionary["BoxTV1"];
            texture = this.textureDictionary["planksInTvTexture"];
            transform3D = new Transform3D(new Vector3(0, 0, 0), new Vector3(0, 0, 0), Vector3.One, Vector3.UnitX, Vector3.UnitY);
            matProperties = new MaterialProperties(0.2f, 0.8f, 0.7f);
            collidableObject = new WorldTriangleMeshObject(this, transform3D, this.modelEffect, modelData, texture, Color.White, matProperties);
            collidableObject.Enable(true, 1);
            collidableObject.ObjectType = ObjectType.Pickup;
            this.objectManager.Add(collidableObject);

            modelData = this.modelDataDictionary["bottomTV"];
            texture = this.textureDictionary["motherboardTexture"];
            transform3D = new Transform3D(new Vector3(0, 0, 0), new Vector3(0, 0, 0), Vector3.One, Vector3.UnitX, Vector3.UnitY);
            matProperties = new MaterialProperties(0.2f, 0.8f, 0.7f);
            collidableObject = new WorldTriangleMeshObject(this, transform3D, this.modelEffect, modelData, texture, Color.White, matProperties);
            collidableObject.Enable(true, 1);
            collidableObject.ObjectType = ObjectType.Pickup;
            this.objectManager.Add(collidableObject);

            modelData = this.modelDataDictionary["PlankSideTV"];
            texture = this.textureDictionary["motherboardTexture"];
            transform3D = new Transform3D(new Vector3(0, 0, 0), new Vector3(0, 0, 0), Vector3.One, Vector3.UnitX, Vector3.UnitY);
            matProperties = new MaterialProperties(0.2f, 0.8f, 0.7f);
            collidableObject = new WorldTriangleMeshObject(this, transform3D, this.modelEffect, modelData, texture, Color.White, matProperties);
            collidableObject.Enable(true, 1);
            collidableObject.ObjectType = ObjectType.Pickup;
            this.objectManager.Add(collidableObject);

            modelData = this.modelDataDictionary["PlankTV1"];
            texture = this.textureDictionary["planksInTvTexture"];
            transform3D = new Transform3D(new Vector3(0, 0, 0), new Vector3(0, 0, 0), Vector3.One, Vector3.UnitX, Vector3.UnitY);
            matProperties = new MaterialProperties(0.2f, 0.8f, 0.7f);
            collidableObject = new WorldTriangleMeshObject(this, transform3D, this.modelEffect, modelData, texture, Color.White, matProperties);
            collidableObject.Enable(true, 1);
            collidableObject.ObjectType = ObjectType.Pickup;
            this.objectManager.Add(collidableObject);

            modelData = this.modelDataDictionary["PlankTV2"];
            texture = this.textureDictionary["planksInTvTexture"];
            transform3D = new Transform3D(new Vector3(0, 0, 0), new Vector3(0, 0, 0), Vector3.One, Vector3.UnitX, Vector3.UnitY);
            matProperties = new MaterialProperties(0.2f, 0.8f, 0.7f);
            collidableObject = new WorldTriangleMeshObject(this, transform3D, this.modelEffect, modelData, texture, Color.White, matProperties);
            collidableObject.Enable(true, 1);
            collidableObject.ObjectType = ObjectType.Pickup;
            this.objectManager.Add(collidableObject);

            modelData = this.modelDataDictionary["cathodeRayTube"];
            texture = this.textureDictionary["cathodeRayTube"];
            transform3D = new Transform3D(new Vector3(0, 0, 0), new Vector3(0, 0, 0), Vector3.One, Vector3.UnitX, Vector3.UnitY);
            matProperties = new MaterialProperties(0.2f, 0.8f, 0.7f);
            collidableObject = new WorldTriangleMeshObject(this, transform3D, this.modelEffect, modelData, texture, Color.White, matProperties);
            collidableObject.Enable(true, 1);
            collidableObject.ObjectType = ObjectType.Pickup;
            this.objectManager.Add(collidableObject);

            modelData = this.modelDataDictionary["Plugs"];
            texture = this.textureDictionary["plugsTexture"];
            transform3D = new Transform3D(new Vector3(0, 0, 0), new Vector3(0, 0, 0), Vector3.One, Vector3.UnitX, Vector3.UnitY);
            matProperties = new MaterialProperties(0.2f, 0.8f, 0.7f);
            collidableObject = new WorldTriangleMeshObject(this, transform3D, this.modelEffect, modelData, texture, Color.White, matProperties);
            collidableObject.Enable(true, 1);
            collidableObject.ObjectType = ObjectType.Pickup;
            this.objectManager.Add(collidableObject);

            modelData = this.modelDataDictionary["SpiderWeb"];
            texture = this.textureDictionary["spiderWeb"];
            transform3D = new Transform3D(new Vector3(0, 0, 0), new Vector3(0, 0, 0), Vector3.One, Vector3.UnitX, Vector3.UnitY);
            matProperties = new MaterialProperties(0.2f, 0.8f, 0.7f);
            collidableObject = new WorldTriangleMeshObject(this, transform3D, this.modelEffect, modelData, texture, Color.White, matProperties);
            collidableObject.Enable(true, 1);
            collidableObject.ObjectType = ObjectType.Pickup;
            this.objectManager.Add(collidableObject);

            modelData = this.modelDataDictionary["Switch"];
            texture = this.textureDictionary["MetalBare"];
            transform3D = new Transform3D(new Vector3(0, 0, 0), new Vector3(0, 0, 0), Vector3.One, Vector3.UnitX, Vector3.UnitY);
            matProperties = new MaterialProperties(0.2f, 0.8f, 0.7f);
            collidableObject = new WorldTriangleMeshObject(this, transform3D, this.modelEffect, modelData, texture, Color.White, matProperties);
            collidableObject.Enable(true, 1);
            collidableObject.ObjectType = ObjectType.Pickup;
            this.objectManager.Add(collidableObject);

            #endregion


            modelData = this.modelDataDictionary["Fence1"];
            texture = this.textureDictionary["fenceTexture"];
            transform3D = new Transform3D(new Vector3(0, 0, 0), new Vector3(0, 0, 0), Vector3.One, Vector3.UnitX, Vector3.UnitY);
            matProperties = new MaterialProperties(0.2f, 0.8f, 0.7f);
            collidableObject = new WorldTriangleMeshObject(this, transform3D, this.modelEffect, modelData, texture, Color.White, matProperties);
            collidableObject.Enable(true, 1);
            collidableObject.ObjectType = ObjectType.Pickup;
            this.objectManager.Add(collidableObject);

            modelData = this.modelDataDictionary["Fence2"];
            texture = this.textureDictionary["fenceTexture"];
            transform3D = new Transform3D(new Vector3(0, 0, 0), new Vector3(0, 0, 0), Vector3.One, Vector3.UnitX, Vector3.UnitY);
            matProperties = new MaterialProperties(0.2f, 0.8f, 0.7f);
            collidableObject = new WorldTriangleMeshObject(this, transform3D, this.modelEffect, modelData, texture, Color.White, matProperties);
            collidableObject.Enable(true, 1);
            collidableObject.ObjectType = ObjectType.Pickup;
            this.objectManager.Add(collidableObject);

            modelData = this.modelDataDictionary["Fence3"];
            texture = this.textureDictionary["fenceTexture"];
            transform3D = new Transform3D(new Vector3(0, 0, 0), new Vector3(0, 0, 0), Vector3.One, Vector3.UnitX, Vector3.UnitY);
            matProperties = new MaterialProperties(0.2f, 0.8f, 0.7f);
            collidableObject = new WorldTriangleMeshObject(this, transform3D, this.modelEffect, modelData, texture, Color.White, matProperties);
            collidableObject.Enable(true, 1);
            collidableObject.ObjectType = ObjectType.Pickup;
            this.objectManager.Add(collidableObject);

            modelData = this.modelDataDictionary["Fence4"];
            texture = this.textureDictionary["fenceTexture"];
            transform3D = new Transform3D(new Vector3(0, 0, 0), new Vector3(0, 0, 0), Vector3.One, Vector3.UnitX, Vector3.UnitY);
            matProperties = new MaterialProperties(0.2f, 0.8f, 0.7f);
            collidableObject = new WorldTriangleMeshObject(this, transform3D, this.modelEffect, modelData, texture, Color.White, matProperties);
            collidableObject.Enable(true, 1);
            collidableObject.ObjectType = ObjectType.Pickup;
            this.objectManager.Add(collidableObject);

            modelData = this.modelDataDictionary["HouseWall"];
            texture = this.textureDictionary["HouseWallBricksTexture"];
            transform3D = new Transform3D(new Vector3(0, 0, 0), new Vector3(0, 0, 0), Vector3.One, Vector3.UnitX, Vector3.UnitY);
            matProperties = new MaterialProperties(0.2f, 0.8f, 0.7f);
            collidableObject = new WorldTriangleMeshObject(this, transform3D, this.modelEffect, modelData, texture, Color.White, matProperties);
            collidableObject.Enable(true, 1);
            collidableObject.ObjectType = ObjectType.Pickup;
            this.objectManager.Add(collidableObject);

            modelData = this.modelDataDictionary["crashedShip1"];
            texture = this.textureDictionary["SpaceshipTexture"];
            transform3D = new Transform3D(new Vector3(0, 0, 0), new Vector3(0, 0, 0), Vector3.One, Vector3.UnitX, Vector3.UnitY);
            matProperties = new MaterialProperties(0.2f, 0.8f, 0.7f);
            collidableObject = new WorldTriangleMeshObject(this, transform3D, this.modelEffect, modelData, texture, Color.White, matProperties);
            collidableObject.Enable(true, 1);
            collidableObject.ObjectType = ObjectType.Pickup;
            this.objectManager.Add(collidableObject);

            modelData = this.modelDataDictionary["crashedShip2"];
            texture = this.textureDictionary["SpaceshipTexture"];
            transform3D = new Transform3D(new Vector3(0, 0, 0), new Vector3(0, 0, 0), Vector3.One, Vector3.UnitX, Vector3.UnitY);
            matProperties = new MaterialProperties(0.2f, 0.8f, 0.7f);
            collidableObject = new WorldTriangleMeshObject(this, transform3D, this.modelEffect, modelData, texture, Color.White, matProperties);
            collidableObject.Enable(true, 1);
            collidableObject.ObjectType = ObjectType.Pickup;
            this.objectManager.Add(collidableObject);

            modelData = this.modelDataDictionary["crashedShip3"];
            texture = this.textureDictionary["SpaceshipTexture"];
            transform3D = new Transform3D(new Vector3(0, 0, 0), new Vector3(0, 0, 0), Vector3.One, Vector3.UnitX, Vector3.UnitY);
            matProperties = new MaterialProperties(0.2f, 0.8f, 0.7f);
            collidableObject = new WorldTriangleMeshObject(this, transform3D, this.modelEffect, modelData, texture, Color.White, matProperties);
            collidableObject.Enable(true, 1);
            collidableObject.ObjectType = ObjectType.Pickup;
            this.objectManager.Add(collidableObject);

            modelData = this.modelDataDictionary["crashedGroung"];
            texture = this.textureDictionary["gardenPathTexture"];
            transform3D = new Transform3D(new Vector3(0, 0, 0), new Vector3(0, 0, 0), Vector3.One, Vector3.UnitX, Vector3.UnitY);
            matProperties = new MaterialProperties(0.2f, 0.8f, 0.7f);
            collidableObject = new WorldTriangleMeshObject(this, transform3D, this.modelEffect, modelData, texture, Color.White, matProperties);
            collidableObject.Enable(true, 1);
            collidableObject.ObjectType = ObjectType.Pickup;
            this.objectManager.Add(collidableObject);


            #endregion


            #region NON COLLIDABLE OBJECTS
            //////////////////////MODEL OBJECTS/////////////////////////////////////

            modelData = this.modelDataDictionary["spider"];
            texture = this.textureDictionary["spiderTexture"];
            transform3D = new Transform3D(new Vector3(0, 0, 0), new Vector3(0, 0, 0), Vector3.One, Vector3.UnitX, Vector3.UnitY);
            modelObject = new ModelObject(this, transform3D, this.modelEffect, modelData, texture, Color.White);
            this.objectManager.Add(modelObject);


            #endregion

            modelData = this.modelDataDictionary["antCenterred"];
            texture = this.textureDictionary["antTexture"];
            transform3D = new Transform3D(new Vector3(62, 2, 92), new Vector3(-90, 0, 0), new Vector3(1, 1, 1), -Vector3.UnitX, Vector3.UnitY);
            matProperties = new MaterialProperties(0.2f, 0.8f, 0.7f);
            collidableObject = new EnemyCollidableObject(this, transform3D, this.modelEffect, modelData, texture, Color.White, this.pathEngineAnt, this.nodeNames, 1.2f, 0.3f, 1, 1);
            collidableObject.Enable(false, 1);
            collidableObject.ObjectType = ObjectType.Pickup;
            this.objectManager.Add(collidableObject);

            modelData = this.modelDataDictionary["antCenterred"];
            texture = this.textureDictionary["antTexture"];
            transform3D = new Transform3D(new Vector3(48, 2, 102), new Vector3(-90, 0, 0), new Vector3(1, 1, 1), -Vector3.UnitX, Vector3.UnitY);
            matProperties = new MaterialProperties(0.2f, 0.8f, 0.7f);
            collidableObject = new EnemyCollidableObject(this, transform3D, this.modelEffect, modelData, texture, Color.White, this.pathEngineAnt, this.nodeNames, 1.2f, 0.3f, 1, 1);
            collidableObject.Enable(false, 1);
            collidableObject.ObjectType = ObjectType.Pickup;
            this.objectManager.Add(collidableObject);

            modelData = this.modelDataDictionary["antCenterred"];
            texture = this.textureDictionary["antTexture"];
            Transform3D transform3DRotCube = new Transform3D(new Vector3(44, 2, 89), new Vector3(-90, 0, 0), new Vector3(1, 1, 1), -Vector3.UnitX, Vector3.UnitY);
            transform3DRotCube.rotateByAxisAngle(Vector3.UnitX, 0.9f);
            matProperties = new MaterialProperties(0.2f, 0.8f, 0.7f);
            collidableObject = new EnemyCollidableObject(this, transform3DRotCube, this.modelEffect, modelData, texture, Color.White, this.pathEngineAnt, this.nodeNames, 1.2f, 0.3f, 1, 1);
            collidableObject.Enable(false, 1);
            collidableObject.ObjectType = ObjectType.Pickup;
            this.objectManager.Add(collidableObject);

          
            //////////////////////////////////////COLLIDABLE PICKABLE AUGUMENTS//////////////////////////////////////////////////////////

            modelData = this.modelDataDictionary["fuse"];
            texture = this.textureDictionary["fuseTexture"];
            transform3D = new Transform3D(new Vector3(164, 1f, -110), new Vector3(0, 0, 0), new Vector3(1, 1, 1), -Vector3.UnitZ, Vector3.UnitY);
            matProperties = new MaterialProperties(0.2f, 0.8f, 0.7f);
            collidableObject = new CollidableObject(this, transform3D, this.modelEffect, modelData, texture, Color.White);
            primitiveBox = new Box(transform3D.Translation, Matrix.Identity, new Vector3(1f, 1.5f, 1f)); //boxes are scaled for both auguments
            collidableObject.AddPrimitive(primitiveBox, matProperties);
            collidableObject.Enable(true, 1);
            collidableObject.ObjectType = ObjectType.MissionPickup;
            this.objectManager.Add(collidableObject);

            modelData = this.modelDataDictionary["augumentCogWheel"];
            texture = this.textureDictionary["goldCogWheelTexture"];
            transform3D = new Transform3D(new Vector3(-109, 0.5f, -6), new Vector3(0, 0, 0), new Vector3(1, 1, 1), -Vector3.UnitZ, Vector3.UnitY);
            matProperties = new MaterialProperties(0.2f, 0.8f, 0.7f);
            collidableObject = new CollidableObject(this, transform3D, this.modelEffect, modelData, texture, Color.White);
            primitiveBox = new Box(transform3D.Translation, Matrix.Identity, new Vector3(1.5f, 1.7f, 0.3f)); //boxes are scaled for both auguments
            collidableObject.AddPrimitive(primitiveBox, matProperties);
            collidableObject.Enable(true, 1);
            collidableObject.ObjectType = ObjectType.AugumentCogWheel;
            this.objectManager.Add(collidableObject);

            modelData = this.modelDataDictionary["augumentSpring"];
            texture = this.textureDictionary["MetalBare"];
            transform3D = new Transform3D(new Vector3(139, 1f, -141), new Vector3(0, 0, 0), new Vector3(2, 2, 2), -Vector3.UnitZ, Vector3.UnitY);
            matProperties = new MaterialProperties(0.2f, 0.8f, 0.7f);
            collidableObject = new CollidableObject(this, transform3D, this.modelEffect, modelData, texture, Color.White);
            primitiveBox = new Box(transform3D.Translation, Matrix.Identity, new Vector3(0.5f, 1.5f, 0.5f));
            collidableObject.AddPrimitive(primitiveBox, matProperties);
            collidableObject.Enable(true, 1);
            collidableObject.ObjectType = ObjectType.AugumentSpring;
            this.objectManager.Add(collidableObject);

        }

        private void InitialisePathFindingEngineAnt()
        {
            this.pathEngineAnt = new PathFindingEngine("ant path system");

            Node tempNode = null;
            nodeNames = new string[] { "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n" };

            tempNode = new Node("a", new Vector3(61, 2, 38));
            pathEngineAnt.AddNode(tempNode);
            tempNode = new Node("b", new Vector3(34, 2, 80));
            pathEngineAnt.AddNode(tempNode);
            tempNode = new Node("c", new Vector3(-4, 2, 56));
            pathEngineAnt.AddNode(tempNode);
            tempNode = new Node("d", new Vector3(16, 2, 86));
            pathEngineAnt.AddNode(tempNode);
            tempNode = new Node("e", new Vector3(133, 2, 15));
            pathEngineAnt.AddNode(tempNode);
            tempNode = new Node("f", new Vector3(37, 2, 22));
            pathEngineAnt.AddNode(tempNode);
            tempNode = new Node("g", new Vector3(-2, 2, 42));
            pathEngineAnt.AddNode(tempNode);
            tempNode = new Node("h", new Vector3(-12, 2, 91));
            pathEngineAnt.AddNode(tempNode);
            tempNode = new Node("i", new Vector3(26, 2, 127));
            pathEngineAnt.AddNode(tempNode);
            tempNode = new Node("j", new Vector3(58, 2, 19));
            pathEngineAnt.AddNode(tempNode);
            tempNode = new Node("k", new Vector3(32, 2, 61));
            pathEngineAnt.AddNode(tempNode);
            tempNode = new Node("l", new Vector3(5, 2, 79));
            pathEngineAnt.AddNode(tempNode);
            tempNode = new Node("m", new Vector3(56, 2, 71));
            pathEngineAnt.AddNode(tempNode);
            tempNode = new Node("n", new Vector3(51, 2, 99));
            pathEngineAnt.AddNode(tempNode);

            pathEngineAnt.AddEdge("a", "b", 1);
            pathEngineAnt.AddEdge("b", "c", 1);
            pathEngineAnt.AddEdge("c", "d", 1);
            pathEngineAnt.AddEdge("d", "e", 1);
            pathEngineAnt.AddEdge("e", "a", 1);
            pathEngineAnt.AddEdge("e", "f", 1);
            pathEngineAnt.AddEdge("f", "g", 1);
            pathEngineAnt.AddEdge("f", "a", 1);
            pathEngineAnt.AddEdge("g", "h", 1);
            pathEngineAnt.AddEdge("g", "a", 1);
            pathEngineAnt.AddEdge("h", "i", 1);
            pathEngineAnt.AddEdge("h", "a", 1);
            pathEngineAnt.AddEdge("i", "b", 1);
            pathEngineAnt.AddEdge("i", "a", 1);

            pathEngineAnt.AddEdge("j", "a", 1);
            pathEngineAnt.AddEdge("j", "e", 1);
            pathEngineAnt.AddEdge("j", "f", 1);
            pathEngineAnt.AddEdge("j", "k", 1);

            pathEngineAnt.AddEdge("k", "g", 1);
            pathEngineAnt.AddEdge("k", "f", 1);
            pathEngineAnt.AddEdge("k", "a", 1);
            pathEngineAnt.AddEdge("k", "l", 1);

            pathEngineAnt.AddEdge("l", "g", 1);
            pathEngineAnt.AddEdge("l", "f", 1);
            pathEngineAnt.AddEdge("l", "m", 1);
            pathEngineAnt.AddEdge("l", "h", 1);

            pathEngineAnt.AddEdge("m", "a", 1);
            pathEngineAnt.AddEdge("m", "i", 1);
            pathEngineAnt.AddEdge("m", "h", 1);
            pathEngineAnt.AddEdge("m", "b", 1);

            pathEngineAnt.AddEdge("n", "a", 1);
            pathEngineAnt.AddEdge("n", "i", 1);
            pathEngineAnt.AddEdge("n", "b", 1);
            pathEngineAnt.AddEdge("n", "k", 1);

        }

        private void AddRespawnedAnt()
        {
           
            CollidableObject collidableObject = null;
            Transform3D transform3D = null;
            ModelData modelData = null;
            Texture2D texture = null;

            MaterialProperties matProperties;
            modelData = this.modelDataDictionary["antCenterred"];
            texture = this.textureDictionary["antTexture"];
            transform3D = new Transform3D(new Vector3(62, 2, 92), new Vector3(-90, 0, 0), new Vector3(1, 1, 1), -Vector3.UnitX, Vector3.UnitY);
            matProperties = new MaterialProperties(0.2f, 0.8f, 0.7f);
            collidableObject = new EnemyCollidableObject(this, transform3D, this.modelEffect, modelData, texture, Color.White, this.pathEngineAnt, this.nodeNames, 1.2f, 0.3f, 1, 1);
            collidableObject.Enable(false, 1);
            collidableObject.ObjectType = ObjectType.Pickup;
            this.objectManager.Add(collidableObject);
      
        }

        private void AddZones()
        {
            Transform3D transform3D = null;
            AntZoneObject antZoneObj = null;
            AntStopZoneObject antStopFollowZone = null;
            EndMissionZone missionZone = null;

            //zone ant 1
            transform3D = new Transform3D(new Vector3(-24, 10, 121),
                new Vector3(0, 0, 0),
                new Vector3(10,20,40), -Vector3.UnitX, Vector3.UnitY); //look and up are irrelevant
            antZoneObj = new AntZoneObject(this, transform3D, this.characterObject);
            antZoneObj.ObjectType = ObjectType.Zone;
            antZoneObj.AddPrimitive(new Box(transform3D.Translation, Matrix.Identity, transform3D.Scale));
            antZoneObj.Enable();
            this.objectManager.Add(antZoneObj);


            //stop follow zone
            transform3D = new Transform3D(new Vector3(-44, 10, 121),
                new Vector3(0, 0, 0),
                 new Vector3(10, 20, 60), -Vector3.UnitX, Vector3.UnitY); //look and up are irrelevant
            antStopFollowZone = new AntStopZoneObject(this, transform3D, this.characterObject);
            antStopFollowZone.ObjectType = ObjectType.Zone;
            antStopFollowZone.AddPrimitive(new Box(transform3D.Translation, Matrix.Identity, transform3D.Scale));
            antStopFollowZone.Enable();
            this.objectManager.Add(antStopFollowZone);

            //start zone2
            transform3D = new Transform3D(new Vector3(72, 10, -33),
               new Vector3(0, 60, 0),
               new Vector3(10, 20, 100), -Vector3.UnitX, Vector3.UnitY); //look and up are irrelevant
            antZoneObj = new AntZoneObject(this, transform3D, this.characterObject);
            antZoneObj.ObjectType = ObjectType.Zone;
            antZoneObj.AddPrimitive(new Box(transform3D.Translation, Matrix.Identity, transform3D.Scale));
            antZoneObj.Enable();
            this.objectManager.Add(antZoneObj);


            //stop follow zone 2
            transform3D = new Transform3D(new Vector3(72, 10, -50),
                new Vector3(0, 60, 0),
                 new Vector3(10, 20, 120), -Vector3.UnitX, Vector3.UnitY); //look and up are irrelevant
            antStopFollowZone = new AntStopZoneObject(this, transform3D, this.characterObject);
            antStopFollowZone.ObjectType = ObjectType.Zone;
            antStopFollowZone.AddPrimitive(new Box(transform3D.Translation, Matrix.Identity, transform3D.Scale));
            antStopFollowZone.Enable();
            this.objectManager.Add(antStopFollowZone);

            //mission 1 zone
            transform3D = new Transform3D(new Vector3(-261, 0, 199),
                new Vector3(0, 0, 0),
                new Vector3(40, 10, 40), -Vector3.UnitX, Vector3.UnitY); //look and up are irrelevant
            missionZone = new EndMissionZone(this, transform3D, this.characterObject);
            missionZone.ObjectType = ObjectType.Zone;
            missionZone.AddPrimitive(new Box(transform3D.Translation, Matrix.Identity, transform3D.Scale));
            missionZone.Enable();
            this.objectManager.Add(missionZone);

        }


        private void AddCharacterObject()
        {
            //2.54mesurement difference for unscaled models in 3ds max to XNA
            Transform3D transform3D = null;
            ModelData modelData = null;
            Texture2D texture = null;
            MaterialProperties matProperties;


            BulletCollidableObject collidableObjectBullet = null;
            modelData = this.modelDataDictionary["sphereTest"];
            texture = this.textureDictionary["bulletTexture"];
            transform3D = new Transform3D(new Vector3(0, 0, 0), new Vector3(0, 0, 0), Vector3.One, -Vector3.UnitZ, Vector3.UnitY);
            collidableObjectBullet = new BulletCollidableObject(this, transform3D, this.modelEffect, modelData, texture, Color.White, 0.15f);

            modelData = this.modelDataDictionary["gunCenterred"];
            texture = this.textureDictionary["gunTexture"];
            //texture = null;
            transform3D = new Transform3D(new Vector3(-268, 6, 190), new Vector3(0, 0, 0), new Vector3(1, 1, 1), -Vector3.UnitZ, Vector3.UnitY);
            matProperties = new MaterialProperties(0.2f, 0.8f, 0.7f);
            modelObjectGun = new GunCollidableObject(this, transform3D, this.modelEffect, modelData, texture, Color.White, collidableObjectBullet);
            this.objectManager.Add(modelObjectGun);

             modelData = this.modelDataDictionary["Red_Idle"];
            texture = this.textureDictionary["checkerTexture"];
            //near ship -272, 5, 193   near ants -268, 6, 190
            transform3D = new Transform3D(new Vector3(-268, 6, 190),
              new Vector3(-90, 0, 0),
                 Vector3.One, //y-z are reversed because the capsule is rotation by 90 degrees around X-axis - See CharacterObject constructor
                 Vector3.UnitX, Vector3.UnitY);
            this.characterObject = new PlayerCollidableObject(this, transform3D, this.customEffect, modelData, texture, Color.White,
                KeyInfo.Key_Array_Player,
                1f, 1f, 1, 1, "Take 001", new Vector3(0, 0.3f, 0), modelObjectGun);
            this.characterObject.ObjectType = ObjectType.Player;
            this.characterObject.Enable(false, 1);
            this.objectManager.Add(characterObject);

        }


        #endregion

        #region UI
        private void InitializeHUD()
        {
            //Health and Weapon
            this.hudManager.Add(new UITextureObject(this, this.textureDictionary["healthBar"], TextureInfo.SOURCE_RECTANGLE_HUD_HEALTH_BAR, Color.White, 0, 1f, this.fontDictionary["hud"], new Vector2(GameInfo.HEALTH_BAR_WIDTH / 2, (this.screenCentre.Y * 2) - (GameInfo.HEALTH_BAR_HEIGHT / 2))));
            this.hudManager.Add(new UITextureObject(this, this.textureDictionary["weaponEnergyBar"], TextureInfo.SOURCE_RECTANGLE_HUD_WEAPON_BAR, Color.White, 0, 1f, this.fontDictionary["hud"], new Vector2((this.screenCentre.X * 2) - (GameInfo.WEAPON_BAR_WIDTH / 2), (this.screenCentre.Y * 2) - (GameInfo.WEAPON_BAR_HEIGHT / 2))));
            this.hudManager.Add(new UITextureObject(this, this.textureDictionary["health"], TextureInfo.SOURCE_RECTANGLE_HUD_HEALTH_BAR, Color.White, 0, 1f, this.fontDictionary["hud"], new Vector2(GameInfo.HEALTH_BAR_WIDTH / 2, (this.screenCentre.Y * 2) - (GameInfo.HEALTH_BAR_HEIGHT / 2))));
            this.hudManager.Add(new UITextureObject(this, this.textureDictionary["weapon"], TextureInfo.SOURCE_RECTANGLE_HUD_WEAPON_BAR, Color.White, 0, 1f, this.fontDictionary["hud"], new Vector2((this.screenCentre.X * 2) - (GameInfo.WEAPON_BAR_WIDTH / 2), (this.screenCentre.Y * 2) - (GameInfo.WEAPON_BAR_HEIGHT / 2))));

            //debug camera info - remove later
            this.hudManager.Add(new UIDebugObject(this, 5, 5, this.fontDictionary["hud"], Color.Black));

            mouseUI = new UIMouseObject(this, 
                this.fontDictionary["uimouse"],
                this.textureDictionary["mouseIcon"],
                TextureInfo.SOURCE_RECTANGLE_MOUSE_ICON, Color.MediumAquamarine, 0, 0.5f);

            //add mouse with new icon
            this.hudManager.Add(mouseUI);


            uiMenuObject = new UIMenuObject(this,
                new Transform2D(new Vector2(0, (screenHeigth - GameInfo.HEALTH_BAR_HEIGHT - (GameInfo.LEFT_HUD_BORDER_HEIGHT * GameInfo.LEFT_HUD_BORDER_Y_SCALE))), 0, new Vector2(GameInfo.LEFT_HUD_BORDER_X_SCALE, GameInfo.LEFT_HUD_BORDER_Y_SCALE)),
                this.textureDictionary["leftHUDBorder"],
                this.fontDictionary["uimenu"],
                new Color(1f, 1f, 1f, 1f));

            uiMenuObject.Add(new MenuItem(MenuInfo.UI_Menu_StartLog, MenuInfo.UI_Menu_StartLog,
                MenuInfo.UI_Menu_StartLog_Bounds, MenuInfo.Menu_Inactive_Color, MenuInfo.Menu_Inactive_Color));

            menuMission = new MenuItem(MenuInfo.UI_Menu_Mission1, MenuInfo.UI_Menu_Mission1,
                MenuInfo.UI_Menu_Mission1_Bounds, MenuInfo.Menu_Inactive_Color, MenuInfo.Menu_Inactive_Color);
            uiMenuObject.Add(menuMission);

            uiMenuObject.Add(new MenuItem(MenuInfo.UI_Menu_EndLog, MenuInfo.UI_Menu_EndLog,
                MenuInfo.UI_Menu_EndLog_Bounds, MenuInfo.Menu_Inactive_Color, MenuInfo.Menu_Inactive_Color));

            this.hudManager.Add(uiMenuObject);

            //right hud

            uiMenuObject = new UIMenuObject(this,
                new Transform2D(new Vector2((screenWidth - (GameInfo.LEFT_HUD_BORDER_WIDTH * GameInfo.LEFT_HUD_BORDER_X_SCALE)), (screenHeigth - GameInfo.HEALTH_BAR_HEIGHT - (GameInfo.LEFT_HUD_BORDER_HEIGHT * GameInfo.LEFT_HUD_BORDER_Y_SCALE))), 0, new Vector2(GameInfo.LEFT_HUD_BORDER_X_SCALE, GameInfo.LEFT_HUD_BORDER_Y_SCALE)),
                this.textureDictionary["rightHUDBorder"],
                this.fontDictionary["uimenu"],
                new Color(1f, 1f, 1f, 1f));

            uiMenuObject.Add(new MenuItem(MenuInfo.UI_Menu_Weapon_Stats, MenuInfo.UI_Menu_Weapon_Stats,
                MenuInfo.UI_Menu_Weapon_Stats_Bounds, MenuInfo.Menu_Inactive_Color, MenuInfo.Menu_Inactive_Color));

            uiMenuObject.Add(new MenuItem(MenuInfo.UI_Menu_Damage, MenuInfo.UI_Menu_Damage,
                MenuInfo.UI_Menu_Damage_Bounds, MenuInfo.Menu_Inactive_Color, MenuInfo.Menu_Inactive_Color));

            uiMenuObject.Add(new MenuItem(MenuInfo.UI_Menu_Fire_Rate, MenuInfo.UI_Menu_Fire_Rate,
                MenuInfo.UI_Menu_Fire_Rate_Bounds, MenuInfo.Menu_Inactive_Color, MenuInfo.Menu_Inactive_Color));

            this.hudManager.Add(uiMenuObject);

            this.textU1=new UITextureObject(this, this.textureDictionary["gauge"], TextureInfo.SOURCE_RECTANGLE_HUD_GAUGE_DAMAGE, Color.White, 0, 1f, this.fontDictionary["hud"], new Vector2(1210 - ((120 - GameInfo.HUD_GAUGE_DAMAGE_WIDTH) /2), 745));
            this.textU2=new UITextureObject(this, this.textureDictionary["gauge"], TextureInfo.SOURCE_RECTANGLE_HUD_GAUGE_FIRE_RATE, Color.White, 0, 1f, this.fontDictionary["hud"], new Vector2(1210 - ((120 - GameInfo.HUD_GAUGE_FIRE_RATE_WIDTH) / 2), 825));

            this.hudManager.Add(this.textU1);

            this.hudManager.Add(this.textU2);


        }
        #endregion

        #region Cameras

        private void InitializeMultiCameraByZone(Viewport viewPort, int viewportPaddingVertical, int viewportPaddingHorizontal)
        {
            //create the template camera which is used to create all others
            Camera camera = new Camera("generic", CameraType.Static, Transform3D.ZeroUnitX, ProjectionParameters.StandardFourThreeFar, viewPort);

            //remove any pre-existing cameras
            this.cameraManager.Clear();

            #region Main 3rd Person Camera
            InitializeCamera3rdPersonWorldDemo(viewportPaddingVertical, viewportPaddingHorizontal);
            #endregion

            #region Entrance Camera
            Camera entranceCamera = (Camera)camera.Clone();
            //set name and type
            entranceCamera.Name = "Camera 2 - Entrance";
            //set position, look, up 
            entranceCamera.Transform = new Transform3D(new Vector3(145, 13, -136), new Vector3(0, -3, 5), new Vector3(0, 1, 0));
            //add to the camera manager
            cameraManager.Add(entranceCamera);
            #endregion

            #region 2nd Camera
            Camera secondCamera = (Camera)camera.Clone();
            //set name and type
            secondCamera.Name = "Camera 2 - Entrance";
            //set position, look, up 
            secondCamera.Transform = new Transform3D(new Vector3(187, 13, -125), new Vector3(-5, -3, 0), Vector3.UnitY);
            //add to the camera manager
            cameraManager.Add(secondCamera);
            #endregion

            #region Ramp Camera
            Camera rampCamera = (Camera)camera.Clone();
            //set name and type
            rampCamera.Name = "Ramp Camera";
            //set position, look, up 
            rampCamera.Transform = new Transform3D(new Vector3(185, 20, -140), new Vector3(-3, -5, 5), Vector3.UnitY);
            //add to the camera manager
            cameraManager.Add(rampCamera);
            #endregion

            #region Upper Level Camera 1
            Camera upperLevel1Camera = (Camera)camera.Clone();
            //set name and type
            upperLevel1Camera.Name = "Upper Level Camera 1";
            //set position, look, up 
            upperLevel1Camera.Transform = new Transform3D(new Vector3(140, 20, -137), new Vector3(5, -3, 0), Vector3.UnitY);
            //add to the camera manager
            cameraManager.Add(upperLevel1Camera);
            #endregion

            #region Upper Level Camera 2
            Camera upperLevel2Camera = (Camera)camera.Clone();
            //set name and type
            upperLevel2Camera.Name = "Upper Level Camera 2";
            //set position, look, up 
            upperLevel2Camera.Transform = new Transform3D(new Vector3(187, 23, -105), new Vector3(-5, -3, -3), Vector3.UnitY);
            //add to the camera manager
            cameraManager.Add(upperLevel2Camera);
            #endregion

            #region Fuse Camera
            Camera fuseCamera = (Camera)camera.Clone();
            //set name and type
            fuseCamera.Name = "Upper Level Camera 2";
            //set position, look, up 
            fuseCamera.Transform = new Transform3D(new Vector3(154, 10, -105), new Vector3(1, -0.5f, -0.2f), Vector3.UnitY);
            //add to the camera manager
            cameraManager.Add(fuseCamera);
            #endregion

            #region Fuse Camera
            Camera exitCamera = (Camera)camera.Clone();
            //set name and type
            exitCamera.Name = "Exit Camera";
            //set position, look, up 
            exitCamera.Transform = new Transform3D(new Vector3(185, 18, -125), new Vector3(-1, -0.5f, 1f), Vector3.UnitY);
            //add to the camera manager
            cameraManager.Add(exitCamera);
            #endregion

            #region TestCamera
            Camera testCamera = (Camera)camera.Clone();
            //set name and type
            testCamera.Name = "Test Camera";
            //set position, look, up 
            testCamera.Transform = new Transform3D(new Vector3(80, 5, 53), new Vector3(-1, 0, 0), Vector3.UnitY);
            //add to the camera manager
            cameraManager.Add(testCamera);
            #endregion
        }

        private void InitializeCameraFirstPerson(int viewportPaddingVertical, int viewportPaddingHorizontal)
        {
            //create the template camera which is used to create all others
            Camera camera = new Camera("generic", CameraType.Static, Transform3D.ZeroUnitX, ProjectionParameters.StandardFourThreeFar, new Viewport(0, 0, this.screenWidth, this.screenHeigth));           
                SingleCameraView(camera, viewportPaddingVertical, viewportPaddingHorizontal); 
        }

        private void InitializeCamera3rdPersonWorldDemo(int viewportPaddingVertical, int viewportPaddingHorizontal)
        {
            //create the template camera which is used to create all others
            Camera camera = new Camera("generic", CameraType.Static, Transform3D.ZeroUnitX, ProjectionParameters.StandardFourThreeFar, new Viewport(0, 0, this.screenWidth, this.screenHeigth));
           SingeCameraThirdPersonWorldDemo(camera, viewportPaddingVertical, viewportPaddingHorizontal);
        }
        private void InitializeCamera3rdPersonDemo(int viewportPaddingVertical, int viewportPaddingHorizontal)
        {
            //create the template camera which is used to create all others
            Camera camera = new Camera("generic", CameraType.Static, Transform3D.ZeroUnitX, ProjectionParameters.StandardFourThreeFar, new Viewport(0, 0, this.screenWidth, this.screenHeigth));
            SingeCameraThirdPersonDemo(camera, viewportPaddingVertical, viewportPaddingHorizontal);
        }

        private void SingeCameraThirdPersonDemo(Camera camera, int viewportPaddingVertical, int viewportPaddingHorizontal)
        {
            #region Camera - third person
            Camera thirdpersonCamera = (Camera)camera.Clone();
            //set name and type
            thirdpersonCamera.Name = "3rd person";
            //set type
            thirdpersonCamera.CameraType = CameraType.ThirdPersonCamera;
            //set viewport
            thirdpersonCamera.Viewport = ScreenUtility.GetPaddedViewport(new Viewport(0, 0, 800, 600), viewportPaddingVertical, viewportPaddingHorizontal);
            //attach controller
            thirdpersonCamera.CameraController = new ThirdPersonCameraController(this, thirdpersonCamera, characterObject, 25, 170, 1f);
            //add to the camera manager
            cameraManager.Add(thirdpersonCamera);
            #endregion
        }

        private void SingeCameraThirdPersonWorldDemo(Camera camera, int viewportPaddingVertical, int viewportPaddingHorizontal)
        {
            #region Camera - third person
            this.thirdpersonCamera = (Camera)camera.Clone();
            //set name and type
            thirdpersonCamera.Name = "3rd person";
            //set type
            thirdpersonCamera.CameraType = CameraType.ThirdPersonCamera;
            //set viewport
            thirdpersonCamera.Viewport = ScreenUtility.GetPaddedViewport(new Viewport(0, 0, this.screenWidth, this.screenHeigth), 0, 0);
            //attach controller
            thirdpersonCamera.CameraController = new ThirdPersonCameraController(this, thirdpersonCamera, characterObject, 20, 151, 1f);
            //add to the camera manager
            cameraManager.Add(thirdpersonCamera);
            #endregion
        }

        private void SingleCameraView(Camera camera, int viewportPaddingVertical, int viewportPaddingHorizontal)
        {
            //remove any pre-existing cameras
            this.cameraManager.Clear();

            #region Camera - first person
            //copy the template camera
            Camera freeLookCamera = (Camera)camera.Clone();
            //set name and type
            freeLookCamera.Name = "1st person";
            //set type
            freeLookCamera.CameraType = CameraType.FreeLookCamera;
            //set position, look, up 
            //in downsize
            //freeLookCamera.Transform = new Transform3D(new Vector3(-260, 5, 159), -Vector3.UnitZ, Vector3.UnitY);

            //in center
            freeLookCamera.Transform = new Transform3D(new Vector3(1, 19,14), -Vector3.UnitZ, Vector3.UnitY);
            //set viewport
            freeLookCamera.Viewport = ScreenUtility.GetPaddedViewport(new Viewport(0, 0, 800, 600), viewportPaddingVertical, viewportPaddingHorizontal);
            //attach controller
            freeLookCamera.CameraController = new FirstPersonCameraController(this, KeyInfo.Key_Array_Free_Look_Camera, 
                GameInfo.Camera_Move_Speed, GameInfo.Camera_Strafe_Speed, GameInfo.Camera_Rotation_Speed, false);
            //add to the camera manager
            cameraManager.Add(freeLookCamera);
            #endregion
        }
        private void MultiCameraView(Camera camera, int viewportPaddingVertical, int viewportPaddingHorizontal)
        {
            //remove any pre-existing cameras
            this.cameraManager.Clear();


            #region Camera - free look
            //copy the template camera
            Camera freeLookCamera = (Camera)camera.Clone();
            //set name and type
            freeLookCamera.Name = "1st person";
            //set type
            freeLookCamera.CameraType = CameraType.FreeLookCamera;
            //set position, look, up 
            freeLookCamera.Transform = new Transform3D(new Vector3(0, 5, 30), -Vector3.UnitZ, Vector3.UnitY);
            //set viewport
            freeLookCamera.Viewport = ScreenUtility.GetPaddedViewport(new Viewport(0, 0, 400, 300), viewportPaddingVertical, viewportPaddingHorizontal);
            //attach controller
            freeLookCamera.CameraController = new FirstPersonCameraController(this, KeyInfo.Key_Array_Free_Look_Camera, GameInfo.Camera_Move_Speed, GameInfo.Camera_Strafe_Speed, GameInfo.Camera_Rotation_Speed, true);
            //add to the camera manager
            cameraManager.Add(freeLookCamera);
            #endregion

            GameObject boxObject = null;

            #region Camera - third person
            Camera thirdpersonCamera = (Camera)camera.Clone();
            //set name and type
            thirdpersonCamera.Name = "3rd person";
            //set type
            thirdpersonCamera.CameraType = CameraType.ThirdPersonCamera;
            //set viewport
            thirdpersonCamera.Viewport = ScreenUtility.GetPaddedViewport(new Viewport(400, 0, 400, 300), viewportPaddingVertical, viewportPaddingHorizontal);
            //attach controller
            thirdpersonCamera.CameraController = new ThirdPersonCameraController(this, thirdpersonCamera, boxObject, 25, 170, 0.025f);
            //add to the camera manager
            cameraManager.Add(thirdpersonCamera);
            #endregion

            #region Camera - rail
            Camera railCamera = (Camera)camera.Clone();
            //set name and type
            railCamera.Name = "rail";
            //set type
            railCamera.CameraType = CameraType.RailCamera;
            //set viewport
            railCamera.Viewport = ScreenUtility.GetPaddedViewport(new Viewport(0, 300, 400, 300), viewportPaddingVertical, viewportPaddingHorizontal);
            //attach controller
            railCamera.CameraController = new RailCameraController(this, railCamera, boxObject, this.railDictionary["rail1"]);
            //add to the camera manager
            cameraManager.Add(railCamera);
            #endregion

            #region Camera - track
            Camera trackCamera = (Camera)camera.Clone();
            //set name and type
            trackCamera.Name = "path";
            //set type
            trackCamera.CameraType = CameraType.TrackCamera;
            //set viewport
            trackCamera.Viewport = ScreenUtility.GetPaddedViewport(new Viewport(400, 300, 400, 300), viewportPaddingVertical, viewportPaddingHorizontal);
            //attach controller
            trackCamera.CameraController = new TrackCameraController(this, trackCamera,
                this.trackDictionary["vertigo track"]);
            //add to the camera manager
            cameraManager.Add(trackCamera);
            #endregion
        }
        #endregion

        #region Load & Unload Content
        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            //dispose of all assets and clear all dictionaries and 
            this.textureDictionary.Dispose();
            this.fontDictionary.Dispose();
            this.trackDictionary.Dispose();
            this.railDictionary.Dispose();

            //remove all loaded content
            Content.Unload();
        }
        #endregion

        #region Draw & Update
        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        /// 
        
        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            if(antCounter > 0)
            {
                respawnRate -= gameTime.ElapsedGameTime.TotalMilliseconds;
                if (respawnRate <= 0)
                {
                    this.AddRespawnedAnt();
                    antCounter--;
                    respawnRate = staticRespawnRate; //reset firerate
                }
             
            }

            if(this.mission1End == true)
            {
                this.uiPoster.Transform.Translation += new Vector2(0,-1.5f);
            }


            UpdateMusic(gameTime);
            DemoHealthChange();
            DemoCameraSwitch();
            DemoGaugeChange();

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);
            base.Draw(gameTime);
       }
        #endregion

        #region Demos
        
        private void DemoGaugeChange()
        {
            if(KeyboardManager.IsFirstKeyPress(KeyInfo.Key_Pause_Show_UI_Menu))
            {
                if(hudOnScreen)
                {
                    hudOnScreen = false;
                    hudManager.Remove(9);
                    hudManager.Remove(8);
                }
                else
                {
                    hudOnScreen = true;

                    this.hudManager.Add(this.textU1);

                    this.hudManager.Add(this.textU2);
                }
            }
        }
        
        //audio demo

        private void UpdateMusic(GameTime gameTime)
        {
            //who is listening?
            this.SoundManager.UpdateListenerPosition(this.cameraManager[0].Transform.Translation);

            //where is the sound coming from? normally, this value changes with the object emitters position
            this.audioEmitter.Position = this.cameraManager.ActiveCamera.Transform.Translation;

            if (this.audioEmitter != null)
            {
                if(this.SoundManager.BkgrndMusic)
                {
                    if (!SoundManager.isCuePlaying("lostBackgroundMusic"))
                    {
                        SoundManager.Play3DCue("lostBackgroundMusic", this.audioEmitter);
                        SoundManager.ResumeCue("fightMusic");
                        SoundManager.StopCue("fightMusic");
                        SoundManager.ResumeCue("menuMusic");
                        SoundManager.StopCue("menuMusic");
                        SoundManager.ResumeCue("tvMusic");
                        SoundManager.StopCue("tvMusic");
                    }
                }
                else if(this.SoundManager.BattleMusic)
                {
                    if (!SoundManager.isCuePlaying("fightMusic"))
                    {
                        SoundManager.Play3DCue("fightMusic", this.audioEmitter);
                        SoundManager.ResumeCue("lostBackgroundMusic");
                        SoundManager.StopCue("lostBackgroundMusic");
                        SoundManager.ResumeCue("menuMusic");
                        SoundManager.StopCue("menuMusic");
                        SoundManager.ResumeCue("tvMusic");
                        SoundManager.StopCue("tvMusic");
                    }
                }
                else if(this.SoundManager.MenuMusic)
                {
                    if (!SoundManager.isCuePlaying("menuMusic"))
                    {
                        SoundManager.Play3DCue("menuMusic", this.audioEmitter);
                        SoundManager.ResumeCue("fightMusic");
                        SoundManager.StopCue("fightMusic");
                        SoundManager.ResumeCue("lostBackgroundMusic");
                        SoundManager.StopCue("lostBackgroundMusic");
                        SoundManager.ResumeCue("tvMusic");
                        SoundManager.StopCue("tvMusic");
                    }
                }
                else if(this.SoundManager.TVMusic)
                {
                    if(!SoundManager.isCuePlaying("tvMusic"))
                    {
                        SoundManager.Play3DCue("tvMusic", this.audioEmitter);
                        SoundManager.ResumeCue("fightMusic");
                        SoundManager.StopCue("fightMusic");
                        SoundManager.ResumeCue("lostBackgroundMusic");
                        SoundManager.StopCue("lostBackgroundMusic");
                        SoundManager.ResumeCue("menuMusic");
                        SoundManager.StopCue("menuMusic");
                    }
                }
            }

            this.extraAudEmi[0].Position = new Vector3(-309, 10, 175);

            if (extraAudEmi[0] != null && menuManager.Pause)
            {
                GameInfo.RADIO -= gameTime.ElapsedGameTime.TotalMilliseconds;

                if (GameInfo.RADIO <= 0)
                {
                    soundManager.Play3DCue("shipRadio", extraAudEmi[0]);
                    soundManager.apply3D("shipRadio", extraAudEmi[0]);
                    GameInfo.RADIO = 60000;
                }
            }
        }

        private void DemoHealthChange()
        {
            if (this.keyboardManager.IsFirstKeyPress(Keys.D0))
            {
                this.UIManager.Health += 10;

                if (UIManager.Health > GameInfo.HEALTH_BAR_WIDTH)
                {
                    UIManager.Health = GameInfo.HEALTH_BAR_WIDTH;
                }
            }
            else if (this.keyboardManager.IsFirstKeyPress(Keys.D9))
            {
                this.UIManager.Health -= 10;

                if (UIManager.Health < 0)
                {
                    UIManager.Health = 0;
                }
            }
            else if (this.keyboardManager.IsFirstKeyPress(Keys.D7))
            {


                this.UIManager.Weapon += 10;

                if (UIManager.Weapon > GameInfo.WEAPON_BAR_WIDTH)
                {
                    UIManager.Weapon = GameInfo.WEAPON_BAR_WIDTH;
                }
            }
            else if (this.keyboardManager.IsFirstKeyPress(Keys.D8))
            {
                this.UIManager.Weapon -= 10;

                if (UIManager.Weapon < 0)
                {
                    UIManager.Weapon = 0;
                }
            }
        }

        private void DemoCameraSwitch()
        {

            if (this.keyboardManager.IsFirstKeyPress(Keys.F5))
            {
                this.cameraManager.CycleActiveCamera(true);
            }
            else if (this.keyboardManager.IsFirstKeyPress(Keys.F4))
            {
                this.cameraManager.CycleActiveCamera(false);
            }

        }

        #endregion


    }
}
